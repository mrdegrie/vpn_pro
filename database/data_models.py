from pydantic import BaseModel
from typing import Optional
from datetime import datetime
from uuid import UUID


class User(BaseModel):
    user_id: int
    user_name: Optional[str] = None
    balance: float
    partner_id: Optional[int] = None
    role: str
    language: Optional[str] = None
    register_date: datetime
    status: Optional[bool] = False


class OvpnPlans(BaseModel):
    plan_id: int
    name: str
    description: str
    price: float
    ovpn_limit: int
    range: int
    status: bool


class OVPNServer(BaseModel):
    server_id: int
    parent_ovpn_item_id: Optional[int] = None
    country: str
    ip: str
    ssh_user: str
    ssh_pass: str
    status: bool


class OvpnPlanTransaction(BaseModel):
    user_id: int
    plan_id: int
    name: str
    description: str
    price: int
    ovpn_limit: int
    range: int
    status: bool


class SharedVPNtransaction(BaseModel):
    order_id: int
    user_id: int
    ovpn_user_name: str
    country: str
    ovpn_config: str
    order_time: datetime
    exp_time: datetime
    tariff_id: int
    auto_update: Optional[bool] = True
    item_id: int
    trial: bool
    status: bool


# Модель временной транзакции при покупке SHRED VPN плана
class SharedPlanTempTransaction(BaseModel):
    order_id: int
    user_id: int
    plan_id: int
    name: str
    description: str
    price: int
    ovpn_limit: int
    range: int
    url: Optional[str] = None
    status: str


# Модель временной транзакции при покупке SHRED VPN плана
class SharedOvpnPlans(BaseModel):
    user_id: int
    plan_id: int
    name: str
    description: str
    price: int
    ovpn_limit: int
    range: int
    start_date: datetime
    ext_date: datetime
    status: bool


# Модель временных транзвкций
class TempTransaction(BaseModel):
    order_id: int
    user_id: int
    user_name: Optional[str] = None
    name: str
    price: int
    url: str
    date_created: datetime
    vendor_code: str
    status: str


# Мдель wallet
class Wallet(BaseModel):
    wallet_name: str
    value_1: Optional[str] = None
    value_2: Optional[str] = None
    value_3: Optional[str] = None
    value_4: Optional[str] = None
    status: bool


# class OvpnItem(BaseModel):
#     item_id: int
#     country: str
#     price: int
#     renewal_price: int
#     max_clients: int
#     status: bool


class OvpnItem(BaseModel):
    item_id: int
    country: str
    country_code: str
    price: int
    renewal_price: int
    status: bool


class PrivateTrial(BaseModel):
    """Модель индивидуального триала"""

    id: UUID
    user_id: int
    days: Optional[int] = 1
    exp: Optional[datetime]
    alert_status: Optional[str] = None
    status: Optional[bool] = False


class VPNTariff(BaseModel):
    """Модель тарифа VPN"""

    tariff_id: int
    name: Optional[str] = None
    period: int
    percent: int
    status: Optional[bool] = True


class IpCheckerTransaction(BaseModel):
    """Траназкция IP чекера"""
    order_id: int
    user_id: int
    ip_count: int
    amount: float
    payload: str
    check_time: Optional[datetime] = None
    vendor_code: int


class NewUser(BaseModel):
    user_id: int
    user_name: Optional[str] = None
    partner_balance: int
    partner_id: int
    role: str
    language: str
