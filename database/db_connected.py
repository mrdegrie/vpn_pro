import sqlite3
import aioredis
from aioredis import Redis

from configs.config import redis_url, passwod_redis


# класс для управления коммандами к БД
class DBConnect:
    def __init__(self):
        self._DBPatch = './database/database.db'
        self.connect = sqlite3.connect(self._DBPatch)
        self.connect: sqlite3.connect  # Обьект коннект к sqlite_3
        self.cursor = self.connect.cursor()


async def get_redis() -> Redis:
    return await aioredis.from_url(url=redis_url, encoding="utf-8", decode_responses=True, password=passwod_redis)

