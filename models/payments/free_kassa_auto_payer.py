import hashlib
import time
import logging
from typing import Optional, List
from datetime import datetime, timedelta
import hmac
import aiohttp
from aiogram import Dispatcher, exceptions

from configs import config
from database.data_models import SharedPlanTempTransaction
from models.backend.payment_endpoints import PaymentAPI
from models.backend.backend_models import LastTempTransaction


class AutoKassa:
    """Автономный обработчик платежей Free-Kassa"""
    def __init__(self):
        self.MerchantID = config.merchant_id
        self.Secret = config.secret_1
        self.Secret_2 = config.secret_1
        self.api_key = config.free_kassa_api_key

        self.TransactionFK = []
        self.TempTransaction: Optional[List[SharedPlanTempTransaction]] = []
        self.URL = 'https://api.freekassa.ru/v1/orders'
        self._header = {'User-Agent': 'Mozilla/5.0 (Platform; Security; OS-or-CPU; Localization; rv:1.4) '
                                      'Gecko/20030624 Netscape/7.1 (ax)'}

    # Получаем заявки на оплату не старше 30 минут, проверяем их по API - зачесляем средства
    async def auto_checker(self, dp: Dispatcher):
        """
        Step 1: Получаем временные траназкции со статусом Paid за сегодня
        Step 2: Получить сегодняшние транзы из ФК
        Step 3: найти трнзакцию - если есть, произвести изменение статуса страназкции
        step 4: Произвести уведомления юзера о зачеслении

        :param dp:
        :return:
        """
        transactionFK = []
        temp_transaction: Optional[LastTempTransaction] = None
        # Получаем временные траназкции со статусом created за сегодня
        search_data = datetime.now() - timedelta(days=1)
        backend_free_kassa = PaymentAPI()
        temp_trans = await backend_free_kassa.get_last_temp_transaction(search_data)
        if temp_trans.result:
            temp_transaction = temp_trans
            # Транзакции есть, получаем новые транзы ФК
            transactionFK = await self._request_last_transaction_from_fk()
            if transactionFK:
                # находим заявки из БД в заявках фк
                for temp_trans in temp_transaction.result:
                    for fk_trans in transactionFK:
                        try:
                            if int(fk_trans['merchant_order_id']) == int(temp_trans.order_id):
                                if int(fk_trans['status']) == 1:  # Платеж прошел, зачисляем средства
                                    # Зачисляем юзеру транзакцию
                                    result = await backend_free_kassa.fk_success_pay(temp_trans.order_id)
                                    if result.status:
                                        try:
                                            await dp.bot.send_message(
                                                chat_id=int(temp_trans.user_id),
                                                text=f'<b>✅ Баланс успешно пополнен! /start</b>',
                                                parse_mode='html'
                                            )
                                        except Exception as e:
                                            logging.warning(e)

                                        try:
                                            for admin in config.admins:
                                                await dp.bot.send_message(
                                                    admin, f'<b>FK приняла платеж: #{temp_trans.order_id} *** '
                                                           f'{temp_trans.price}🆔:{temp_trans.user_id}</b>',
                                                    parse_mode='html')
                                        except Exception as e:
                                            logging.warning(e)
                                            break
                                        else:
                                            break
                                    else:
                                        logging.warning(f'error payment {result.msg}')
                        except ValueError as e:
                            logging.warning(e)

        time_extp = datetime.now()
        try:
            await dp.bot.send_message(1126262393,
                                      f'<b>FK Проверку произвел, время {str(time_extp)}</b>',
                                      parse_mode='html',
                                      )
        except exceptions.BadRequest:
            logging.warning('Failed send report')

    # Получить последние транзакции в ФК
    async def _request_last_transaction_from_fk(self):
        time_check = time.time()
        sign = hmac.new(
            bytearray(self.api_key, 'utf-8'),
            bytearray(f'{time_check}|{self.MerchantID}', 'utf-8'),
            hashlib.sha256).hexdigest()

        data = {
            "nonce": str(time_check),
            "shopId": self.MerchantID,
            "signature": sign
        }
        TransactionFK = None
        async with aiohttp.ClientSession(headers=self._header) as session:
            async with session.post(self.URL, json=data) as resp:
                if resp.status == 200:
                    try:
                        result_json = await resp.json()
                        if result_json['type'] == 'success':
                            transaction_fk = result_json['orders']
                            TransactionFK = transaction_fk
                    except TypeError:
                        pass
        return TransactionFK
