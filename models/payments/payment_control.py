from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup, WebAppInfo
from typing import Optional

from models.backend.payment_endpoints import PaymentAPI
from models.base_models.local_settings import LocalSettings
from models.ui.ui_storage import UIControl
from configs.config import support_link


class PaymentControl:
    """Управление платежными системами"""

    def __init__(self):
        self.MSG: Optional[str] = None
        self.error_msg: Optional[str] = None
        self._keyboard = InlineKeyboardMarkup()
        self.img_id: Optional[str] = None

    @property
    def keyboard(self):
        """Клавиатура платежной системы"""
        return self._keyboard

    async def get_all_active_wallet(self,
                                    user_id: int,
                                    get_settings: bool = False,
                                    ):
        """Сформировать все активные платежные системы"""

        if get_settings:
            settings = LocalSettings()
            self.img_id = await settings.get_start_photo()

        ui = UIControl(user_id=user_id)
        await ui.get_language()
        payment_backend = PaymentAPI()
        response = await payment_backend.get_active_wallet(location=ui.language)
        print(response)
        if response.wallet:
            for index, wallet in enumerate(response.wallet, start=1):
                # if wallet.wallet_name == 'FK':
                #     self._keyboard.row(InlineKeyboardButton(text=f'💳 CARD RU/FK',
                #                                             callback_data=f'wallet#{wallet.wallet_name}'))
                #     self._keyboard.row(InlineKeyboardButton(text=f'💲 CRYPTO/FK',
                #                                             callback_data=f'wallet#{wallet.wallet_name}'))
                #     self._keyboard.row(InlineKeyboardButton(text=f'💰 QIWI/FK',
                #                                             callback_data=f'wallet#{wallet.wallet_name}'))
                #     self._keyboard.row(InlineKeyboardButton(text=f'→ ПРОЧЕЕ/FK',
                #                                             callback_data=f'wallet#{wallet.wallet_name}'))

                if wallet.wallet_name == 'PAYOK':
                    self._keyboard.row(
                        InlineKeyboardButton(
                            text=ui.get_text("wallet_crypto"), callback_data=f'wallet#{wallet.wallet_name}')
                    )

                elif wallet.wallet_name == 'LAVA':
                    if ui.language == 'ru':
                        self._keyboard.row(
                            InlineKeyboardButton(
                                text=ui.get_text("wallet_bank_card"), callback_data=f'wallet#{wallet.wallet_name}'),
                            InlineKeyboardButton(text=ui.get_text("wallet_qiwi"),
                                                 callback_data=f'wallet#{wallet.wallet_name}')

                        )
                    else:
                        self._keyboard.row(
                            InlineKeyboardButton(
                                text=ui.get_text("wallet_bank_card"), callback_data=f'wallet#{wallet.wallet_name}'),
                        )

                    if index == len(response.wallet):
                        self._keyboard.row(InlineKeyboardButton(text=ui.get_text("other_methods_button"),
                                                                callback_data=f'wallet#{wallet.wallet_name}'))

            self._keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"),
                                                    callback_data='private_cabinet'))
            self.MSG = ui.get_text("choosing_payment_system")
        else:
            self.error_msg = ui.get_text("payment_error")
        return self.MSG

    async def select_amount_page(self, user_id: int, lang: str):
        """Получить страницу запроса суммы пополнения баланса"""

        ui = UIControl(user_id=user_id)
        await ui.get_language()
        self.MSG = ui.get_text('select_deposit_amount')

        self._keyboard.row(
            InlineKeyboardButton(text='$5', callback_data='amounnt_pay#5'),
            InlineKeyboardButton(text='$10', callback_data='amounnt_pay#10'),
            InlineKeyboardButton(text='$15', callback_data='amounnt_pay#15'),
        )
        self._keyboard.row(
            InlineKeyboardButton(text='$20', callback_data='amounnt_pay#20'),
            InlineKeyboardButton(text='$30', callback_data='amounnt_pay#30'),
            InlineKeyboardButton(text='$50', callback_data='amounnt_pay#50'),
        )
        self._keyboard.row(InlineKeyboardButton(text=ui.get_text("custom_amounnt_pay"),
                                                callback_data='custom_amounnt_pay'))
        self._keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='all_payments'))
        return self.keyboard

    async def get_tyemp_transaction_info(self,
                                         order_id: int,
                                         user_id: int,
                                         lang: str,
                                         ):
        """Получить информацию о временнй транзакции"""
        ui = UIControl(user_id=user_id)
        payment_api = PaymentAPI()
        trans_info = await payment_api.get_info_temp_transaction(order_id=order_id)
        await ui.get_language()

        if trans_info.trnsaction:
            if trans_info.trnsaction.status == 'paid':
                self.MSG = ui.get_text("successful_payment_text").format(pice=trans_info.trnsaction.price)
            elif trans_info.trnsaction.status == 'created':

                self.MSG = ui.get_text("waiting_for_payment").format(support_link=support_link)
                self._keyboard.row(InlineKeyboardButton(
                    text=ui.get_text("to_pay_button"), web_app=WebAppInfo(url=trans_info.trnsaction.url)))
                self._keyboard.row(InlineKeyboardButton(
                    text=ui.get_text("check_payment_button"),
                    callback_data=f'pending_pay#{trans_info.trnsaction.order_id}'))

            self._keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"),
                                                    callback_data='all_payments'))
        else:
            self.MSG = 'Ошибка проверки!'
            self._keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"),
                                                    callback_data='all_payments'))
