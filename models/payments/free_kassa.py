from typing import Optional
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup, WebAppInfo

from models.backend.payment_endpoints import PaymentAPI
from models.ui.ui_storage import UIControl


class FreeKassaControl:
    def __init__(self):
        self.MSG: Optional[str] = None
        self.keyboard = InlineKeyboardMarkup()

    # станица запроса суммы пополнения FK
    async def select_amount_page(self, user_id: int, lang: str):
        ui = UIControl(user_id=user_id)
        await ui.get_language()
        self.MSG = ui.get_text("custom_deposit_amount")
        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='all_payments'))
        return self.keyboard

    # Создать новую платежку FREE-KASSA
    async def create_new_payment(self, user_id: int, amount: int, lang: str, user_name: str = None):
        ui = UIControl(user_id=user_id)
        payment_backend = PaymentAPI()
        await ui.get_language()
        result = await payment_backend.create_free_kassa_pay(user_id, amount, user_name=user_name)
        if result.status:
            self.MSG = ui.get_text('new_payment_fk').format(order_id=result.order_id, price=result.price)
            self.keyboard.row(InlineKeyboardButton(text=ui.get_text("to_pay_button"),
                                                   web_app=WebAppInfo(url=result.payment_url)))
        else:
            self.MSG = ui.get_text("payment_creation_error")
        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='all_payments'))
        return self.keyboard

    async def get_error_amount_message(self, user_id: int, lang: str):
        ui = UIControl(user_id=user_id)
        await ui.get_language()
        self.MSG = ui.get_text("payment_amount_error")
        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='all_payments'))
        return self.keyboard



