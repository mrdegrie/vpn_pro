import logging
import asyncio
from typing import Optional
from datetime import datetime
from aiogram import Dispatcher, exceptions

from configs import config
from models.backend.backend_models import ActivateRecord
from models.ui.locales.available_localizations import country_lang_conv
from models.backend.shared_vpn_endpoints import SharedOVPNConfigs
from models.uttils.alerts import Alerts
from models.ui.ui_storage import UIControl


class ConfigAutoPayer:
    """Автономный обработчик оплат конфигов"""

    def __init__(self):
        self.status: Optional[bool] = False
        self._status_codes = {
            404: "Not dound configs to activations",
            500: "Items / tariffs not found",
            501: "backend_error",
        }

    # --- NEW VERSION AUTO-ACTIVATION
    async def get_config_activation(self, dp: Dispatcher):
        """
        Автоматичесий активатор конфигов OPEN VPN
        Вызывает метод активации на стороне backend-а
        Получает в ответ результат активации - делает рассылку
        """
        config_api = SharedOVPNConfigs()
        get_config = await config_api.get_extend_configs()
        if not get_config.status:
            await self._admin_alert(
                dp, f"❗️Активатор конфигов: "
                    f"|{get_config.status_code}({self._status_codes[get_config.status_code]})"
                    f"|{get_config.msg}",
            )
            return

        ui = UIControl(666)
        await ui.open_redis_connection()
        start_check_time = datetime.now()
        for index, report in enumerate(get_config.report, start=1):
            """
            Status codes of report:
                200 - ok, extend
                201 - Конфиг деактивирван из-за отлюченнго автопродления
                400 - Деактивирован из-за нехватки баланса
                401 - Не удалось деактивирвать конфиг по не известной причине 
            """
            await ui.get_language_for_mainling(report.user_id)
            if report.status_code == 200:
                # Конфиг успешно продлен
                await self._alert_user(dp, report, ui, "config_successfully_extended")

            elif report.status_code == 201:
                # Конфиг успешно деактивирован из-за отлюченнго продления
                await self._alert_user(dp, report, ui, "config_deactivate")

            elif report.status_code == 400:
                # Конфиг деактивирован из-за не достата баланса
                await self._alert_user(dp, report, ui, "no_money_for_extension")

            elif report.status_code == 401:
                await self._admin_alert(
                    dp, f"❗️Активатор конфигов:\nОшибка продления: "
                        f"|{report.config_id}({report.status_code})"
                        f"|{report.user_id}",
                )
            else:
                await self._admin_alert(
                    dp, f"❗️Активатор конфигов:\nНеизвестная струтура данных: "
                        f"|{report.config_id}({report.status_code})"
                        f"|{report.user_id}",
                )

            await self._admin_alert(
                dp, f"⚠️ Активатор: итерация {index}"
                    f"|{report.config_status}",
            )
            await asyncio.sleep(.5)

        await ui.close_redis_connection()
        finish_time = datetime.now() - start_check_time
        for system_admin in config.system_alerts:
            await dp.bot.send_message(
                chat_id=system_admin,
                text=f'🟢 Активатор конфигов отработал, затрачено времени: {finish_time}')
        return

    # --- OLD VERSION AUTO-ACTIVATOR
    # async def start_config_auto_payner(self, dp: Dispatcher):
    #     """
    #     Старт автоматического чекера оплат конфигов - отключает конфиги которые не ввозможно оплатить
    #     Если баланс пользователя достаточен для продления - снимает баланс и продлевает exp date в БД конфига
    #     """
    #
    #     # Получить конфиги которые кончаются сегодня и items для мэпинга цен
    #     config_api = SharedOVPNConfigs()
    #     item_api = OVPNItems()
    #     ui = UIControl(666)
    #     await ui.open_redis_connection()
    #
    #     exp_configs = await config_api.get_exp_configs(days_before_disconnection=0)
    #     renewal_items = await item_api.get_active_items()
    #     items = {}  # OVPN тарифы
    #
    #     if not renewal_items.items:
    #         logging.error('No active ITEM in Config Deactivate!!!!')
    #         return
    #
    #     for i in renewal_items.items:
    #         items.update({i.item_id: i})
    #
    #     start_check_time = datetime.now()
    #     if exp_configs.configs:
    #         for index, ovpn_config in enumerate(exp_configs.configs, start=1):
    #             # Получить днные ITEM по ID
    #             if items.get(ovpn_config.item_id) is None:
    #                 continue
    #
    #             renewal_item = items.get(ovpn_config.item_id)
    #             # вызываем метод продления
    #             extend_result = await config_api.extend_config(
    #                 config_id=ovpn_config.order_id,
    #                 extend_price=renewal_item.renewal_price,
    #             )
    #             await asyncio.sleep(.5)
    #             if extend_result.status:
    #                 await ui.get_language_for_mainling(ovpn_config.user_id)
    #                 if extend_result.status_code == 200:
    #                     # Конфиг успешно продлен
    #                     await self._alert_user(dp, ovpn_config, ui, "config_successfully_extended")
    #
    #                 elif extend_result.status_code == 107:
    #                     # Конфиг деактивирован из-за не достата баланса
    #                     await self._alert_user(dp, ovpn_config, ui, "no_money_for_extension")
    #
    #                 elif extend_result.status_code == 104:
    #                     # Конфиг успешно деактивирован из-за отлюченнго продления
    #                     await self._alert_user(dp, ovpn_config, ui, "config_deactivate")
    #
    #                 else:
    #                     #  Не известная ошибка
    #                     text = f'⚠️ Проблема с продлением конфига: MSG: ' \
    #                            f'{extend_result.msg}, CODE_ID: {extend_result.status_code}'
    #
    #                     await self._admin_alert(dp, text, )
    #
    #
    #             else:
    #                 text = f'⚠️ Проблема с продлением конфига: MSG: ' \
    #                        f'{extend_result.msg}, CODE_ID: {extend_result.status_code}'
    #
    #                 await self._admin_alert(dp, text, )
    #
    #             for admin in config.system_alerts:
    #                 try:
    #                     await dp.bot.send_message(chat_id=admin, text=f'Активация, итерация: {index}')
    #                 except exceptions.BadRequest as err:
    #                     logging.error(err.text)
    #
    #     await ui.close_redis_connection()
    #     finish_time = datetime.now() - start_check_time
    #     for system_admin in config.system_alerts:
    #         await dp.bot.send_message(
    #             chat_id=system_admin,
    #             text=f'🟢 Активатор конфигов отработал, затрачено времени: {finish_time}')
    #     return

    async def _alert_user(self,
                          dp: Dispatcher,
                          report: ActivateRecord,
                          ui: UIControl,
                          ui_id: str):

        mailing = Alerts(dp=dp)
        try:
            await mailing.send_message(
                user_id=report.user_id,
                text_message=ui.get_text(ui_id).format(
                    country=country_lang_conv(
                        country=report.county,
                        lang=ui.language,
                    )
                ),
            )
            mailing.clean()
        except exceptions.BadRequest as err:
            logging.error(err.text)

    async def _admin_alert(self, dp: Dispatcher, message_text: str):

        for admin in config.system_alerts:
            try:
                await dp.bot.send_message(chat_id=admin, text=message_text)
            except exceptions.BadRequest as err:
                logging.error(err.text)
