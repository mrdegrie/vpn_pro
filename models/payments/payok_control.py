from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup, WebAppInfo
from typing import Optional

from models.backend.payment_endpoints import PaymentAPI
from models.ui.ui_storage import UIControl


class PayokControl:
    def __init__(self):
        self.keyboard = InlineKeyboardMarkup()
        self.MSG: Optional[str] = None

    async def create_new_payment(self,
                                 amount: int,
                                 user_id: int,
                                 user_name: str,
                                 lang: str,
                                 ):
        """Создать новый платеж и получить ссылку на оплату пользователя в виде WebView Keyboard"""

        pay_api = PaymentAPI()
        ui = UIControl(user_id=user_id)
        await ui.get_language()

        new_pay = await pay_api.create_payok_pay(user_id=user_id,
                                                 user_name=user_name,
                                                 currency='RUB',
                                                 amount=amount)
        if new_pay.status:
            self.MSG = ui.get_text("new_payment_fk").format(
                order_id=new_pay.payment.order_id,
                price=amount,
            )
            self.keyboard.row(InlineKeyboardButton(text=ui.get_text("to_pay_button"),
                                                   web_app=WebAppInfo(url=new_pay.payment.url)))

        else:
            self.MSG = ui.get_text("payment_creation_error")
        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='all_payments'))
        return self.keyboard

    async def get_error_page(self, user_id: int, ui_id: str):
        ui = UIControl(user_id=user_id)
        await ui.get_language()
        self.MSG = ui.get_text(ui_id)
        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='all_payments'))
        return self.keyboard

    # async def pending_payment(self, order_id: int):
    #     """Проверить статус платежа в система"""
    #     pay_api = PaymentAPI()
    #     await pay_api.get_temp_transactions()
