from typing import Optional
from pydantic import ValidationError
from aiogram.types import InlineKeyboardMarkup
from aioredis import Redis

from models.backend.user_endpoints import UserAPIv1
from models.backend import backend_models
from models.backend.backend_models import UserInfoResponse
from database.db_connected import get_redis


class UserControl:
    """Класс пользователя"""

    def __init__(self):
        self.MSG: Optional[str] = None
        self.keyboard = InlineKeyboardMarkup()
        self.error_message: Optional[str] = None

    # Добавить нового пользователя
    async def add_user(self, user_id: int,
                       user_name: Optional[str] = None,
                       partner_id: Optional[int] = 666,
                       language: str = 'ru',
                       ) -> backend_models.AddUserResponse:
        """
        :param language:
        :param user_id:
        :param user_name:
        :param partner_id:
        :return:
        """

        user_backend = UserAPIv1()
        return await user_backend.add_user(user_id=user_id,
                                           user_name=user_name,
                                           partner_id=partner_id,
                                           language=language,
                                           )

    # Получить инфомацию о пользователе
    async def get_user_info(self, user_id: int, get_cashed: bool = False):
        user_backend = UserAPIv1()
        if get_cashed:
            redis: Redis = await get_redis()
            user = await redis.get(name=f'user_info_{user_id}')
            if user is None:
                user_info = await user_backend.get_user_info(user_id)
                await redis.set(name=f'user_info_{user_id}', value=user_info.json(), ex=60)
                await redis.close()
                return user_info
            else:
                await redis.close()
                try:
                    return UserInfoResponse.parse_raw(user)
                except ValidationError:
                    return False
        else:
            return await user_backend.get_user_info(user_id)

    # Обновить баланс пользовтеля
    async def update_user_balane(self, user_id: int, amount: int, update_mode: str = 'add'):
        user_api = UserAPIv1()
        return await user_api.update_user_balance(user_id, amount, update_mode)

    async def get_user_language(self, user_id: int):
        user_api = UserAPIv1()
        response = await user_api.get_user_language(user_id)
        if not response.status:
            self.error_message = response.msg
            return False
        return response.language

    async def update_user_language(self, user_id: int, language: str):
        user_api = UserAPIv1()
        response = await user_api.update_user_language(user_id, language)
        if not response.status:
            self.error_message = response.msg
            return False
        return response
