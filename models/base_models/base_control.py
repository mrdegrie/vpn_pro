from typing import Optional
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup
from aiogram import types
from aiogram import exceptions

from models.users.user_control import UserControl
from models.base_models.local_settings import LocalSettings
from models.ui.ui_storage import UIControl
from models.backend.backend_models import AddUserResponse
from configs.config import channels_id, channels_uls, btc_exchanger_url
from models.ui.locales.available_localizations import locales, flags, check_localization_availability


class BaseControl:
    """Клас базового функционала (стартовое меню и прочее)"""

    def __init__(self):
        self.MSG: Optional[str] = None
        self.keyboard = InlineKeyboardMarkup()
        self._text_keyboard = ReplyKeyboardMarkup(resize_keyboard=True)
        self.start_photo: Optional[str] = None
        self.error_message: Optional[str] = None
        self._new_user: Optional[AddUserResponse] = None
        self._referral = None
        self._language = 'ru'

    @property
    def text_keyboard(self):
        return self._text_keyboard

    @property
    def start_image(self):
        return self.start_photo

    @property
    def new_user(self):
        return self._new_user

    @property
    def referral(self):
        return self._referral

    @property
    def language(self):
        return self._language

    def channel_link(self, lang: str):
        try:
            return channels_uls[lang]
        except KeyError:
            return channels_uls['en']

    async def start_bot(self,
                        language: str,
                        user_id: Optional[int] = None,
                        message_text: Optional[str] = None,
                        user_name: Optional[str] = None,
                        start: bool = True
                        ):
        """Старт бота - софмировать сообщение"""

        if start:
            user_control = UserControl()
            referral = 666
            if message_text:
                if len(message_text[7:]) > 0:
                    referral = int(message_text[7:])
                    if referral == user_id:
                        referral = 666
                    else:
                        pass
                        self._referral = referral
            if not user_name:
                user_name = None
            self._new_user = await user_control.add_user(user_id=user_id,
                                                         user_name=user_name,
                                                         partner_id=referral,
                                                         language=check_localization_availability(language),
                                                         )
        ui = UIControl(user_id)
        await ui.get_language()
        self._language = ui.language

        # Формиуем стартовое мен
        main_menu_text_button = [ui.get_text(ui_id='main_menu_text_button')]
        self._text_keyboard.add(*main_menu_text_button)

        self.keyboard.row(
            InlineKeyboardButton(text=ui.get_text("shared_vpn"), callback_data='shared_vpn'),
            InlineKeyboardButton(text=ui.get_text("main_proxy_page"), callback_data='main_proxy_page'),

        )
        self.keyboard.row(
            InlineKeyboardButton(text=ui.get_text("main_ip_score_page"), callback_data='main_ip_score_page'),
            InlineKeyboardButton(text=ui.get_text("private_cabinet"), callback_data='private_cabinet'),

        )
        self.keyboard.row(
            InlineKeyboardButton(text=ui.get_text("partner_cabinet"), callback_data='partner_cabinet'),
            InlineKeyboardButton(text=ui.get_text("channel_url_button"), url=self.channel_link(ui.language)),
        )
        self.keyboard.row(
            InlineKeyboardButton(text=ui.get_text("vpn_info"), callback_data='vpn_info'),
            InlineKeyboardButton(text=ui.get_text("help_meesage"), callback_data='help_meesage'),
        )

        self.keyboard.row(
            InlineKeyboardButton(text=ui.get_text("crypto_change_button"), url=btc_exchanger_url),

        )

        self.MSG = ui.get_text('start_message')

        settings = LocalSettings()
        self.start_photo = await settings.get_start_photo()
        return self.keyboard

    async def sub_check(self, message: types.Message, user_id: int):
        ui = UIControl(user_id=user_id)
        lang = await ui.get_language()
        try:
            chanell_chek = await message.bot.get_chat_member(channels_id[lang], user_id)
        except exceptions.ChatNotFound:
            chanell_chek = {'status': 'left1'}
        if chanell_chek["status"] != 'left':
            return True
        else:
            self.keyboard = None
            self.keyboard = types.InlineKeyboardMarkup()
            self.MSG = ui.get_text("subscribe")
            self.keyboard.row(InlineKeyboardButton(
                text=ui.get_text('channel_url_button'),
                url=self.channel_link(ui.language),
            ),
            )
            self.keyboard.row(InlineKeyboardButton(text=ui.get_text("check_subs"), callback_data='check_subs'))
            self.error_message = ui.get_text('error_message')
            return False

    async def change_lang_page(self):
        time_kayboad_storage = []

        for index, leng in enumerate(locales, start=1):
            time_kayboad_storage.append(leng)
            if len(time_kayboad_storage) == 2:
                self.keyboard.row(
                    InlineKeyboardButton(text=flags[time_kayboad_storage[0]],
                                         callback_data=f'change_lang#{time_kayboad_storage[0]}'),
                    InlineKeyboardButton(text=flags[time_kayboad_storage[1]],
                                         callback_data=f'change_lang#{time_kayboad_storage[1]}')
                )
                time_kayboad_storage.clear()
                continue
        if time_kayboad_storage:
            self.keyboard.row(
                InlineKeyboardButton(text=flags[time_kayboad_storage[0]],
                                     callback_data=f'change_lang#{time_kayboad_storage[0]}')
            )

        self.keyboard.row(InlineKeyboardButton(text='🔙 Back', callback_data='private_cabinet'))
        self.MSG = '<b>Choose a language for the bot 👇</b>'
        return self.keyboard

    async def change_bot_lang(self, new_lang: str, user_id: int):
        """Произвести замену локализации бота"""

        ui = UIControl(user_id)
        return await ui.update_language(language=new_lang)
