from typing import Optional
from database.db_connected import DBConnect


class LocalSettings:
    """Класс для упавления настроойкми бота"""
    def __init__(self):
        self.result: Optional[str] = None

    async def get_start_photo(self):
        """# Получить стартовое фото"""
        db = DBConnect()
        message = db.cursor.execute("""SELECT value_1 FROM settings WHERE name = 'start_photo'""").fetchall()
        db.connect.close()
        if message:
            self.result = message[0][0]
        return self.result

    async def get_vpn_user_manual_from_db(self, manual_type: str):
        """# Get User vpn manual from db"""
        db = DBConnect()
        message = db.cursor.execute("""SELECT value_1 FROM settings WHERE name = ?""", [manual_type]).fetchall()
        db.connect.close()
        if message:
            self.result = message[0][0]
        return self.result

