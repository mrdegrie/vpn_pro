from typing import Optional
# from aiogram import types
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup, WebAppInfo

# from models.users.user_control import UserControl
from models.base_models.local_settings import LocalSettings
from models.ui.ui_storage import UIControl
from configs.config import support_link, backend_remote_domain


class UserSupport:
    """Класс личного кабинета"""
    def __init__(self):
        self.MSG: Optional[str] = None
        self.keyboard = InlineKeyboardMarkup()
        self.start_photo: Optional[str] = None
        self._faq_url = f'{backend_remote_domain}/v1/users/faq-page'

    # Получить страницу FAQ
    async def get_faq_page(self, user_id: int, lang: str):
        ui = UIControl(user_id=user_id)
        await ui.get_language()
        self.MSG = ui.get_text('FAQ_text')
        self.keyboard.row(
            InlineKeyboardButton(text=ui.get_text("vpn_manual"), callback_data='vpn_manual'),
            InlineKeyboardButton(text=ui.get_text("FAQ"), web_app=WebAppInfo(url=self._faq_url))
        )
        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("ask_me"), url=support_link))
        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"),
                                               callback_data='next_start_message'))
        return self.keyboard

    # Сформиррвать страницу информации о VPN
    async def vpn_info_page(self, user_id: int, lang: str, call_back: bool = False):
        ui = UIControl(user_id=user_id)
        await ui.get_language()
        self.MSG = ui.get_text('help_message')
        if call_back:
            self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"),
                                                   callback_data='next_start_message'))
        return self.MSG

    async def seelect_usert_device(self, user_id: int, lang: str, text_mode: bool = False):
        """Сорфмировать страницу VPN user Manual"""

        ui = UIControl(user_id=user_id)
        await ui.get_language()

        self.MSG = ui.get_text("what_device")
        self.keyboard.row(
            InlineKeyboardButton(text=ui.get_text("ios_vpn-user_manual"),
                                 callback_data='user_manual#ios_vpn_manual_text'),
            InlineKeyboardButton(text=ui.get_text("android_vpn_user_manual"),
                                 callback_data='user_manual#android_vpn_manual_text'),
        )
        self.keyboard.row(
            InlineKeyboardButton(text=ui.get_text("macos_user_manual"),
                                 callback_data='user_manual#macos_user_manual_text'),
            InlineKeyboardButton(text=ui.get_text("win_user_manual"), callback_data='user_manual#win_user_manual_text')
        )
        self.keyboard.row(
            InlineKeyboardButton(text=ui.get_text("linux_user_manual"),
                                 callback_data='user_manual#linux_user_manual_text'),
        )

        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='help_meesage'))
        if text_mode:
            settings = LocalSettings()
            self.start_photo = await settings.get_start_photo()
        return self.MSG

    # Сформировать пользовательский мануал - VPN USR MANUAL
    async def vpn_user_manual_page(self,
                                   manual_type: str,
                                   user_id: int,
                                   lang: str,
                                   call_back: bool = False,
                                   ):

        ui = UIControl(user_id=user_id)
        await ui.get_language()
        self.MSG = ui.get_text(manual_type)

        if call_back:
            self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='vpn_manual'))
        return self.MSG







