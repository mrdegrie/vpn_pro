from pydantic import BaseModel
from typing import Optional


# class BaseAnalytical(BaseModel):
#     all_users: Optional[int] = 0
#     global_sum_sale: Optional[int] = 0
#     all_profit: Optional[int] = 0
#     avaliable_balance: Optional[int] = 0
#
#
# class ResponseBasePartnerStats(BaseModel):
#     status: bool
#     msg: Optional[str] = None
#     base_analytical: Optional[BaseAnalytical] = None
