from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from typing import Optional
import qrcode

from configs.config import bot_name, mim_amount_request_pay
# from models.users.user_control import UserControl
from models.base_models.local_settings import LocalSettings
from models.analytical.partner_analytical import BasePartnerAnalytical
from models.backend.partner_endpoints import PartnerAPI
from models.ui.ui_storage import UIControl


class ParnerSystem:
    """Класс партнерской системы: генерация интерфейсов, взаимодействие с ПП"""

    def __init__(self, user_id: Optional[int] = None, lang: Optional[str] = 'ru'):
        self.keyboard = InlineKeyboardMarkup()
        self.MSG: Optional[str] = None
        self.alert_msg: Optional[str] = None
        self.start_message: Optional[str] = None
        self.user_amount: Optional[float] = 0
        self.order_id: Optional[str] = None
        self.qr_path: Optional[str] = None
        self.user_id: int = user_id
        self.lang = lang

    async def start_partner_cabinet(self, user_id: int, lang: str) -> bool:
        """Создать личный кабинет партнереки"""

        # user_control = UserControl()
        ui = UIControl(user_id=user_id)
        setting_control = LocalSettings()
        await ui.get_language()
        # Считаем базовую стату партнера
        base_analytic = BasePartnerAnalytical()
        data = await base_analytic.start_base_partner_analytic(user_id, get_cash=True)
        if data.status:
            stats = data.base_analytical
            #             ref_link_2 = f"<a href='{bot_name}?start={user_id}'>VPN BOT</a>"
            ref_link = f'{bot_name}?start={user_id}'
            shared_link = f'https://telegram.me/share/url?url={ref_link}'
            qr_code = qrcode.make(ref_link)
            qr_path = f'storage/qrs/{user_id}.png'
            qr_code.save(qr_path)

            self.qr_path = qr_path
            self.MSG = ui.get_text("partner_sysstem_general_text").format(
                partner_percent=stats.partner_percent,
                ref_link=ref_link,
                all_users=stats.all_users,
                paid_users_specified_partner=stats.paid_users_specified_partner,
                all_profit="%.2f" % stats.all_profit,
                avaliable_balance="%.2f" % stats.avaliable_balance,
            )

            self.keyboard.row(InlineKeyboardButton(text=ui.get_text("share_a_link"), url=shared_link))
            self.keyboard.row(InlineKeyboardButton(text=ui.get_text("partner_payment_request"),
                                                   callback_data='partner_payment_request'))
        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"),
                                               callback_data='exit_partner_cabonet'))
        self.start_message = await setting_control.get_start_photo()
        return True

    async def cabinet_request_money(self, user_id: int):
        """Создать страницу запроса вывода средств в кабинете партнера"""

        ui = UIControl(user_id=user_id)
        base_analytic = BasePartnerAnalytical()
        data = await base_analytic.start_base_partner_analytic(user_id)
        await ui.get_language()

        if data.status:
            stats = data.base_analytical
            self.MSG = ui.get_text("request_money_text").format(
                avaliable_balance=stats.avaliable_balance,
            )
            if stats.avaliable_balance >= mim_amount_request_pay:
                if ui.language == 'ru':
                    self.keyboard.row(
                        InlineKeyboardButton(text=ui.get_text("request_money#card"), callback_data='request_money#card'),
                        InlineKeyboardButton(text=ui.get_text("request_money#usdt"), callback_data='request_money#usdt'),

                    )
                    self.keyboard.row(
                        InlineKeyboardButton(text=ui.get_text("request_money#qiwi"), callback_data='request_money#qiwi'),
                        InlineKeyboardButton(text=ui.get_text("request_money#youmoney"),
                                             callback_data='request_money#youmoney'),
                    )
                    self.user_amount = stats.avaliable_balance
                else:
                    self.keyboard.row(
                        InlineKeyboardButton(text=ui.get_text("request_money#usdt"),
                                             callback_data='request_money#usdt'),
                        InlineKeyboardButton(text=ui.get_text("request_money#paypal"),
                                             callback_data='request_money#paypal'),

                    )
                    self.user_amount = stats.avaliable_balance
            else:
                self.alert_msg = ui.get_text("not_enough_money_text").format(
                    mim_amount_request_pay=mim_amount_request_pay,
                )
        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"),
                                               callback_data='partner_cabinet'))
        return self.keyboard

    async def get_user_wallet(self, currency: str):
        ui = UIControl(user_id=self.user_id)
        await ui.get_language()
        self.MSG = ui.get_text("get_user_wallet").format(currency=currency_conf(currency, ui=ui))
        self.keyboard.row(InlineKeyboardButton(
            text=ui.get_text("next_start_message"),
            callback_data='partner_payment_request'),
        )
        return self.keyboard

    async def accept_create_new_equest_pament(self, currency: str, user_wallet: str, amount: float):
        """Создать страницу запрос подтверждения на создание нового запроса валюты"""

        ui = UIControl(user_id=self.user_id)
        await ui.get_language()

        self.MSG = ui.get_text("accept_create_new_request_payment").format(
            currency=currency_conv(currency, ui=ui),
            user_wallet=user_wallet,
            amount=amount,
        )
        self.keyboard.row(
            InlineKeyboardButton(text=ui.get_text("yes"), callback_data='accept_create_request_wallet'),
            InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='partner_payment_request'),
        )
        return self.keyboard

    async def create_order_request_paymwnt(self,
                                           user_id: int,
                                           amount: float,
                                           currency: str,
                                           user_wallet: str,
                                           user_name: Optional[str] = None,
                                           ) -> bool:

        ui = UIControl(user_id=self.user_id)
        await ui.get_language()
        api = PartnerAPI()
        result = await api.create_request_payment(user_id=user_id,
                                                  amount=amount,
                                                  currency=currency,
                                                  user_wallet=user_wallet,
                                                  user_name=user_name,
                                                  )
        self.keyboard.row(
            InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='next_start_message'),
        )
        if not result.status:
            self.MSG = ui.get_text("request_creation_error")
            return False

        if result.order_id:
            self.MSG = ui.get_text("successful_application").format(order_id=result.order_id)
            self.order_id = result.order_id
            self.alert_msg = f'⛔️ Новая заявка на выплату!\n\n' \
                             f'<b>#</b><code>{self.order_id}</code>\n' \
                             f'├ User: <b>{user_id}</b>\n' \
                             f'├ Name: <b>{user_name}</b>\n' \
                             f'├ Сумма: <b>${amount}</b>\n' \
                             f'├ Метод: <b>{currency}</b>\n' \
                             f'└ Перевести на: <code>{user_wallet}</code>\n\n' \
                             f'#заявка'
            return True

        if not result.order_id:
            self.MSG = result.msg
            return False

        else:
            self.MSG = ui.get_text("request_creation_error")
            return False


def currency_conf(curency: str, ui: UIControl):
    if ui.language == 'ru':
        conv = {
            "card": ui.get_text("bank_card_number"),
            "usdt": ui.get_text("wallet_number"),
            "qiwi": ui.get_text("qiwi_number"),
            "youmoney": ui.get_text("Yoomoney_number"),
        }
    else:
        conv = {
            "paypal": ui.get_text("paypal_number"),
            "usdt": ui.get_text("wallet_number"),
        }
    try:
        return conv[curency]
    except KeyError:
        return curency


def currency_conv(curency: str, ui: UIControl):
    conv = {
        "card": ui.get_text("Bank_card"),
        "usdt": "USDT (TRC20)",
        "qiwi": "Qiwi",
        "youmoney": "Yoomoney",
    }
    try:
        return conv[curency]
    except KeyError:
        return curency
