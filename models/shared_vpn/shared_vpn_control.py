from typing import Optional, Union
from pydantic import ValidationError
import logging
import operator
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from uuid import UUID

from configs.config import renewal_price
from models.users.user_control import UserControl
from models.backend.shared_vpn_endpoints import SharedOVPNPlans, SharedOVPNConfigs, OVPNItems
from models.backend.trials_endpoints import TrialsAPIv1
from models.backend.shared_vpn_endpoints import VPNTaiffsAPI
from models.shared_vpn import models as shared_models
from models.backend import backend_models
from models.base_models.local_settings import LocalSettings
from models.ui.ui_storage import UIControl
from models.ui.locales.available_localizations import country_lang_conv


class SharedVPNcontrol:
    """Класс Shared VPN models - слой логистической модели впн-а"""

    def __init__(self):
        self.MSG: Optional[str] = None
        self.keyboard = InlineKeyboardMarkup()
        self.config_text: Optional[str] = None
        self._config_name: Optional[str] = None
        self.start_message: Optional[str] = None
        self._config_exp_date: Optional[int] = 30
        self._trial_id: Optional[UUID] = None

        self.result: dict = {}
        self.custom_data = {}
        self.error_message: Optional[str] = None
        self.new_status: bool = False
        self._config: Optional[backend_models.BuyConfigResponse] = None

    @property
    def config(self):
        return self._config

    @property
    def config_name(self):
        return self._config_name

    async def all_target_methods(self, user_id: int):
        """Получить страницу с доступными назначениями OpenVPN"""

        user_control = UserControl()
        setting = LocalSettings()
        ui = UIControl(user_id=user_id)
        await ui.get_language()

        response = await user_control.get_user_info(user_id)
        self.start_message = await setting.get_start_photo()
        if response.user_info and response.status:
            all_items_response = await self.get_item_by_type('target')
            if all_items_response.status and all_items_response.items:
                for item in all_items_response.items:
                    self.result.update(
                        {
                            item.item_id: country_lang_conv(
                                country=item.country_code,
                                lang=ui.language,
                            )
                        }
                    )

                    # self.result.update({item.item_id: item.country.split('#')[0]})
                    self.custom_data.update({item.item_id: item})

        else:
            self.error_message = ui.get_text('user_block_message')

        custom_keyboard = [
            InlineKeyboardButton(text=ui.get_text("shared_vpn_country"), callback_data='shared_vpn_country'),
            InlineKeyboardButton(text=ui.get_text("my_shared_config_list_button"), callback_data='my_shared_config_list'),
            InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data="next_start_message")]

        self.keyboard.row(InlineKeyboardButton(
            text=ui.get_text('next_start_message'), callback_data="next_start_message"),
        )

        return self.result, custom_keyboard, self.custom_data

    async def all_shared_vpn_items(self,
                                   user_id: int,
                                   text_mode: bool = False,
                                   query: str = None,
                                   search_mode=False,
                                   ):
        """
        Сформировать страницу доступных товаров ВПН-а.
        """

        user_control = UserControl()
        setting = LocalSettings()
        ui = UIControl(user_id=user_id)
        language = await ui.get_language()

        response = await user_control.get_user_info(user_id)
        self.start_message = await setting.get_start_photo()

        if response.user_info:
            if response.user_info.status:
                # Пользователь активен - получаем все активные items
                if query:
                    all_items_response = await self.search_country_configs(query=query)
                else:
                    all_items_response = await self.get_item_by_type('country')
                if all_items_response.status and all_items_response.items:
                    for item in all_items_response.items:
                        self.result.update(
                            {
                                item.item_id: country_lang_conv(
                                    country=item.country_code,
                                    lang=language,
                                )
                            }
                        )

                        # self.result.update({item.item_id: item.country.split('#')[0]})
                        self.custom_data.update({item.item_id: item})
            else:
                self.error_message = ui.get_text('user_block_message')
        else:
            self.error_message = ui.get_text("user_block_message")
        if search_mode:
            next_callback = 'shared_vpn'
        else:
            next_callback = 'shared_vpn'

        custom_keyboard = [
            # InlineKeyboardButton(text=ui.get_text("search_country_button"), callback_data='search_country'),
            InlineKeyboardButton(text=ui.get_text("my_shared_config_list_button"),
                                 callback_data='my_shared_config_list'),
            InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data=next_callback)]

        self.keyboard.row(InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data=next_callback))
        return self.result, custom_keyboard, self.custom_data

    # Получить список активных серверов
    async def get_all_active_items(self):
        item_backend = OVPNItems()
        return await item_backend.get_active_items()

    async def get_item_by_type(self, item_type: str):
        item_backend = OVPNItems()
        return await item_backend.get_item_by_type(item_type)

    # Произвести поиск по активных товарам по названию страны
    async def search_country_configs(self, query: str):
        item_backend = OVPNItems()
        return await item_backend.search_sountry(query=query)

    async def server_remote_page(self, item_id: str, user_id: int):
        """ Создать страницу информации о сервере"""

        ui = UIControl(user_id=user_id)
        language = await ui.get_language()
        if not item_id:
            self.error_message = ui.get_text("general_error_message")
            self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='shared_vpn'))
            return self.keyboard

        item_control = OVPNItems()
        taiff_contrrol = VPNTaiffsAPI()

        item_response = await item_control.get_item(item_id)

        if item_response.status:
            # Получим все активные тарифы
            # Так-же, информацию о наличии у пользователя триальной подписки
            tariffs_response = await taiff_contrrol.get_active_tariffs()
            if not tariffs_response.status:
                self.error_message = ui.get_text("general_error_message")
                self.keyboard.row(
                    InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='shared_vpn'))
                return self.keyboard
            tarifs = {}
            for tarriff in tariffs_response.tariffs:
                tarifs.update({tarriff.name: tarriff})

            month_6 = await self._config_price_factory(
                period=tarifs['month_6'].period,
                percent=tarifs['month_6'].percent,
                one_config_price=int(item_response.item.price)
            )
            year_1 = await self._config_price_factory(
                period=tarifs['year_1'].period,
                percent=tarifs['year_1'].percent,
                one_config_price=int(item_response.item.price)
            )
            year_3 = await self._config_price_factory(
                period=tarifs['year_3'].period,
                percent=tarifs['year_3'].percent,
                one_config_price=int(item_response.item.price)
            )

            self.MSG = ui.get_text("tariff_line").format(
                country=country_lang_conv(
                    country=item_response.item.country_code,
                    lang=language,
                ),
                price=item_response.item.price,
                month_6=month_6,
                year_1=year_1,
                year_3=year_3,
            )

            self.keyboard.row(
                InlineKeyboardButton(
                    text=ui.get_text("bay_shared_ovpn_1"),
                    callback_data=f'bay_shared_ovpn#{item_response.item.item_id}#{tarifs["month_1"].tariff_id}'),
                InlineKeyboardButton(
                    text=ui.get_text("bay_shared_ovpn_6"),
                    callback_data=f'bay_shared_ovpn#{item_response.item.item_id}#{tarifs["month_6"].tariff_id}')
            )
            self.keyboard.row(InlineKeyboardButton(
                text=ui.get_text("bay_shared_ovpn_12"),
                callback_data=f'bay_shared_ovpn#{item_response.item.item_id}#{tarifs["year_1"].tariff_id}'),
                InlineKeyboardButton(
                    text=ui.get_text("bay_shared_ovpn_36"),
                    callback_data=f'bay_shared_ovpn#{item_response.item.item_id}#{tarifs["year_3"].tariff_id}')
            )

            trial_api = TrialsAPIv1()
            trial = await trial_api.get_private_trial_user(user_id=user_id)

            if trial.trial:
                self.keyboard.row(
                    InlineKeyboardButton(
                        text=ui.get_text("trial_pay_button").format(days=trial.trial.days),
                        callback_data=f'trial_pay#{item_response.item.item_id}'),
                )
        else:
            self.error_message = ui.get_text("general_error_message")
        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='shared_vpn'))
        return self.keyboard

    async def select_user_device(self, user_id: int):
        """Создать стрницу выбора девайса пользователя"""

        ui = UIControl(user_id=user_id)
        await ui.get_language()
        self.MSG = ui.get_text("user_device_selection")

        self.keyboard.row(
            InlineKeyboardButton(text=ui.get_text("ios_vpn-user_manual"),
                                 callback_data='config_pay#ios_vpn_manual_text'),
            InlineKeyboardButton(text=ui.get_text("android_vpn_user_manual"),
                                 callback_data='config_pay#android_vpn_manual_text'),
        )
        self.keyboard.row(
            InlineKeyboardButton(text=ui.get_text("macos_user_manual"),
                                 callback_data='config_pay#macos_user_manual_text'),
            InlineKeyboardButton(text=ui.get_text("win_user_manual"),
                                 callback_data='config_pay#win_user_manual_text')
        )

        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='shared_vpn'))
        return self.keyboard

    # --- NEW VERSSION CREATE (Buy) CONFIG
    async def buy_config(self,
                         item_id: int,
                         user_id: int,
                         device: str,
                         tariff_id: Optional[int] = None,
                         trial: bool = False,
                         ) -> bool:
        """
        Создать новый конфиг для SHAREED OVPN
        200 - ok. Created config
        404: User/item/tariff not found
        405: Trial not active/Eror deactiate trial
        406: Not enough balance
        407: Error createte config
        """

        config_backend = SharedOVPNConfigs()
        ui = UIControl(user_id=user_id)
        await ui.get_language()

        # Покупаем конфиг:
        response = await config_backend.buy_config(
            item_id=item_id,
            user_id=user_id,
            tariff_id=tariff_id,
            trial=trial,
        )
        if response.status:
            country_name = country_lang_conv(
                country=response.config.country_code,
                lang=ui.language,
            )

            self._config_name = f'{country_name}_{response.config.order_id}.ovpn'
            # Конфиг создан успешно, создаем файл и месседж ответа
            self.MSG = ui.get_text(device)
            self.MSG = self.MSG + ui.get_text("config_inform")
            self.keyboard.row(InlineKeyboardButton(text=ui.get_text("buy_more"), callback_data='bay_vpn_trial_version'))
            self._config = response
            return True

        elif response.status_code == 404:
            # User/item/tariff not found
            # self.MSG = '404'
            self.MSG = ui.get_text("error_create_vpn")
            return False

        elif response.status_code == 405:
            # Trial not active/Eror deactiate trial
            self.MSG = ui.get_text("error_create_vpn")
            # self.MSG = '405'
            return False

        elif response.status_code == 406:
            # Not enough balance
            self.MSG = ui.get_text("no_money_to_buy")
            self.keyboard.row(InlineKeyboardButton(
                text=ui.get_text("all_payments"),
                callback_data='all_payments'),
            )
            return False

        elif response.status_code == 407:
            # Error createte config
            self.MSG = '407'
            return False

        else:
            # Eror not found (!!)
            self.MSG = ui.get_text("error_create_vpn")
            return False

    # Создать страницу выпущенных конфигов юзера
    async def get_my_configs(self, user_id: int, lang: str):
        config_backend = SharedOVPNConfigs()
        ui = UIControl(user_id=user_id)
        await ui.get_language()

        response = await config_backend.get_all_user_config(user_id)
        if response.status:
            if response.result:
                for config in response.result:
                    country_name = country_lang_conv(
                        country=config.country,
                        lang=ui.language,
                    )
                    self.result.update(
                        {config.order_id: f'{country_name}*{config.order_id}'})
                    self.custom_data.update({config.order_id: config})
                    self.MSG = ui.get_text("overview_configs")
            else:
                self.error_message = ui.get_text("no_vpn_purchased")
                self.keyboard.row(InlineKeyboardButton(text=ui.get_text("shared_vpn"), callback_data='shared_vpn'))
        else:
            self.error_message = ui.get_text("backend_error")
            logging.warning(f'Error back response: {response.msg}')

        custom_keyboard = [
            InlineKeyboardButton(text=ui.get_text("shared_vpn"), callback_data='shared_vpn'),
            InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='private_cabinet')]
        # self.keyboard.row(InlineKeyboardButton(text='🛡 Купить VPN', callback_data='shared_vpn'))
        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='private_cabinet'))
        if self.result:
            self.result = dict(sorted(self.result.items(), key=operator.itemgetter(1)))
        return self.result, custom_keyboard, self.custom_data

    # Формируем страницу управления конфигом
    async def get_config_remote_control(self,
                                        user_id: int,
                                        config_data,
                                        order_id: int,
                                        update_mode: Optional[bool] = False,
                                        update_status: Optional[bool] = None
                                        ):
        ui = UIControl(user_id=user_id)
        await ui.get_language()
        data: Optional[shared_models.ConfigsData] = None

        try:
            data = shared_models.ConfigsData(configs=config_data)
        except ValidationError as err:
            logging.warning(err.json())

        if data:
            config_info = data.configs[order_id]
            auto_payer = config_info.auto_update
            if update_mode:
                auto_payer = update_status
            country_name = country_lang_conv(
                country=config_info.country,
                lang=ui.language,
            )
            self.MSG = ui.get_text("config_overview_text").format(
                country=country_name,
                country_2=country_name,
                ovpn_user_name=config_info.order_id,
                order_time=config_info.order_time.date().strftime("%d.%m.%Y"),
                exp_time=config_info.exp_time.date().strftime("%d.%m.%Y"),
                renewal_price=renewal_price,
                auto_payer=auto_update_conv(auto_payer),
            )
            self.keyboard.row(
                InlineKeyboardButton(
                    text=f'{auto_update_conv(auto_payer, mode=2)} {ui.get_text("to_extend_button")}',
                    callback_data=f'auto_update_select#{config_info.order_id}'),
                InlineKeyboardButton(
                    text=ui.get_text("download"), callback_data=f'download_shared_config#{config_info.order_id}'))

            self.keyboard.row(
                InlineKeyboardButton(text=ui.get_text('refrash_config'), callback_data='refrash_config'),
                InlineKeyboardButton(text=ui.get_text('dell_config'), callback_data='dell_config'),

            )
        else:
            self.error_message = ui.get_text("error_read_file")

        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"),
                                               callback_data='my_shared_config_list'))
        return self.keyboard

    # Удалить конфиг впн
    async def delete_ovpn_config(self, ovpn_transaction_id: int):
        config_backend = SharedOVPNConfigs()
        response = await config_backend.dell_config_ovpn(ovpn_transaction_id)
        if response.status:
            self.MSG = '✅ Успешно!'
        else:
            self.MSG = '<b>❌ Ошибка удаления</b>'
        return response

    # Получить конфиг по ID
    async def get_config_text(self, user_id: int, order_id: int) -> shared_models.SharedVPNtransaction or bool:
        config_backend = SharedOVPNConfigs()
        response = await config_backend.get_all_user_config(user_id)
        if response.status:
            if response.result:
                try:
                    for config in response.result:
                        if config.order_id == order_id:
                            return config
                    return False
                except KeyError:
                    return False
            else:
                return False
        else:
            return False

    # Переключить статус автооплаты конфига
    async def select_config_auto_update(self, order_id: int, user_id: int, lang: str):
        ui = UIControl(user_id=user_id)
        await ui.get_language()

        api = SharedOVPNConfigs()
        response = await api.update_autopayer(order_id)
        if response.status:
            if response.new_status:
                self.MSG = ui.get_text("enabling_auto_renewal")
                self.new_status = response.new_status
                return True
            else:
                self.MSG = ui.get_text("disablet_auto_renewal")
                self.new_status = response.new_status
                return True
        else:
            self.MSG = '⛔️ Error'
            return False

    async def search_country_page(self, user_id: int):
        """Создать страницу поиска геолокации по названию"""
        ui = UIControl(user_id=user_id)
        await ui.get_language()

        self.MSG = ui.get_text('search_country_text')

        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='shared_vpn'))
        return self.keyboard

    async def _config_price_factory(self,
                                    period: int,
                                    percent: int,
                                    one_config_price: int,
                                    ) -> Union[int, float]:

        if period == 1:
            return one_config_price
        else:
            item_price: float = round(
                (one_config_price * (period / 30)) - (((one_config_price * (period / 30)) / 100) * percent), 1,
            )
            return item_price

    def period_gen(self):
        """Произвести конвертацию периода VPN тарифа из дней в словесноое представление
        Пример: 30 ->1мес, 60->2мес,
        """


def auto_update_conv(status: bool, mode: int = 1) -> str or bool:
    if mode == 1:
        conv = {True: '✅', False: '⛔️'}
        return conv[status]
    if mode == 2:
        conv = {True: '✅', False: '⛔️'}
        return conv[status]
    else:
        return status
