from pydantic import BaseModel
from typing import Optional, Dict

from database.data_models import SharedVPNtransaction


class BaseResponse(BaseModel):
    status: bool
    msg: Optional[str] = None


class ConfigsData(BaseModel):
    configs: Optional[Dict[int, SharedVPNtransaction]] = None

