from aiogram.dispatcher.middlewares import BaseMiddleware
from aiogram import types
from cachetools import TTLCache  # https://cachetools.readthedocs.io/en/stable/
from aiogram.dispatcher.handler import CancelHandler


class ThrottleMiddleware(BaseMiddleware):
    async def on_process_callback_query(self, call: types.CallbackQuery, data: dict):
        cache = TTLCache(maxsize=float('inf'), ttl=1)
        if not cache.get(call.message.message_id):
            cache[call.message.message_id] = True
            return
        else:
            raise CancelHandler
