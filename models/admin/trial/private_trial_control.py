from typing import Optional, List
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

from models.backend.user_endpoints import UserAPIv1
from models.backend.trials_endpoints import TrialsAPIv1
from database.data_models import User


class PrivateTrialControl:
    def __init__(self):
        self.MSG: Optional[str] = None
        self.keyboard = InlineKeyboardMarkup()
        self.status: bool = False
        self.user_count = 0
        self.users: Optional[List[User]] = None

    async def select_activate_mode(self):
        """Создать старницу выбора метода сортировки юзеров"""

        self.MSG = '<b>Выбирите метод сортировки юзеров 👇</b>'
        self.keyboard.row(InlineKeyboardButton(text='🏄🏻 По активности', callback_data='private_trial_activae_sort'))
        self.keyboard.row(InlineKeyboardButton(text='🆔 Указанному юзеру', callback_data='private_trial_id_sotr'))
        self.keyboard.row(InlineKeyboardButton(text='🔙 Назад', callback_data='admin_shared_vpn_remote'))
        return self.keyboard

    async def get_active_pattern_mailing(self):
        """Формируем патерны выборки по активности"""

        self.keyboard.row(InlineKeyboardButton(text='🐔 Юзерам без покупок', callback_data='private_trial#no_purchases'))
        self.keyboard.row(InlineKeyboardButton(text='🔙 Назад', callback_data='add_private_trials'))
        self.MSG = '<b>Выберите критерий выборки юзеров для триалки 👇</b>'
        return self.keyboard

    async def get_users_for_private_trial(self, pattern: str):
        """Получить пользователей подходящих для активации триалки по указанному паттерну"""

        api = UserAPIv1()
        response = await api.get_users_of_private_trial_activate(pattern=pattern)
        if response.status:
            if response.users:
                if len(response.users) > 0:
                    self.MSG = f'Для триала доступно: <b>{len(response.users)} чел</b>\n\n' \
                               f'<b>➡️ Укажите на сколько дней активируем триал?</b>'
                    self.keyboard.row(
                        InlineKeyboardButton(text='1д', callback_data='trial_period#1'),
                        InlineKeyboardButton(text='2д', callback_data='trial_period#2'),
                        InlineKeyboardButton(text='3д', callback_data='trial_period#3'),
                    )
                    self.keyboard.row(
                        InlineKeyboardButton(text='4д', callback_data='trial_period#4'),
                        InlineKeyboardButton(text='5д', callback_data='trial_period#5'),
                    )
                    self.status = True
                    self.user_count = len(response.users)
                    self.users = response.users
                else:
                    self.MSG = f'<b>Нет юзеров по данному паттерну</b>'
            else:
                self.MSG = f'<b>Нет юзеров по данному паттерну</b>'
        else:
            self.MSG = f'Ошибка запроса юзеров'

        self.keyboard.row(InlineKeyboardButton(text='🔙 Назад', callback_data='add_private_trials'))
        return self.keyboard

    async def accept_create_trial_activate(self, days: int, users: int):
        """Создать страницу подтверждения создания активации триалки"""

        self.MSG = f'⛔️ Вы собираетесь активировать триальный период <b>для {users} чел на {days} чел</b>\n\n' \
                   f'➡️ <b>Активировать?</b>'
        self.keyboard.row(InlineKeyboardButton(text='🟢 Да', callback_data='private_trial_activate'))
        self.keyboard.row(InlineKeyboardButton(text='🔙 Отмена', callback_data='add_private_trials'))
        return self.keyboard

    async def trial_activate(self, days: int, pattern: str):
        """Активировать триал доступ"""

        api = TrialsAPIv1()
        response = await api.activate_private_trial(pattern=pattern, days=days)
        if not response.status:
            self.MSG = f'<b>Ошибка активации:</b> <i>{response.msg}</i>'
            self.keyboard.row(InlineKeyboardButton(text='🔙 Назад', callback_data='add_private_trials'))
            return self.keyboard

        self.MSG = f'✅ Триал успешно добавлен {response.users_count} людям.\n\n' \
                   f'➡️ <b>Произвести рассылку уведомлений этим юзерам?</b>'

        self.keyboard.row(InlineKeyboardButton(text='📮 ДА!', callback_data='start_trial_mailing'))
        self.keyboard.row(InlineKeyboardButton(text='🔙 Отмена', callback_data='add_private_trials'))
        return self.keyboard



