from typing import Optional, Union
import aioredis
from pydantic import ValidationError
from aioredis import Redis
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

from models.backend.analyticks_endpoints import AnalyticAPI
from models.backend.backend_models import BaseStatsResponse
from models.uttils.data_scales import DataScale
from database.db_connected import get_redis


class Analyticks:
    """Класс аналитики"""

    def __init__(self):
        self.MSG: Optional[str] = None
        self.keyboard = InlineKeyboardMarkup()
        self.stats_img_url = 'storage/scales/users.png'
        self.scales_mode: bool = False

    async def start_base_admin_analytic(self) -> Union[InlineKeyboardMarkup, None]:
        """Страница базовой аналитики админки"""

        analytica = AnalyticAPI()
        redis: Redis = await get_redis()
        base_analytic = await redis.get(name=f'admin_base_stats')
        if base_analytic is None:
            stats = await analytica.get_base_stats()
            await redis.set(name='admin_base_stats', value=stats.json(), ex=1200)
            await redis.close()
        else:
            try:
                stats = BaseStatsResponse.parse_raw(base_analytic)
            except ValidationError:
                await redis.close()
                return None

        if stats.base_stats:
            base_stats = stats.base_stats
            # Отрисовываем график статы
            scales = DataScale(stats=stats)
            if await scales.user_history_reg_scale():
                self.scales_mode = True

            trial_configs_percent = (base_stats.trial_configs / (base_stats.total_number_trilas / 100))
            after_the_trial_percent = base_stats.count_purchases_after_the_trial / (base_stats.trial_configs / 100)
            trial_conversion = base_stats.count_purchases_after_the_trial / (base_stats.total_number_trilas / 100)
            global_conversion = (base_stats.paid_users - base_stats.count_purchases_after_the_trial) / \
                                (base_stats.allusers / 100)

            self.MSG = '<b>🧧 БАЗОВАЯ СТАТА\n\n' \
                       '├ Общая</b>\n' \
                       f'├ Всего пользователей: <b>{base_stats.allusers}</b>\n' \
                       f'├ Активных пользователей: <b>{base_stats.active_users}</b>\n' \
                       f'├ Пополнений: <b>${base_stats.all_donats} ({base_stats.paid_users})</b>\n' \
                       f'├ За сегодня: <b>${base_stats.one_day_donats} ({base_stats.unique_users_donats_today})</b>\n' \
                       f'├ За вчера: <b>${base_stats.nex_day_sales} ({base_stats.unique_users_donats_yesterday})</b>\n' \
                       f'├ За мес: <b>${base_stats.current_month_donats} ({base_stats.unique_users_donats_tomonth})</b>\n' \
                       f'├ За прошлый: $<b>{base_stats.last_month_donats} ({base_stats.unique_users_donats_last_month})</b>\n' \
                       f'├ Уникальных пополнений: <b>{base_stats.paid_users}</b>\n' \
                       f'└ Реф.начисления: $<b>{base_stats.referral_payments}</b>\n\n' \
                       f'├ <b>SHARED VPN</b>\n' \
                       f'├ Всего создано конфигов: <b>{base_stats.shared_vpn_all_sales_count}</b>\n' \
                       f'├ Активно конфигов: <b>{base_stats.active_shared_configs}</b>\n' \
                       f'├ Отправлено триалок: <b>{base_stats.total_number_trilas}</b>\n' \
                       f'├ Активировано: <b>{base_stats.trial_configs} ({"%.2f" % trial_configs_percent}%)</b>\n' \
                       f'├ Людей продлило VPN: <b>{base_stats.count_extension_vpn}</b>\n' \
                       f'├ Перестали продлевать: <b>{base_stats.departed_users}</b>\n' \
                       f'├ Покупок от активированых:\n' \
                       f'    └ <b>{base_stats.count_purchases_after_the_trial} ' \
                       f'({"%.2f" % after_the_trial_percent}%) на ${base_stats.sum_purchases_after_the_trial}</b>\n' \
                       f'├ Конверсия триальной системы:\n' \
                       f'    └ <b>{"%.2f" % trial_conversion}%</b>\n' \
                       f'└ Конверсия прямых покупок:\n' \
                       f'    └<b>{"%.2f" % global_conversion}%</b>\n'

            self.keyboard.row(InlineKeyboardButton(text='🌍 Геолокации', callback_data='admin_vpn_country_stats'))

        else:
            self.MSG = '<b>Ошибка на стороне бекенда!</b>'

        self.keyboard.row(InlineKeyboardButton(text='🔄 Обновить', callback_data='refrash_stats'))
        self.keyboard.row(InlineKeyboardButton(text='🔙 Назад', callback_data='back_admin'))
        return self.keyboard

