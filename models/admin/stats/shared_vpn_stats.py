from typing import Optional
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

from models.backend.analyticks_endpoints import AnalyticAPI
from models.base_models.local_settings import LocalSettings
from models.uttils.data_scales import DataScale


class SharedVPNStats:
    """Статистика Shared VPN"""

    def __init__(self):
        self.MSG: Optional[str] = None
        self.start_img: Optional[str] = None
        self.keyboard = InlineKeyboardMarkup()
        self.country_stats_img_url = 'storage/scales/country.png'
        self.svg_path = 'storage/scales/country.svg'
        self.scales_mode: bool = False

    async def vpn_country_stats(self):
        """Аналитика использования геолокаций VPN"""

        stats_api = AnalyticAPI()
        response = await stats_api.get_vpn_country_stats()
        scales_data = []
        if not response.status:
            self.keyboard.row(InlineKeyboardButton(text='🔙 Назад', callback_data='refrash_stats'))
            self.MSG = '⛔️ Ошибка получения данных аналитики.. 😞'
            return self.keyboard
        result = response.country_stats.country_config_counts

        self.MSG = '<b>Аналитика по гео</b>\n\n' \
                   '<b>Активные конфиги:</b>\n'
        active_configs = 0
        for stats in result:
            self.MSG += f'├ {stats.country}: <b>{stats.count}</b>\n'
            scales_data.append((stats.count, stats.country))
            active_configs += stats.count
        self.MSG += f'\n├ Всего конфигов: <b>{active_configs}</b>\n' \
                    f'├ Всего стран: <b>{len(result)}</b>'

        self.keyboard.row(InlineKeyboardButton(text='🔄 Обновить', callback_data='refrash_country_stats'))
        self.keyboard.row(InlineKeyboardButton(text='🔙 Назад', callback_data='refrash_stats'))
        scales_utils = DataScale()
        await scales_utils.render_bars(data=scales_data,
                                       title='Use of countries',
                                       svg_path=self.svg_path,
                                       img_path=self.country_stats_img_url)
        if scales_utils.Status:
            self.scales_mode = True

        return self.keyboard
