# from pydantic import ValidationError
from typing import Optional
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

# from models.base_models.local_settings import LocalSettings
from models.backend.analyticks_endpoints import AnalyticAPI


class AdminControl:
    """Admin control class"""

    def __init__(self):
        self.MSG: Optional[str] = None
        self.keyboard = InlineKeyboardMarkup()
        self.header_image: Optional[str] = None

    # Получить главое меню админки
    async def get_general_admin_window(self):
        self.MSG = '<b>VPN-PRO ADMIN v.0.1\n\n' \
                   '<i>Добро пожаловать, приятной работы 💼</i></b>'
        self.keyboard.row(InlineKeyboardButton(text='📈 Аналитика', callback_data='admin_analyticks'))
        self.keyboard.row(InlineKeyboardButton(text='🛡 Управление Shared VPN', callback_data='admin_shared_vpn_remote'))
        self.keyboard.row(
            InlineKeyboardButton(text='💰 Платежки', callback_data='admin_payments'),
            InlineKeyboardButton(text='⚙️ Настройки', callback_data='admin_settings'),
        )
        self.keyboard.row(
            InlineKeyboardButton(text='🤑 Партнерка', callback_data='admin_partner_system'),
            InlineKeyboardButton(text='👥 Пользователи', callback_data='admin_user_remote'),

        )
        self.keyboard.row(
            InlineKeyboardButton(text='📨 Рассылка', callback_data='admin_mainling'),
            InlineKeyboardButton(text='⚙️ Утилиты', callback_data='admin_unils'),
        )
        self.keyboard.row(InlineKeyboardButton(text='🔙 Выход', callback_data='exit_admin'))

        # setting = LocalSettings()
        # self.header_image = await setting.get_start_photo()
        return self.keyboard

    async def shared_vpn_settings_page(self):
        """Получить страницу управления SHARED VPN"""

        self.MSG = '<b>Управление Shared VPN</b>'
        self.keyboard.row(InlineKeyboardButton(text='🆓 Добавить приват триал', callback_data='add_private_trials'))
        self.keyboard.row(InlineKeyboardButton(text='🔙 Назад', callback_data='start_admin'))
        return self.keyboard


