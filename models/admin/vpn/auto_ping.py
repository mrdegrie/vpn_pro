# import logging
# import asyncio
# from typing import Optional
# from datetime import datetime, timedelta
from aiogram import Dispatcher

from configs import config
from models.admin.vpn.admin_vpn_control import AdminVPNControl


async def server_auto_ping(dp: Dispatcher):
    api = AdminVPNControl()
    msg = await api.ping_vpn_servers(hiden_mode=True)
    for system_admin in config.system_alerts:
        await dp.bot.send_message(chat_id=system_admin, text=msg)
    return
