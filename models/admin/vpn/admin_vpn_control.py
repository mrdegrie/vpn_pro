from typing import Optional, List
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

from models.backend.shared_vpn_endpoints import SharedOVPNServers


class AdminVPNControl:
    """Админский контрол VPN"""

    def __init__(self):
        self.keyboard = InlineKeyboardMarkup()
        self.MGS: Optional[str] = None

    async def ping_vpn_servers(self, hiden_mode: bool = False):
        """Произвесьти ping и деактивацию неакктивных серверов"""

        server_api = SharedOVPNServers()
        ping = await server_api.ping_vpn_servers()
        if not ping.ping_result:
            self.MGS = f'⛔️ <b>Ошибка пинга!</b> <i>#{ping.msg}</i>'

        self.MGS = '✅ <b>Ping произведен успешно!</b>\n\n'
        for result in ping.ping_result:
            if not hiden_mode:
                self.MGS += f'├ <b>{result.country}: {status_conv(result.status)}</b>\n'
            else:
                if not result.status:
                    self.MGS += f'├ <b>{result.country}: {status_conv(result.status)}</b>\n'

        return self.MGS


def status_conv(status: bool):
    conv = {True: '🟢 Актив', False: '🔴 Деактив'}
    return conv[status]
