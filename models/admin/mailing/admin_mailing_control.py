import asyncio
import logging
# from datetime import datetime, timedelta
from aiogram import types, exceptions
from aiogram.types import CallbackQuery
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from typing import Optional, List, Union

from configs.config import admins, support_link
from models.backend.user_endpoints import UserAPIv1
from database.data_models import User
from models.ui.ui_storage import UIControl


# Класс расслок по базе клиентов
class Mailing:
    def __init__(self,
                 user_id: Optional[int] = None,
                 user_name: Optional[str] = None,
                 text_meiling: Optional[str] = None,
                 dp: Optional[CallbackQuery] = None,
                 msg: Optional[str] = None,
                 pattern: Optional[str] = None,
                 disable_notification: Optional[bool] = False,
                 keyboards: Optional[List[InlineKeyboardButton]] = None,
                 mode: Optional[str] = 'mailing',
                 trial_users: Optional[List[User]] = None,
                 text_mode: Optional[str] = None,
                 ):

        self.UserID = user_id
        self.UserName = user_name
        self.TextMeiling = text_meiling
        self.Keyboard = types.InlineKeyboardMarkup()
        self.DP = dp
        self.DisableAlert = disable_notification
        self.MSG = msg
        self.Pattern = pattern
        self._trial_users: Optional[List[User]] = trial_users
        self._keyboad: Optional[List[InlineKeyboardButton]] = keyboards
        self._mailing_keyboad: Optional[InlineKeyboardMarkup] = None
        self._mode: str = mode

        # --- send message errros status
        self.BodBlocked = 0
        self.InvalidUserID = 0
        self.SuccessDendMessage = 0
        self.UserBacket = []  # Список пользоватеелй которые необходимо удалить из БД после ррассылки
        self._text_mode = text_mode

        self.Message = ''

    # Формируем админский лаунчер
    async def admin_launch_create(self):
        if self.UserID in admins:
            self.Keyboard.row(
                types.InlineKeyboardButton(text='🏄🏻 По активности', callback_data='malinig_active_gradation'))

            self.Keyboard.row(types.InlineKeyboardButton(
                text='🛍 По товарам', callback_data='mailing_items_sale_gradation'))

            self.Keyboard.row(types.InlineKeyboardButton(
                text='📦 Заготовленные', callback_data='pre_mainling'))

            self.Keyboard.row(types.InlineKeyboardButton(text='🔙 Назад', callback_data='start_admin'))

            self.Message = '<b>🧧 SMART MAILING\n\n' \
                           '→ Выберете основной паттерн рассылки:</b>'
            return self.Keyboard

    async def pre_mailing_page(self):
        if self.UserID in admins:
            self.Keyboard.row(
                types.InlineKeyboardButton(text='Постянным, напомнить о рефке',
                                           callback_data='pre_mainl#active_users'))
            self.Keyboard.row(
                types.InlineKeyboardButton(text='Тег переставших платить',
                                           callback_data='pre_mainl#stopped_paying'))
            self.Keyboard.row(types.InlineKeyboardButton(text='🔙 Назад', callback_data='start_admin'))
            self.Message = '<b>🧧 SMART MAILING\n\n' \
                           '→ Какую рассылку выбырать? </b>'
            return self.Keyboard
        return False

    # Формируем патерны рассыки по активности
    async def get_active_pattern_mailing(self):
        self.Keyboard.row(
            types.InlineKeyboardButton(text='⏱ Юзерам без покупок', callback_data='active_mailing#users_no_purchases'))
        self.Keyboard.row(
            types.InlineKeyboardButton(text='⏱ Юзерам купившим что-либо', callback_data='active_mailing#yes_purchases'))
        self.Keyboard.row(types.InlineKeyboardButton(text='🔙 Назад', callback_data='admin_mainling'))
        self.Message = '<b>Выберете критерий выборки юзеров для рассылки 👇</b>'
        return self.Keyboard

    # Метод старта рассылки
    async def start_mailing(self):

        count, errors = 0, 0
        # Получаем выборку пользователей относительно выбранного патерна рассылки
        user_base = await self.pattern_factory(pattern=self.Pattern)
        sleep_alert = 0
        if user_base:
            ui = UIControl(6666)
            await ui.open_redis_connection()
            for index, user in enumerate(user_base, start=1):
                sleep_alert += index
                if is_int(sleep_alert / 100):
                    await self.stat_alert()
                    await asyncio.sleep(10)

                if user.user_id not in admins:
                    try:
                        await ui.get_language_for_mainling(user.user_id)
                        message = await self.message_factory(user_id=user.user_id, ui=ui)
                        await self._keyboard_factoy(ui)  # Формируем клавиатуру для рассылки
                        if not message:
                            continue

                        await self.send_message(user.user_id, message)
                        await asyncio.sleep(.35)  # 20 messages per second (Limit: 30 messages per second)
                    except exceptions.BadRequest:
                        errors += 1
                        await asyncio.sleep(.45)

            await ui.close_redis_connection()
        keyboard = types.InlineKeyboardMarkup()
        keyboard.row(types.InlineKeyboardButton(text="◀", callback_data="start_admin"))
        self.Keyboard = keyboard
        self.Message = '<b>Рассылка заверешна!</b>\n\n' \
                       f'<b>✅ Успешно доставлено: <code>{self.SuccessDendMessage}</code> сообщений</b>\n' \
                       f'<b>🐓 Заблокировали бота: <code>{self.BodBlocked}</code> чел</b>\n' \
                       f'<b>☠️ Невалидных пользователей: <code>{self.InvalidUserID}</code>\n</b>'
        return self.Keyboard

    # Метод уедомления Админа о сне
    async def sleep_alert(self):
        try:
            await self.DP.bot.edit_message_text(
                chat_id=self.UserID,
                message_id=self.MSG,
                text='<b>ХОД РАССЫЛКИ:</b>\n\n'
                     f'<b>✅ Успешно доставлено: <code>{self.SuccessDendMessage}</code> сообщений</b>\n'
                     f'<b>🐓 Заблокировали бота: <code>{self.BodBlocked}</code> чел</b>\n'
                     f'<b>☠️ Невалидных пользователей: <code>{self.InvalidUserID}</code></b>\n'
                     f'<b>😴 Сплю 10 сек чтобы меня не банило..😴</b>',
                parse_mode='html'
            )
        except exceptions.MessageNotModified:
            pass

    # Алерт админу о состоянии рассылки
    async def stat_alert(self):
        try:
            await self.DP.bot.edit_message_text(
                chat_id=self.UserID,
                message_id=self.MSG,
                text='<b>ХОД РАССЫЛКИ:</b>\n\n'
                     f'<b>✅ Успешно доставлено: <code>{self.SuccessDendMessage}</code> сообщений</b>\n'
                     f'<b>🐓 Заблокировали бота: <code>{self.BodBlocked}</code> чел</b>\n'
                     f'<b>☠️ Невалидных пользователей: <code>{self.InvalidUserID}</code>\n</b>',
                parse_mode='html')
        except exceptions.MessageNotModified:
            pass

    async def pattern_factory(self, pattern) -> Union[bool, List[User]]:
        """Получаем генератор пользователей относительно выбраного паттерна"""

        result_user_base = None
        user_api = UserAPIv1()
        users = {}

        if self._mode == 'mailing':
            response = await user_api.get_users_of_mailing(pattern=pattern)
            if response.status:
                users_of_mailing = response.users
            else:
                return False

        elif self._mode == 'private_trial_mailing':
            users_of_mailing = self._trial_users
        else:
            return False

        # for user in users_of_mailing:
        #     users.update({user.user_id: user.user_id})
        # result_user_base = user_generation(users)

        # return result_user_base
        return users_of_mailing

    async def _keyboard_factoy(self, ui: Optional[UIControl] = None):
        """Фабрика сборки клавиауты"""

        if not self._keyboad:
            # Если клавиатура не передана в конструктор, проверяем есть ли она в UI по моду
            if self._text_mode == 'start_trial_mailing':
                text_kayborad = ui.get_text("bay_vpn_trial_version")
                if not text_kayborad:
                    return False
                self._mailing_keyboad = InlineKeyboardMarkup()
                self._mailing_keyboad.row(
                    InlineKeyboardButton(text=text_kayborad, callback_data='bay_vpn_trial_version'),
                )
                return True
            return False

        self._mailing_keyboad = InlineKeyboardMarkup()
        for kayboard in self._keyboad:
            self._mailing_keyboad.row(kayboard)
        return True

    async def message_factory(self, ui: UIControl, user_id: int = None) -> Union[bool, str]:

        if not self._text_mode:
            return self.TextMeiling

        elif self._text_mode == 'pre_mailing_active_users':
            """Рассылка по активным юзерам с напоминанием о рефке"""
            return ui.get_text("pre_mailing_active_users").format(user_id=user_id)

            # return "Привет, дорогой друг!\n\n" \
            #        "В первую очередь хотим поблагодарить тебя за то, что используешь наш VPN.\n\n" \
            #        "Во-вторых, сообщаем, что у нас есть раздел «Партнерская программа» с удобным QR-кодом, " \
            #        "который ты можешь скинуть другу и зарабатывать по 10% с каждого его пополнения. " \
            #        "Деньги впоследствии можно вывести либо потратить на оплату услуг в боте.\n\n" \
            #        "⬇️️ Твоя реферальная ссылка:\n" \
            #        f"└ https://t.me/YouFast_vpn_bot?start={user_id}"

        elif self._text_mode == 'pre_mailing_stopped_paying':
            return ui.get_text("pre_mailing_stopped_paying").format(support_link=support_link)

        elif self._text_mode == 'start_trial_mailing':
            return ui.get_text("trial_send_text").format(
                days=5,
            )
        else:
            return False

    # Метод отправки сообщеия
    async def send_message(self, user_id: int, message: str) -> bool:
        try:
            await self.DP.bot.send_message(user_id, message, reply_markup=self._mailing_keyboad)
        except exceptions.BotBlocked:
            logging.error(f"Target [ID:{user_id}]: blocked by user")
            self.BodBlocked += 1
            return False
        except exceptions.ChatNotFound:
            logging.error(f"Target [ID:{user_id}]: invalid user ID")
            self.InvalidUserID += 1
            self.UserBacket.append(user_id)
            return False
        except exceptions.RetryAfter as e:
            logging.error(f"Target [ID:{user_id}]: Flood limit is exceeded. Sleep {e.timeout} seconds.")
            await asyncio.sleep(e.timeout)
            await asyncio.sleep(.10)
            return await self.send_message(user_id=user_id, message=message)  # Recursive call
        except exceptions.UserDeactivated:
            logging.error(f"Target [ID:{user_id}]: user is deactivated")
            self.InvalidUserID += 1
            self.UserBacket.append(user_id)
            return False
        except exceptions.TelegramAPIError:
            logging.exception(f"Target [ID:{user_id}]: failed")
            return False
        except exceptions.CantTalkWithBots:
            logging.exception(f"BOT [ID:{user_id}]: failed")
            return False
        except:
            self.BodBlocked += 1
            return False
        else:
            logging.info(f"Target [ID:{user_id}]: success")
            self.SuccessDendMessage += 1
            return True

    async def create_amount_base_active_mailing(self) -> int:
        """
        Посчитать количество пользователей которые получат
        сообщения относительного выбранного паттерна рассылки

        """
        users = await self.pattern_factory(self.Pattern)
        users_count = 0
        if users:
            users_count = len(users)
            # for users, user_base in enumerate(user_base, start=1):
            #     users += 1
        self.Message = f'<b><code>📨 Рассылка</code>\n🚀 Подходящих юзеров: {users_count} чел\n\n</b>'
        return users_count

    def clean(self):
        """Очистить глобальные свойства класса"""

        self.UserID = None
        self.UserName = None
        self.TextMeiling = None
        self.Keyboard = None
        self.DP = None
        self.DisableAlert = None
        self.MSG = None
        self.Pattern = None
        self._trial_users = None
        self._keyboad = None
        self._mailing_keyboad = None
        self._mode = None

        # --- send message errros status
        self.BodBlocked = 0
        self.InvalidUserID = 0
        self.SuccessDendMessage = 0
        self.UserBacket = []  # Список пользоватеелй которые необходимо удалить из БД после ррассылки

        self.Message = ''
        return


def user_generation(users_list):
    yield from users_list


def is_int(n):
    return int(n) == float(n)
