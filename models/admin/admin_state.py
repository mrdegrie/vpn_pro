from aiogram.dispatcher.filters.state import State, StatesGroup


class MailingState(StatesGroup):
    """Модуль рассыки -- MAILING"""
    CREATE_MESSAGE_TEXT_ACTIVE_PATTERN = State()
    START_MAILING = State()
    START_PRE_MAILING = State()


class PrivateTrialState(StatesGroup):
    """Стейты модуля приват триалки"""

    SELECT_ACTIVE_PATTERN = State()
    SELECT_DAYES = State()
    TRIAL_ACTIVATE = State()
    TRIAL_MAILIG = State()



