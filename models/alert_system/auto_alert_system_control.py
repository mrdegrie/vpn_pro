# from pydantic import ValidationError
from typing import Optional
from datetime import datetime, timedelta
# from random import randint
import logging
from aiogram.types import InlineKeyboardMarkup
from aiogram import Dispatcher
from apscheduler.schedulers.asyncio import AsyncIOScheduler

# from configs.config import support
# from models.alert_system import alert_system_models as alert_models
from models.backend.payment_endpoints import PaymentAPI
from models.backend.shared_vpn_endpoints import SharedOVPNConfigs as ConfigAPI
from models.alert_system.mailing import AutoMailing
from models.backend.trials_endpoints import TrialsAPIv1
from models.backend.user_endpoints import UserAPIv1


class AutoAlertSystem:
    """Модуль авоматичиской персонализирванной рассылки"""

    def __init__(self):
        self.MSG: Optional[str] = None
        self.keyboard = InlineKeyboardMarkup()
        self.result_report = ''

    def task_starter(self, dp: Dispatcher, scheduler: AsyncIOScheduler):
        """Метод добавления scheduler тасков"""
        scheduler.add_job(self.unpaid_bill_alerts, 'interval', seconds=7200, args=(dp,))
        scheduler.add_job(
            self.notification_of_disconnection,
            'cron',
            day_of_week='mon-sun',
            hour=18, minute=53,
            args=(dp,),
            timezone='Europe/Moscow',
        )
        scheduler.add_job(
            self.trial_not_active_nitification,
            'cron',
            day_of_week='wed',
            hour=12, minute=10,
            args=(dp,),
            timezone='Europe/Moscow',
        )
        # scheduler.add_job(self.trial_not_active_nitification, 'interval', seconds=30, args=(dp,))

        logging.warning('Tasks AutoAlertSystem succes started! ')

    # --------------------- METHODS -----------------

    async def unpaid_bill_alerts(self, dp: Dispatcher):
        """Метод делает рассылку по пользователям создавшим платеж, но не оплатившим его"""

        pay_api = PaymentAPI()

        temp_transaction = await pay_api.get_temp_transactions(period=3)
        self.result_report += 'Рассылка по 3х часовым временным транзам:\n'
        if temp_transaction.status:
            # Делаем рассылку по бюзерам + отчет админу
            auto_mailing = AutoMailing(users=temp_transaction.transactions, dp=dp)
            self.result_report += await auto_mailing.start_mailing_for_unpaid_invoices()
            auto_mailing.Message = self.result_report
            await auto_mailing.admin_result_alert()
            self.result_report = ''
            await auto_mailing.clean()

        else:
            # Юзеров нет, просто делаем отчет админу
            self.result_report += 'Транзакций не найдено'
            auto_mailing = AutoMailing(dp=dp)
            auto_mailing.Message = self.result_report
            await auto_mailing.admin_result_alert()
            self.result_report = ''
            await auto_mailing.clean()
        return

    async def notification_of_disconnection(self, dp: Dispatcher):
        """Метод уведомления юзеров о предостоящем отключении конфига (за день, два, три)"""

        config_api = ConfigAPI()
        start = datetime.now()
        # three_day_conf = await config_api.get_exp_configs(days_before_disconnection=3)
        two_day_exp_conf = await config_api.get_exp_configs(days_before_disconnection=2)
        one_daye_exp_configs = await config_api.get_exp_configs(days_before_disconnection=1)

        # Делаем рассылку по юзерам за 2 дня:
        auto_mailing = AutoMailing(dp=dp)
        if two_day_exp_conf.configs:
            self.result_report += 'Отчет уедомления о подходящем продлении (2дня):\n'
            self.result_report += await auto_mailing.start_notification_of_disconnection(
                ovpn_transactions=two_day_exp_conf.configs, days=2)
            auto_mailing.Message = self.result_report
            await auto_mailing.admin_result_alert()
            self.result_report = ''
            await auto_mailing.clean()
        else:
            self.result_report += 'Отчет уедомления о подходящем продлении (2д):\n' \
                                  'Ненайдено конфигов для рассылки 😌'

            auto_mailing.Message = self.result_report
            await auto_mailing.admin_result_alert()
            self.result_report = ''
            await auto_mailing.clean()

        # Делаем рассылку по юзерам заа 1 день:
        if one_daye_exp_configs.configs:
            self.result_report += 'Отчет уедомления о подходящем продлении (1день):\n'
            self.result_report += await auto_mailing.start_notification_of_disconnection(
                ovpn_transactions=two_day_exp_conf.configs, days=1)
            auto_mailing.Message = self.result_report
            await auto_mailing.admin_result_alert()
            self.result_report = ''
            await auto_mailing.clean()
        else:
            self.result_report += 'Отчет уедомления о подходящем продлении (1д):\n' \
                                  'Ненайдено конфигов для рассылки 😌'

            auto_mailing.Message = self.result_report
            await auto_mailing.admin_result_alert()
            self.result_report = ''
            await auto_mailing.clean()

        auto_mailing.Message = f'<b>Рассылка заняла: {datetime.now() - start}</b>'
        await auto_mailing.admin_result_alert()
        await auto_mailing.clean()
        return

    async def trial_not_active_nitification(self, dp: Dispatcher):
        """Уведомить получивших но не активировавших триал пользователей"""

        trial_api = TrialsAPIv1()
        user_api = UserAPIv1()
        auto_mailing = AutoMailing(dp=dp)

        # Получить пользвателей для рассылки
        users = await user_api.get_users_of_private_trial_activate(pattern='not_activate')

        if users.users:
            self.result_report += 'Отчет напоминалки о активной триалке:\n'
            self.result_report += await auto_mailing.start_notification_of_trial_not_activation(users=users.users)
            auto_mailing.Message = self.result_report
            # Меняем статус триалок которые уже уведомлены
            await trial_api.edit_alert_status(old_status='active', new_status='inform')
            await auto_mailing.admin_result_alert()
            self.result_report = ''
            await auto_mailing.clean()
        else:
            if users.status:
                self.result_report += 'Отчет напоминалки о активной триалке:\n' \
                                      'Ненайдено юзеров для рассылки 😌'
            else:
                self.result_report += 'Отчет напоминалки о активной триалке:\n' \
                                      '🔴⛔️ Ошибка запроса пользователей #error'

            auto_mailing.Message = self.result_report
            await auto_mailing.admin_result_alert()
            self.result_report = ''
            await auto_mailing.clean()
