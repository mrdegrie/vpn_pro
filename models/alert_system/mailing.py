import asyncio
import logging
from aiogram import types, exceptions, Dispatcher
from typing import Optional, List, Union

from configs.config import admins, system_alerts
from database.data_models import TempTransaction, SharedVPNtransaction, User
from models.ui.ui_storage import UIControl
from models.ui.locales.available_localizations import country_lang_conv


# Класс расслок по базе клиентов
class AutoMailing:
    """Класс рассылки сообщений юзерам"""

    def __init__(self,
                 users: Optional[Union[List[TempTransaction], List[User]]] = None,
                 dp: Optional[Dispatcher] = None,
                 disable_notification: Optional[bool] = False):

        self.users = users
        self.Keyboard = None
        self.DP = dp
        self.DisableAlert = disable_notification

        # --- send message errros status
        self.BodBlocked = 0
        self.InvalidUserID = 0
        self.SuccessDendMessage = 0
        self.UserBacket = []  # Список пользоватеелй которые необходимо удалить из БД после ррассылки

        self.Message = ''

    async def clean(self):
        self.users = None
        self.UserBacket = []
        self.Message = ''
        return

    async def start_mailing_for_unpaid_invoices(self) -> str:
        """Метод старта рассылки ююзерам не оплатившим транзу в течении 3х часов"""

        count, errors, user_dubles = 0, 0, []

        sleep_alert = 0
        ui = UIControl(user_id=666)
        await ui.open_redis_connection()
        for index, trans in enumerate(self.users, start=1):
            sleep_alert += index
            if is_int(sleep_alert / 100):
                await asyncio.sleep(10)

            if trans.user_id not in admins and trans.user_id not in user_dubles:
                user_dubles.append(trans.user_id)
                try:
                    await ui.get_language_for_mainling(user_id=trans.user_id)
                    if await self.send_message(user_id=trans.user_id,
                                               user_name=trans.user_name,
                                               price=trans.price,
                                               mode=100,
                                               ui=ui,
                                               ):
                        pass
                    await asyncio.sleep(.35)  # 20 messages per second (Limit: 30 messages per second)
                except exceptions.BadRequest:
                    errors += 1
                    await asyncio.sleep(.45)

        self.Message = '<b>Рассылка заверешна!</b>\n\n' \
                       f'<b>✅ Успешно доставлено: <code>{self.SuccessDendMessage}</code> сообщений</b>\n' \
                       f'<b>🐓 Заблокировали бота: <code>{self.BodBlocked}</code> чел</b>\n' \
                       f'<b>☠️ Невалидных пользователей: <code>{self.InvalidUserID}</code>\n</b>'

        await ui.close_redis_connection()
        return self.Message

    async def start_notification_of_disconnection(self,
                                                  days: int = 1,
                                                  ovpn_transactions: Optional[List[SharedVPNtransaction]] = None,
                                                  ) -> str:
        """Старт рассылки по юзерам чьи конфиги завтра кончаются"""

        count, errors, user_dubles = 0, 0, []
        sleep_alert = 0
        ui = UIControl(user_id=666)
        await ui.open_redis_connection()
        try:
            for index, user in enumerate(ovpn_transactions, start=1):
                sleep_alert += index
                if is_int(sleep_alert / 100):
                    await asyncio.sleep(10)
                if user.user_id not in admins and user.user_id not in user_dubles:
                    try:
                        await ui.get_language_for_mainling(user_id=user.user_id)
                        if await self.send_message(user_id=user.user_id,
                                                   ui=ui,
                                                   mode=101,
                                                   config_country=user.country,
                                                   days=days
                                                   ):
                            pass
                        await asyncio.sleep(.35)  # 20 messages per second (Limit: 30 messages per second)
                    except exceptions.BadRequest:
                        errors += 1
                        await asyncio.sleep(.45)
        except TypeError:
            pass

        self.Message = '<b>Рассылка заверешна!</b>\n\n' \
                       f'<b>✅ Успешно доставлено: <code>{self.SuccessDendMessage}</code> сообщений</b>\n' \
                       f'<b>🐓 Заблокировали бота: <code>{self.BodBlocked}</code> чел</b>\n' \
                       f'<b>☠️ Невалидных пользователей: <code>{self.InvalidUserID}</code>\n</b>'

        await ui.close_redis_connection()
        return self.Message

    async def start_notification_of_trial_not_activation(self,
                                                         users: Optional[List[User]] = None,
                                                         ) -> str:
        """Старт рассылки по юзерам чьи триалки еще не использованы"""

        ui = UIControl(user_id=666)
        await ui.open_redis_connection()
        count, errors, user_dubles = 0, 0, []
        sleep_alert = 0
        try:
            for index, user in enumerate(users, start=1):
                sleep_alert += index
                if is_int(sleep_alert / 100):
                    await asyncio.sleep(10)
                if user.user_id not in admins and user.user_id not in user_dubles:
                    user_dubles.append(user.user_id)
                    try:
                        await ui.get_language_for_mainling(user_id=user.user_id)
                        if await self.send_message(user_id=user.user_id,
                                                   mode=102,
                                                   ui=ui,
                                                   ):
                            pass
                        await asyncio.sleep(.35)  # 20 messages per second (Limit: 30 messages per second)
                    except exceptions.BadRequest:
                        errors += 1
                        await asyncio.sleep(.45)
        except TypeError:
            pass

        self.Message = '<b>Рассылка заверешна!</b>\n\n' \
                       f'<b>✅ Успешно доставлено: <code>{self.SuccessDendMessage}</code> сообщений</b>\n' \
                       f'<b>🐓 Заблокировали бота: <code>{self.BodBlocked}</code> чел</b>\n' \
                       f'<b>☠️ Невалидных пользователей: <code>{self.InvalidUserID}</code>\n</b>'
        await ui.close_redis_connection()
        return self.Message

    async def admin_result_alert(self):
        """Предупреждение админу о состоянии рассылки"""
        try:
            for admin in system_alerts:
                await self.DP.bot.send_message(chat_id=admin, text=self.Message)
        except exceptions.BadRequest:
            pass

    # Метод отправки сообщеия
    async def send_message(self,
                           user_id: int,
                           mode: int,
                           ui: UIControl,
                           user_name: Union[str, None] = None,
                           price: Optional[int] = None,
                           config_country: Optional[str] = None,
                           days: Optional[int] = 1,
                           ) -> bool:
        """
        Метод отправки сообщения
        Mode: вариантивность сообщения
        100 - 3‑х часовики
        101 - Увдомление о подходящем дисконекте конфига
        102 - Уведомление напоминание о действущей триалке


        """
        try:
            await self.DP.bot.send_message(chat_id=user_id,
                                           text=message_factory(mode=mode,
                                                                ui=ui,
                                                                days=days,
                                                                user_name=user_name,
                                                                price=price,
                                                                config_country=config_country,),
                                           disable_notification=self.DisableAlert,
                                           reply_markup=self.keyboad_factoy(mode=mode, ui=ui),
                                           )

        except exceptions.BotBlocked:
            logging.error(f"Target [ID:{user_id}]: blocked by user")
            self.BodBlocked += 1
            return False
        except exceptions.ChatNotFound:
            logging.error(f"Target [ID:{user_id}]: invalid user ID")
            self.InvalidUserID += 1
            self.UserBacket.append(user_id)
            return False
        except exceptions.RetryAfter as e:
            logging.error(f"Target [ID:{user_id}]: Flood limit is exceeded. Sleep {e.timeout} seconds.")
            await asyncio.sleep(e.timeout)
            await asyncio.sleep(.10)
            return await self.send_message(user_id=user_id,
                                           mode=mode,
                                           user_name=user_name,
                                           price=price,
                                           config_country=config_country,
                                           days=days,
                                           ui=ui,
                                           )
        #
        except exceptions.UserDeactivated:
            logging.error(f"Target [ID:{user_id}]: user is deactivated")
            self.InvalidUserID += 1
            self.UserBacket.append(user_id)
            return False
        except exceptions.TelegramAPIError:
            logging.exception(f"Target [ID:{user_id}]: failed")
            return False
        except:
            self.BodBlocked += 1
            return False
        else:
            logging.info(f"Target [ID:{user_id}]: success")
            self.SuccessDendMessage += 1
            return True

    def keyboad_factoy(self, mode: int, ui: UIControl) -> Union[None, types.InlineKeyboardMarkup]:
        """
        Создать клавиатуру отнсительно текущей рассылки
        :param ui:
        :param mode:
        :return:
        """

        if mode == 101:
            self.Keyboard = types.InlineKeyboardMarkup()
            self.Keyboard.row(
                types.InlineKeyboardButton(
                    text=ui.get_text("payments_for_mailing"),
                    callback_data='payments_for_mailing'),
            )
            return self.Keyboard

        elif mode == 102:
            self.Keyboard = types.InlineKeyboardMarkup()
            self.Keyboard.row(
                types.InlineKeyboardButton(
                    text=ui.get_text("bay_vpn_trial_version"),
                    callback_data='bay_vpn_trial_version',
                )
            )
            return self.Keyboard
        else:
            return None


def is_int(n):
    return int(n) == float(n)


def message_factory(mode: int,
                    ui: UIControl,
                    days: Optional[int] = 1,
                    user_name: Optional[str] = None,
                    price: Optional[int] = None,
                    config_country: Optional[str] = None,
                    ):
    """
    Функция для получения текста рассылки вв зависимости от ыбранного типа рассылки (mode)
    100 - Рассылка можуля временныж трназакций за 2-3 часа
    101 - Увдомление о подходящем дисконекте конфига
    102 - Уведомление напоминание о действущей триалке

    """
    if mode == 100:

        return ui.get_text("time_transaction_mail").format(
            user_name=user_name_conv(user_name),
            price=price,
        )

    elif mode == 101:
        msg = ui.get_text("balanc_task").format(
            days=days,
            config_country=country_lang_conv(
                lang=ui.language,
                country=config_country,
            )
        )
        return msg

    elif mode == 102:
        return ui.get_text("trial_mail")


def user_name_conv(username: str):
    if username:
        return username
    else:
        return 'Anonim!'


def day_conv(days: int) -> str:
    conv = {1: 'день', 2: 'дня', 3: 'дня', 4: 'дня'}
    try:
        return conv[days]
    except KeyError:
        return 'дней'
