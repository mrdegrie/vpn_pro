from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup, WebAppInfo
from typing import Optional

from models.ui.ui_storage import UIControl


class ProxySellMainControl:
    """Общий класс продажи прокси"""

    def __init__(self, user_id: int):
        self._user_id = user_id
        self._keyboard = InlineKeyboardMarkup()
        self._MSG: Optional[str] = None

    @property
    def keyboad(self):
        return self._keyboard

    @property
    def text_message(self):
        return self._MSG

    async def proxy_dell_main_page(self):
        """Главная страница модуля продажи прокси"""

        ui = UIControl(self._user_id)
        await ui.get_language()
        url = 'https://google.com'
        self._MSG = ui.get_text("proxy-sell-main-page")
        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text('button-sell-proxy'), callback_data='sell-proxy'),
            InlineKeyboardButton(text=ui.get_text('button-proxy-history'), callback_data='proxy-history'),
        )
        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='next_start_message'),
            InlineKeyboardButton(text=ui.get_text('button-abount-proxy-service'), web_app=WebAppInfo(url=url)),
        )
        # self._keyboard.row(
        #
        #     InlineKeyboardButton(text=ui.get_text('button-some'), callback_data='button-some'),
        # )

        return self.keyboad

    async def get_sell_proxy_category(self):
        """Получить страницу выбора категорий продажи прокси"""
        ui = UIControl(self._user_id)
        await ui.get_language()

        url = "https://google.com"
        self._MSG = ui.get_text('proxy_sell_category_text')
        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text('proxy_type_datacentr'), callback_data='proxy_type_datacentr'),
            InlineKeyboardButton(text=ui.get_text('proxy_type_mobile'), callback_data='proxy_type_mobile'),
        )
        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text('proxy_type_business'), callback_data='proxy_type_business'),
            InlineKeyboardButton(text=ui.get_text('proxy_type_resident'), callback_data='proxy_type_resident'),
        )
        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='main_proxy_page'),
            InlineKeyboardButton(text=ui.get_text('button-poxy-faq'), web_app=WebAppInfo(url=url)),
        )
        return self.keyboad
