from aiogram import Dispatcher
from apscheduler.schedulers.asyncio import AsyncIOScheduler

# from models.payments.free_kassa_auto_payer import AutoKassa
from models.payments.config_auto_payer import ConfigAutoPayer
from models.alert_system.auto_alert_system_control import AutoAlertSystem
# from models.admin.vpn.auto_ping import server_auto_ping

scheduler = AsyncIOScheduler()


def scheduler_task_starter(dp: Dispatcher):
    scheduler.start()

    # free_kassa = AutoKassa()
    config_checker = ConfigAutoPayer()
    auto_alert_system = AutoAlertSystem()

    # scheduler.add_job(free_kassa.auto_checker, 'interval', seconds=300, args=(dp,))
    # scheduler.add_job(server_auto_ping, 'interval', seconds=600, args=(dp,))
    scheduler.add_job(config_checker.get_config_activation,
                      'cron',
                      day_of_week='mon-sun',
                      hour=15, minute=28,
                      args=(dp,),
                      timezone='Europe/Moscow',
                      )
    auto_alert_system.task_starter(dp, scheduler)  # Запуск система автоалертов
    return
