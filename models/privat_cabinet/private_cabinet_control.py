from typing import Optional
from aiogram import types
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

from models.users.user_control import UserControl
from models.base_models.local_settings import LocalSettings
from models.ui.ui_storage import UIControl


class PrivateCabinet:
    """Класс личного кабинета"""

    def __init__(self):
        self.MSG: Optional[str] = None
        self.keyboard = InlineKeyboardMarkup()
        self.start_photo: Optional[str] = None

    # Сформировать личный кабинет
    async def get_private_cabinet(self,
                                  user_id: int,
                                  text_mode: bool = False,
                                  ) -> types.InlineKeyboardMarkup:

        user_control = UserControl()
        user_info = await user_control.get_user_info(user_id, get_cashed=True)
        ui = UIControl(user_id=user_id)
        await ui.get_language()

        if user_info.status:
            self.MSG = ui.get_text("private_cabinet_text").format(
                user_id=user_id,
                balance=round(user_info.user_info.balance, 5),
            )
            self.keyboard.row(InlineKeyboardButton(text=ui.get_text("all_payments"), callback_data='all_payments'))
            self.keyboard.row(
                InlineKeyboardButton(text=ui.get_text("my_shared_config_list"), callback_data='my_shared_config_list'),
                InlineKeyboardButton(text=ui.get_text("lang_change"), callback_data='lang_change'),
            )
        else:
            self.MSG = ui.get_text("backend_error")
        self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"),
                                               callback_data='next_start_message'))
        if text_mode:
            setting = LocalSettings()
            self.start_photo = await setting.get_start_photo()
        return self.keyboard

    # # Получить страницу сервисов для подписки
    # async def get_all_sub_services(self, lang: str, user_id: int):
    #     ui = UiStorage(lang=lang, user_id=user_id, long_connection=True)
    #
    #     self.MSG = ui.get_text("select_service_text")
    #     self.keyboard.row(InlineKeyboardButton(text=ui.get_text("shared_vpn_plans"), callback_data='shared_vpn_plans'))
    #     self.keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='private_cabinet'))
    #     await ui.close_ui()
    #     return self.keyboard

# # Status convertor
# def status_coonv(status: bool) -> str:
#     conv = {True: '✅ Активна', False: '⛔️ Неактив'}
#     return conv[status]
