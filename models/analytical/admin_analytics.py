from typing import Optional

from models.backend.partner_endpoints import PartnerAPI


class BasePartnerAnalytical:
    """Класс базовой партнерской аналитики"""

    def __init__(self):
        self.MSG: Optional[str] = None
        self.status: Optional[bool] = False

    # Произвести базовую аналитику проекта
    async def start_base_analytic(self, user_id: int):
        back_and = PartnerAPI()
        base_analytic = await back_and.get_base_stats(user_id)
        self.status = base_analytic.status
        return base_analytic
