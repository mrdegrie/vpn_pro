from typing import Optional
import aioredis
from aioredis import Redis

from models.backend.partner_endpoints import PartnerAPI
from models.backend import backend_models
from database.db_connected import get_redis


class BasePartnerAnalytical:
    """Класс базовой партнерской аналитики"""

    def __init__(self):
        self.MSG: Optional[str] = None
        self.status: Optional[bool] = False

    # Произвести базовую аналитику партнера

    async def start_base_partner_analytic(self, user_id: int, get_cash: bool = False):
        back_and = PartnerAPI()
        if get_cash:
            redis: Redis = await get_redis()
            base_analytic = await redis.get(name=f'partner_base_stats_{user_id}')

            if not base_analytic:
                base_analytic = await back_and.get_base_stats(user_id)
                await redis.set(name=f'partner_base_stats_{user_id}', value=base_analytic.json(), ex=600)
                self.status = base_analytic.status
                await redis.close()
                return base_analytic
            else:
                stats = backend_models.ResponseBasePartnerStats.parse_raw(base_analytic)
                self.status = stats.status
                await redis.close()
                return stats
        else:
            stats = await back_and.get_base_stats(user_id)
            self.status = stats.status
            return stats
