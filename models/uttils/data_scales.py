import leather
from reportlab.graphics import renderPM
from datetime import timedelta, datetime
from svglib.svglib import svg2rlg
from typing import Optional

from models.backend.backend_models import BaseStatsResponse


class DataScale:
    """Отрисовка графика статы"""

    def __init__(self, stats: Optional[BaseStatsResponse] = None):
        self.stats = stats
        self.Status = False

    async def user_history_reg_scale(self):
        """Отрисовать график истории регистрация пользователей"""

        data, data_2, start_time = [], [], []

        users = self.stats.base_stats.user_reg_history

        # all_manual_sales = await connect.select(
        #     custom_query="""SELECT date(time), sum(price) FROM transaction_history WHERE vendor_code = '669'
        #             group by date(time) order by date(time);"""
        # )

        for index, user in enumerate(users, start=0):
            if index == 0:
                start_time = user.date
            data.append((user.date, user.count))

        # for index, time in enumerate(all_manual_sales, start=0):
        #     data_2.append((time[0], time[1]))
        if not data:
            times = datetime.now() + timedelta(days=10)
            data.append((datetime.now().date(), 0))

        if not data_2:
            times = datetime.now() + timedelta(days=10)
            data_2.append((times.date(), 1))
        # график с точками
        leather.theme.default_series_colors = ['#ff0000', '#00ff00']
        chart = leather.Chart('New users')
        chart.add_line(data, name='Auto Donate')
        # chart.add_dots(data, name='Refund')
        chart.to_svg('storage/scales/users.svg')

        drawing = svg2rlg("storage/scales/users.svg")
        renderPM.drawToFile(drawing, "storage/scales/users.png", fmt="PNG")
        self.Status = True
        return True

    async def render_bars(self, data: list, title: str, svg_path: str, img_path: str) -> bool:
        """Отрисовать бары"""

        chart = leather.Chart(title)
        chart.add_bars(data)
        chart.to_svg(svg_path)

        drawing = svg2rlg(svg_path)
        renderPM.drawToFile(drawing, img_path, fmt="PNG")
        self.Status = True
        return True
