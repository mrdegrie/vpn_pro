import ssl
import os
import urllib.request
from typing import Optional
from configs.config import TOKEN


class LoadFile:
    def __init__(self):
        self._get_file_url = 'https://api.telegram.org/file/bot'
        self._file_path: Optional[str] = None

    @property
    def file_path(self):
        return self._file_path

    def load_file(self, remote_path: str, file_dir: str, file_name: str):
        try:
            context = ssl._create_unverified_context()
            user_file = urllib.request.urlopen(
                f"{self._get_file_url}{TOKEN}/{remote_path}", context=context).read()
            try:
                os.mkdir(f'{file_dir}')
            except FileExistsError:
                pass
            self._file_path = f'{file_dir}/{file_name}'

            with open(self.file_path, 'wb') as write_data:
                write_data.write(user_file)
            return self.file_path
        except:
            return False

