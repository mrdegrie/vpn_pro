import asyncio
import logging
from aiogram import exceptions, Dispatcher
from aiogram.types import InlineKeyboardMarkup

from typing import Optional


class Alerts:
    def __init__(self, dp: Optional[Dispatcher] = None):
        self._dp = dp

        self.BodBlocked: int = 0
        self.InvalidUserID: int = 0
        self.UserBacket = []
        self.SuccessDendMessage: int = 0

    # Метод отправки сообщения
    async def send_message(self,
                           user_id: int,
                           text_message: str,
                           keyboard: Optional[InlineKeyboardMarkup] = None,
                           ) -> bool:
        try:
            await self._dp.bot.send_message(user_id, text_message, reply_markup=keyboard)
        except exceptions.BotBlocked:
            logging.error(f"Target [ID:{user_id}]: blocked by user")
            self.BodBlocked += 1
            return False
        except exceptions.ChatNotFound:
            logging.error(f"Target [ID:{user_id}]: invalid user ID")
            self.InvalidUserID += 1
            self.UserBacket.append(user_id)
            return False
        except exceptions.RetryAfter as e:
            logging.error(f"Target [ID:{user_id}]: Flood limit is exceeded. Sleep {e.timeout} seconds.")
            await asyncio.sleep(e.timeout)
            await asyncio.sleep(.10)
            return await self.send_message(user_id)  # Recursive call
        except exceptions.UserDeactivated:
            logging.error(f"Target [ID:{user_id}]: user is deactivated")
            self.InvalidUserID += 1
            self.UserBacket.append(user_id)
            return False
        except exceptions.TelegramAPIError:
            logging.exception(f"Target [ID:{user_id}]: failed")
            return False
        except:
            self.BodBlocked += 1
            return False
        else:
            logging.info(f"Target [ID:{user_id}]: success")
            self.SuccessDendMessage += 1
            return True

    def clean(self):
        self.BodBlocked: int = 0
        self.InvalidUserID: int = 0
        self.UserBacket = []
        self.SuccessDendMessage: int = 0

