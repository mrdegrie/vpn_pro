from pymemcache.client import base


class Cash:
    """Работа с кешем"""

    def __init__(self, host: str = '127.0.0.1', port: int = 11211, exp: int = 240):
        self._host = host
        self._port = port
        self.exp = exp

        self.client: base.Client = base.Client((self._host, self._port))

    def get_auth_token(self):
        return self.client.get(key='access_token', default=False)

    def close(self) -> bool:
        self.client.close()
        return True
