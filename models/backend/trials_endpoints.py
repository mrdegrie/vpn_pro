import aiohttp
from typing import Optional, Dict
from pydantic import ValidationError
import asyncio

from models.backend import backend_models
from configs.config import bot_name, backend_base_url
from models.backend.auth import Baselogin


class TrialsAPIv1(Baselogin):
    """Класс для взаимодействия с бекендом. Tial period v1"""

    def __init__(self):
        super().__init__()
        self.__header: Optional[Dict] = None

        self._base_url = f'{backend_base_url}/v1/trials/private-trial/'
        self._activate_trial = 'activate-trial'
        self._get_trial = 'get-trial'
        self._edit_alert_status = 'edit-alert-status'

    async def activate_private_trial(self, pattern: str, days: int) -> backend_models.PtivateTrialActivateResult:
        """Активировать триальный период у пользователей"""

        url = self._base_url + self._activate_trial
        data = {
            "pattern": pattern,
            "days": days,
        }
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=data, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.PtivateTrialActivateResult.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.PtivateTrialActivateResult(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)
                    else:
                        return backend_models.PtivateTrialActivateResult(
                            status=False, msg=f'bad status code: {resp.status}')

    async def get_private_trial_user(self, user_id: int,
                                     ) -> backend_models.UserTrialResult:
        """Получить триал пользователя"""

        url = self._base_url + self._get_trial + f'/{user_id}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.UserTrialResult.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.UserTrialResult(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)
                    else:
                        return backend_models.UserTrialResult(
                            status=False, msg=f'bad status code: {resp.status}')

    async def edit_alert_status(self,
                                old_status: str,
                                new_status: str,
                                ) -> backend_models.EditAlertStatusResult:
        """Массово изменить aler status provate_trial table"""

        url = self._base_url + self._edit_alert_status + f'/{old_status}/{new_status}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.EditAlertStatusResult.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.EditAlertStatusResult(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)
                    else:
                        return backend_models.EditAlertStatusResult(
                            status=False, msg=f'bad status code: {resp.status}')
