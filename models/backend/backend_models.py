from datetime import datetime
from typing import Optional, List, Dict

from pydantic import BaseModel

from database.data_models import (
    OVPNServer,
    OvpnPlans,
    SharedVPNtransaction,
    Wallet,
    TempTransaction,
    User,
    OvpnItem,
    PrivateTrial,
    VPNTariff,
    IpCheckerTransaction,
    NewUser,
)


class BaseResponse(BaseModel):
    status: bool
    msg: Optional[str] = None


# Модель ответа создания пользователя
class AddUserResponse(BaseModel):
    status: bool
    message: str
    user: Optional[NewUser] = None


# Модель запроса для создания нового пользователя
class AddUserRequest(BaseModel):
    api_key: str
    user_id: int
    user_name: str
    balance: Optional[float] = 0
    partner_id: Optional[int] = None
    role: str = 'user'


# результат запроса всех активных серверов
class AllActiveServers(BaseModel):
    status: bool
    msg: Optional[str] = None
    result: Optional[List[OVPNServer]]


# Модель информации о пользователей
class UserInfo(BaseModel):
    user_id: int
    user_name: Optional[str] = None
    balance: Optional[float] = 0
    partner_id: Optional[int] = None
    role: str = 'user'
    register_date: datetime
    status: bool
    config_count: Optional[int] = 0


# Модель ответа при запросе данных пользователя
class UserInfoResponse(BaseModel):
    status: bool
    message: str
    user_info: Optional[UserInfo] = None


# Класс ответа при запросе всех планов
class GetAllPlansResponse(BaseModel):
    status: bool
    msg: Optional[str] = None
    result: Optional[List[OvpnPlans]] = None


# Результат запроса информации о плане впн
class PlanInfoResult(BaseModel):
    status: bool
    msg: Optional[str] = None
    result: Optional[OvpnPlans] = None


class Config(BaseModel):
    order_id: int
    vpn_user_name: str
    country: str
    country_code: str
    config_text: str


class BuyConfigResponse(BaseResponse):
    status_code: int
    config: Optional[Config] = None


class CreateConfigResult(BaseModel):
    status: bool
    msg: Optional[str] = None
    result: Optional[str] = None
    limit_server: Optional[bool] = False
    config_country: Optional[str] = None
    config_name: Optional[str] = None


class AllUserConfigs(BaseResponse):
    result: Optional[List[SharedVPNtransaction]]


class DellConfigResult(BaseModel):
    status: bool
    msg: Optional[str] = None


# Результт создания платежки фриикассы для шаред овпн
class NewFKPay(BaseModel):
    status: bool
    msg: Optional[str]
    payment_url: Optional[str] = None
    order_id: Optional[int] = None
    price: Optional[int] = None


# Модель ответа послдений транзакций (GetLastTempTransaction)
class LastTempTransaction(BaseModel):
    status: bool
    msg: Optional[str] = None
    result: Optional[List[TempTransaction]] = None


class SuccessFKPay(BaseModel):
    status: bool
    msg: Optional[str] = None


class BaseAnalytical(BaseModel):
    all_users: Optional[int] = 0
    global_sum_sale: Optional[int] = 0
    all_profit: Optional[float] = 0
    avaliable_balance: Optional[float] = 0
    paid_users_specified_partner: Optional[int] = 0
    partner_percent: Optional[int] = 10


class ResponseBasePartnerStats(BaseModel):
    status: bool
    msg: Optional[str] = None
    base_analytical: Optional[BaseAnalytical] = None


class AllWallet(BaseModel):
    status: bool
    msg: Optional[str] = None
    wallet: Optional[List[Wallet]] = None


class GetServerInfoResponse(BaseModel):
    status: bool
    msg: Optional[str] = None
    server_info: Optional[OVPNServer]


# Модель ответ метода апдейта баласа
class UpdateUserBalanceResponse(BaseModel):
    status: bool
    msg: Optional[str] = None


# Модель ответа метода опдейта автопродления конфига
class UpdateAutopayerResponse(BaseModel):
    status: bool
    msg: Optional[str] = None
    new_status: Optional[bool] = None


class CountryConfigStats(BaseModel):
    country: str
    count: int


class UsersHistoryRegs(BaseModel):
    date: Optional[datetime] = None
    count: Optional[int] = 0


class BaseStats(BaseModel):
    """Модель базовой аналитики"""

    allusers: Optional[int] = 0  # Всего пользователей
    all_donats: Optional[int] = 0  # Сумма всех пополнений в боте
    shared_vpn_all_sales_count: Optional[int] = 0  # Количество всех проданных конфигов шаред впн
    one_day_donats: Optional[int] = 0  # Сумма донатов за сутки
    current_month_donats: Optional[int] = 0  # Сумма Донатов за текущий мес
    last_month_donats: Optional[int] = 0  # Донатов за прошлый мес
    referral_payments: Optional[int] = 0
    trial_configs: Optional[int] = 0
    active_shared_configs: Optional[int] = 0
    paid_users: Optional[int] = 0
    active_users: Optional[int] = 0
    nex_day_sales: Optional[int] = 0
    unique_users_donats_yesterday: Optional[int] = 0  # # кличество уни поплнений за вчерашний день
    unique_users_donats_today: Optional[int] = 0  # кличество уник поплнений за сегодняшний день
    unique_users_donats_tomonth: Optional[int] = 0  # Кол ник пополнений за текущий мес
    unique_users_donats_last_month: Optional[int] = 0  # Кол уник пополнений за прошлый мес
    total_number_trilas: Optional[int] = 0
    count_purchases_after_the_trial: Optional[int] = 0
    sum_purchases_after_the_trial: Optional[int] = 0
    count_extension_vpn: Optional[int] = 0
    departed_users: Optional[int] = 0  # Кол юзеров отказавшихся от VPN
    user_reg_history: Optional[List[UsersHistoryRegs]] = None
    country_config_counts: Optional[List[CountryConfigStats]] = None


class BaseStatsResponse(BaseModel):
    """Ответ базовой аналитики"""
    status: bool
    msg: Optional[str] = None
    base_stats: Optional[BaseStats] = None


class GetExpCanfigsRequest(BaseModel):
    """Запрос метода get_exp_configs"""
    api_key: str
    days_before_disconnection: int


class GetExpConfigsResponse(BaseModel):
    """Ответ меода get_exp_configs"""
    status: bool
    msg: Optional[str] = None
    configs: Optional[List[SharedVPNtransaction]] = None


class ExtendConfigResult(BaseModel):
    """Результат продления конфига"""

    update_result: bool
    new_exp_date: Optional[datetime] = None
    msg: Optional[str] = None


class ExtendConfigRequest(BaseModel):
    """Запрос метода extend_config"""
    api_key: str
    config_id: int
    extend_price: int


class ExtendConfigResponse(BaseResponse):
    """Ответ метода extend_config"""

    new_exp_date: Optional[datetime] = None
    status_code: int


class GetTempTransRequest(BaseModel):
    """Запрос метода get_temp_transactions"""
    api_key: str
    period: Optional[int] = 3
    status_transaction: Optional[str] = 'created'


class GetTempTransResponse(BaseModel):
    """Ответ метода get_temp_transactions"""
    status: bool
    msg: Optional[str] = None
    transactions: Optional[List[TempTransaction]]


class GetUsersMailingRequest(BaseModel):
    """Модель запроса метода get_users_of_mailing"""
    api_key: str
    pattern: str


class GetUsersMailingResponse(BaseModel):
    """Модель ответа метода get_users_of_mailing"""
    status: bool
    msg: Optional[str] = None
    users: Optional[List[User]]


class CreateNewPaymentPayokRequest(BaseModel):
    """Модель запроса метода crete_new_payment"""
    signature: str
    user_id: int
    user_name: Optional[str] = ''
    currency: Optional[str] = 'USD'
    amount: int


class PayokPayment(BaseModel):
    """Модель созданной платежки Payok"""
    order_id: int
    url: str
    amount: int
    currency: str
    description: str


class CreateNewPaymentPayokResponse(BaseModel):
    """Модель ответа метода crete_new_payment"""

    status: bool
    msg: Optional[str] = None
    payment: Optional[PayokPayment]


class GetTempTransactionRequest(BaseModel):
    """Модель метода get_info_temp_transaction"""
    api_key: str
    transaction_id: int


class GetTempTransactionResponse(BaseModel):
    """Модель ответа метода get_info_temp_transaction"""
    status: bool
    msg: Optional[str] = None
    trnsaction: Optional[TempTransaction] = None


class ServerBackUpResult(BaseModel):
    """Результат бекапа серверов VPN"""

    status: bool
    msg: Optional[str] = None
    servers_backup: Optional[List] = None


class BaseLoginResult(BaseModel):
    """Модель результата базовой авторизации"""

    status: bool
    msg: Optional[str] = None
    access_token: Optional[str] = None


class GetOvpnItemsResponse(BaseModel):
    """Мдель результата запроса активных OVPN ITEMS"""

    status: bool
    msg: Optional[str] = None
    items: Optional[List[OvpnItem]] = None


class GetItemResponse(BaseModel):
    """Модель результат запроса item по id"""

    status: bool
    msg: Optional[str] = None
    item: Optional[OvpnItem] = None


class NewPaymentRequest(BaseModel):
    """Результат создания заявки на выплату пртнеру"""

    status: bool
    msg: Optional[str] = None
    order_id: Optional[int] = None


class GetUsersFromPrivateTrial(BaseModel):
    """Модель ответа метода get_users_of_private_trial"""

    status: bool
    msg: Optional[str] = None
    users: Optional[List[User]]


class PtivateTrialActivateResult(BaseModel):
    """Модель ответ метоода activate_trial"""
    status: bool
    msg: Optional[str] = None
    users_count: Optional[int] = None


class UserTrialResult(BaseModel):
    """Модель ответа метода get a trial user"""

    status: bool
    msg: Optional[str] = None
    trial: Optional[PrivateTrial] = None


class EditAlertStatusResult(BaseModel):
    status: bool
    msg: Optional[str] = None
    old_status: Optional[str] = None
    new_stattus: Optional[str] = None


class VPNCountryStats(BaseModel):
    """Модель статистики использования геолокаций VPN"""

    country_config_counts: Optional[List[CountryConfigStats]] = None


class GetVpnCountryStats(BaseModel):
    """Ответ метода get_country_stats"""

    status: bool
    msg: Optional[str] = None
    country_stats: Optional[VPNCountryStats] = None


class LavaPayment(BaseModel):
    """Модель созданной платежки LAVA"""
    order_id: int
    url: str
    amount: int


class CreateNewLavaPaymentResponse(BaseModel):
    """Результат метода создания платежа LAVA"""

    status: bool
    msg: Optional[str] = None
    payment: Optional[LavaPayment] = None


class ServerPingResult(BaseModel):
    """Модель результата пинга серверов"""
    server_id: int
    country: str
    status: bool


class SeverPingResponse(BaseModel):
    """Модеь метода пинга серверов VPN"""

    status: bool
    msg: Optional[str] = None
    ping_result: Optional[List[ServerPingResult]] = None


class GetUserLanguage(BaseModel):
    status: bool
    msg: Optional[str] = None
    language: Optional[str] = None


class UpdateLanguageResponse(BaseModel):
    status: bool
    msg: Optional[str] = None


class GeoData(BaseModel):
    """Модель гео данных IP адреса"""
    continent: str
    continentCode: str
    country: str
    countryCode: str
    region: str
    regionName: str
    city: str
    zip: str
    lat: float
    lon: float
    timezone: str
    currency: str
    callingCode: int


class OrgData(BaseModel):
    """Модель Организационных данных Ip адреса"""
    isp: str
    org: str
    as_: str
    asname: str
    reverse: Optional[str] = None


class Ports(BaseModel):
    open_ports: Optional[Dict[int, bool]] = None


class ScoreData(BaseModel):
    """МодельРиск данных"""
    fraund_score: Optional[int] = 0
    risk_score: Optional[str] = None


class ConnectionType(BaseModel):
    """Модель типа подключения IP адресса"""
    proxy: bool
    mobile: bool
    resident: bool
    hosting: bool
    active_vpn: bool
    active_tor: bool
    bot: bool
    vpn_risk: bool


class IpCheckResult(BaseResponse):
    """Модель результата одиночного пробива IP адреса"""
    ip: Optional[str] = None
    check_id: Optional[int] = None
    geo_data: Optional[GeoData] = None
    org_data: Optional[OrgData] = None
    ports: Optional[Dict[int, bool]] = None
    score: Optional[ScoreData] = None
    connection_data: Optional[ConnectionType] = None
    web_resul: Optional[str] = None


class SecurityData(BaseModel):
    is_bot: bool
    active_vpn: bool
    vpn_risk: bool


class GetAllVPNTariffs(BaseResponse):
    """Модель получения все тарифы VPN SHARED"""
    tariffs: Optional[List[VPNTariff]]


class ActivateRecord(BaseModel):
    status_code: int
    config_status: bool
    user_id: int
    county: str
    config_id: int
    expire: Optional[datetime] = None


class AutoActivationRespsone(BaseResponse):
    """Модельрезультата метода автматическй акктивации кнфигов"""
    status_code: int = 200
    report: Optional[List[ActivateRecord]] = None


class MultiCheckIP(BaseModel):
    """Модель массового пробива """
    ip: Optional[str] = None
    geo_data: Optional[GeoData] = None
    org_data: Optional[OrgData] = None
    ports: Optional[Dict[int, bool]] = None
    connection_data: Optional[ConnectionType] = None


class MultiCheckResponse(BaseResponse):
    status_code: int = 200
    check_id: Optional[int] = None
    cheking: Optional[List[MultiCheckIP]] = None
    web_result: Optional[str] = None


class CheckerHistoryResponse(BaseResponse):
    """Результат запроса истрии чеков"""
    history: Optional[list[IpCheckerTransaction]] = None
