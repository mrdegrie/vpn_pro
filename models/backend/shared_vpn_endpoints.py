import json
from typing import Optional, Dict
from uuid import UUID

import aiohttp
import asyncio
import hashlib
import hmac
from pydantic import ValidationError

from configs.config import api_key, private_key, bot_name, backend_base_url
from models.backend import backend_models
from models.backend.auth import Baselogin


class SharedOVPNPlans(Baselogin):
    """Ендпоинты SharedOVPNPlans - планы подписок"""

    def __init__(self):
        super().__init__()
        self.__header: Optional[Dict] = None
        self._base_url = f'{backend_base_url}/v1/shared-ovpn/plans/'
        self._get_all_plans_url = 'get-all-plans'
        self._get_plan_info_url = 'get-plan-info'

    # Получить все активные планы
    async def get_all_plans(self):
        url = f'{self._base_url}{self._get_all_plans_url}?api_key={api_key}'

        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.GetAllPlansResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.GetAllPlansResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.GetAllPlansResponse(status=False, msg=f'bad status code: {resp.status}')

    # Получить информацию о плане по ID
    async def get_plan_info(self, plan_id: int):
        url = self._base_url + self._get_plan_info_url
        data = {
            "api_key": api_key,
            "ovpn_plan_id": plan_id,
        }

        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=data, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.PlanInfoResult.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.PlanInfoResult(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.PlanInfoResult(status=False, msg=f'bad status code: {resp.status}')


class SharedOVPNServers(Baselogin):
    """ Ендпоинты SharedOVPNServers - сервера ОВПН"""

    def __init__(self):
        super().__init__()
        self.__header: Optional[Dict] = None
        self._base_url = f'{backend_base_url}/v1/shared-ovpn/servers/'
        self._get_active_servers_url = 'get-active-servers'
        self._get_server_info_url = 'get-server-info'
        self._start_backup_sert_url = 'start-backup-sert'
        self._ping_servers_url = 'ping-servers'

    # Получить список активных серверов ВПН
    async def get_active_servers(self) -> backend_models.AllActiveServers:
        url = f'{self._base_url}{self._get_active_servers_url}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.AllActiveServers.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.AllActiveServers(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.AllActiveServers(status=False, msg=f'bad status code: {resp.status}')

    # Получить инфо сервера по ServerID
    async def get_server_info(self, server_id: int) -> backend_models.GetServerInfoResponse:
        url = self._base_url + self._get_server_info_url + f'/{server_id}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.GetServerInfoResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.GetServerInfoResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.GetServerInfoResponse(status=False, msg=f'bad status code: {resp.status}')

    async def ping_vpn_servers(self) -> backend_models.SeverPingResponse:
        """Произвести пинг серверов с деакктивацией не актива"""
        url = f'{self._base_url}{self._ping_servers_url}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.SeverPingResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.SeverPingResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.SeverPingResponse(status=False, msg=f'bad status code: {resp.status}')


# Бэкенд конфигов
class SharedOVPNConfigs(Baselogin):
    def __init__(self):
        super().__init__()
        self.__header: Optional[Dict] = None
        self._base_url = f'{backend_base_url}/v1/shared-ovpn/configs/'
        self._add_new_configs_url = 'add-new-configs'
        self._buy_config_url = 'buy-vpn-config'
        self.get_all_configs_users_url = 'get-all-configs-users'
        self._dell_config_url = 'dell-config'
        self._update_autopayer_url = 'update-autopayer'
        self.get_exp_configs_url = 'get-exp-configs'
        self.extend_config_url = 'extend-config'
        self._get_extend_configs_url = 'start-shared-openvpn-activator'

    # Создать новый конфиг
    async def buy_config(self,
                         item_id: int,
                         user_id: int,
                         tariff_id: Optional[int] = None,
                         trial: Optional[bool] = False,
                         ) -> backend_models.BuyConfigResponse:
        """
        Status codes:
        200 - ok. Created config
        404: User/item/tariff not found
        405: Trial not active/Eror deactiate trial
        406: Not enough balance
        407: Error createte config
        :param item_id: int
        :param user_id: int
        :param tariff_id: int
        :param trial: bool
        :return: backend_models.BuyConfigResponse
        """
        url = self._base_url + self._buy_config_url
        data = {
            "user_id": user_id,
            "item_id": item_id,
            "tariff_id": tariff_id,
            "trial": trial,
        }

        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()

            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=data, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.BuyConfigResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.BuyConfigResponse(status=False, msg=err.json(), status_code=500)

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.BuyConfigResponse(status=False,
                                                                msg=f'bad status code: {resp.status}',
                                                                status_code=500,
                                                                )

    # Получить список всех конфигов пользователя
    async def get_all_user_config(self, user_id: int):
        url = f'{self._base_url}{self.get_all_configs_users_url}/{user_id}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.AllUserConfigs.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.AllUserConfigs(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.AllUserConfigs(status=False, msg=f'bad status code: {resp.status}')

    # Удалить конфиг пользователя с сервера SHARED VPN
    async def dell_config_ovpn(self, ovpn_transaction_id: int):
        url = self._base_url + self._dell_config_url + f'/{ovpn_transaction_id}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()

            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.delete(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.DellConfigResult.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.DellConfigResult(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.DellConfigResult(status=False, msg=f'bad status code: {resp.status}')

    # Изменить статус продления конфига
    async def update_autopayer(self, order_id: int):
        url = f'{self._base_url}{self._update_autopayer_url}/{order_id}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.UpdateAutopayerResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.UpdateAutopayerResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.UpdateAutopayerResponse(
                            status=False, msg=f'bad status code: {resp.status}')

    # Получить список всех конфигов с указанным количеством дней до оккончания действия
    async def get_exp_configs(self, days_before_disconnection: int):
        url = self._base_url + self.get_exp_configs_url + f'/{days_before_disconnection}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()

            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.GetExpConfigsResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.GetExpConfigsResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.GetExpConfigsResponse(status=False, msg=f'bad status code: {resp.status}')

    # --- NEW
    async def get_extend_configs(self) -> backend_models.AutoActivationRespsone:
        """
        Получить список продлленный конфигов
        Status Codes for reports:
            201 - Конфиг деактивирван из-за отлюченнго автопродления
            400 - Деактивирован из-за нехватки баланса
            401 - Не удалось деактивирвать конфиг
            ---
            Status code for response:
            404 - Not dound configs to activations
            500 - Items/tariffs not found
            501 - backend_error
        """

        url = self._base_url + self._get_extend_configs_url
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()

            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.AutoActivationRespsone.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.AutoActivationRespsone(status=False, msg=err.json(), status_code=501)

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.AutoActivationRespsone(
                            status=False,
                            msg=f'bad status code: {resp.status}',
                            status_code=501,
                        )

    # --- OLD
    async def extend_config(self,
                            config_id: int,
                            extend_price: int,
                            ) -> backend_models.ExtendConfigResponse:
        """
        Status cods response:
            100 - Не найден VPN конфиг для продления
            101 - Не найден сервер для продления
            102 - Сервер не активен
            103 - Ошибка деакктивации конфига
            104 - Конфиг успешно деактивирован из-за отлюченнго продления
            105 - Ошибка продления конфига!
            106 - Ошибка деативация конфига при нехватке баланса
            107 - Конфиг успешно деактивирван из-за не хватки баланса
            200 - Конфиг продлен
            404 - Неизвестная ошибка
        """
        url = self._base_url + self.extend_config_url
        data = {
            "config_id": config_id,
            "extend_price": extend_price,
        }
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()

            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=data, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.ExtendConfigResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.ExtendConfigResponse(status=False, msg=err.json(), status_code=0)

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.ExtendConfigResponse(
                            status=False,
                            msg=f'bad status code: {resp.status}',
                            status_code=0,
                        )


# Платежки
class SharedOVPNPayments(Baselogin):
    """Класс бекенда платежек SharedOVPN"""

    def __init__(self):
        super().__init__()
        self.__header: Optional[Dict] = None
        self._base_url = f'{backend_base_url}/v1/shared-ovpn/payments/'
        self._create_free_kassa_pay_url = 'create-free-kassa-pay'
        self._get_last_temp_transaction_url = 'get-last-temp-transaction'
        self._shared_vpn_success_pay_url = 'shared-vpn-success-pay'

    # Создать новую FreeKassa платежку
    async def create_free_kassa_pay(self, user_id: int, plan_id: int):
        url = self._base_url + self._create_free_kassa_pay_url
        sign = hmac.new(
            bytearray(api_key, 'utf-8'),
            bytearray(f'{user_id}|{private_key}', 'utf-8'),
            hashlib.sha256).hexdigest()
        data = {
            "signature": sign,
            "user_id": user_id,
            "plan_id": plan_id,
        }

        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()

            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=data, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.NewFKPay.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.NewFKPay(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.NewFKPay(status=False, msg=f'bad status code: {resp.status}')


class OVPNItems(Baselogin):
    """Ендпоинты SharedOVPNPlans - планы подписок"""

    def __init__(self):
        super().__init__()
        self.__header: Optional[Dict] = None
        self._base_url = f'{backend_base_url}/v1/shared-ovpn/items/'
        self._get_active_items_url = 'get-active-items'
        self._search_country_url = 'search-country'
        self._get_item_url = 'get-item/'
        self._get_item_by_type_url = 'get-by-type/{item_type}'

    # Получить все активные items
    async def get_active_items(self):
        url = f'{self._base_url}{self._get_active_items_url}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.GetOvpnItemsResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.GetOvpnItemsResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.GetOvpnItemsResponse(status=False, msg=f'bad status code: {resp.status}')

    async def get_item_by_type(self, item_type: str) -> backend_models.GetOvpnItemsResponse:
        url = self._base_url + self._get_item_by_type_url.format(item_type=item_type)
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.GetOvpnItemsResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.GetOvpnItemsResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.GetOvpnItemsResponse(status=False,
                                                                   msg=f'bad status code: {resp.status}')

    # Получить все активные items
    async def search_sountry(self, query: str):
        url = f'{self._base_url}{self._search_country_url}/{query}'

        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.GetOvpnItemsResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.GetOvpnItemsResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.GetOvpnItemsResponse(status=False,
                                                                   msg=f'bad status code: {resp.status}')

    # Получить инфо item по item_id
    async def get_item(self, item_id: str) -> backend_models.GetItemResponse:
        url = self._base_url + self._get_item_url + f'{item_id}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.GetItemResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.GetItemResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.GetItemResponse(status=False, msg=f'bad status code: {resp.status}')


class VPNTaiffsAPI(Baselogin):
    def __init__(self):
        super().__init__()
        self.__header: Optional[Dict] = None
        self._base_url = f'{backend_base_url}/v1/vpn-tariffs/'
        self._get_active_tariffs_url = 'get-active-tariffs'

    async def get_active_tariffs(self) -> backend_models.GetAllVPNTariffs:
        url = f'{self._base_url}{self._get_active_tariffs_url}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.GetAllVPNTariffs.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.GetAllVPNTariffs(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.GetAllVPNTariffs(status=False, msg=f'bad status code: {resp.status}')


class UUIDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            # if the obj is uuid, we simply return the value of uuid
            return str(obj)
        return json.JSONEncoder.default(self, obj)
