import asyncio

from pydantic import ValidationError
from typing import Optional, Dict
import aiohttp

from models.backend import backend_models
from configs.config import bot_name, backend_base_url
from models.backend.auth import Baselogin


class AnalyticAPI(Baselogin):
    """Класс апи бекенда аналитики"""

    def __init__(self):
        super().__init__()
        self.__header: Optional[Dict] = None

        self._base_url = 'http://127.0.0.1:8000/v1/analytics/'
        self._base_stats_url = 'base-stats'
        self._get_vpn_country_stats_url = 'get-vpn-country-stats'

    # получить базовую статистику партнера
    async def get_base_stats(self) -> backend_models.BaseStatsResponse:
        url = self._base_url + self._base_stats_url
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.BaseStatsResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.BaseStatsResponse(status=False, msg=err.json())
                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.BaseStatsResponse(status=False, msg=f'bad status code: {resp.status}')

    # получить статистику по использования геолокаций VPN
    async def get_vpn_country_stats(self) -> backend_models.GetVpnCountryStats:
        url = self._base_url + self._get_vpn_country_stats_url
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.GetVpnCountryStats.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.GetVpnCountryStats(status=False, msg=err.json())
                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.GetVpnCountryStats(status=False,
                                                                 msg=f'bad status code: {resp.status}',
                                                                 )
