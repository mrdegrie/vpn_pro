import aiohttp
import hmac
import hashlib
from pydantic import ValidationError
from datetime import datetime
from typing import Optional, Dict
import asyncio

from models.backend import backend_models
from configs.config import api_key, private_key, bot_name, backend_base_url
from models.backend.auth import Baselogin


# Платежки
class PaymentAPI(Baselogin):
    """Класс бекенда платежек"""

    def __init__(self):
        super().__init__()
        self.__header: Optional[Dict] = None

        self._base_url = f'{backend_base_url}/v1/payments/'
        self._get_active_wallet_url = 'get-active-wallet'
        self._create_free_kassa_pay_url = 'create-free-kassa-pay'
        self._create_lava_pay_url = 'crete-new-lava-payment'
        self._get_last_temp_transaction_url = 'get-last-temp-transaction'
        self.fk_success_pay_url = 'fk-success-pay'
        self.get_temp_transactions_url = 'get-temp-transactions'
        self._create_payok_pay_url = 'crete-new-payok-payment'
        self._get_info_temp_transaction_url = 'get-info-temp-transaction'

    # Получить платежные методы
    async def get_active_wallet(self, location: str):
        url = self._base_url + self._get_active_wallet_url + f'/{location}'
        for index in range(5):
            conn = aiohttp.TCPConnector()
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }

            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.AllWallet.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.AllWallet(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.AllWallet(status=False, msg=f'bad status code: {resp.status}')

    # Создать новую FreeKassa платежку
    async def create_free_kassa_pay(self, user_id: int, amount: int, user_name: str = None):
        url = self._base_url + self._create_free_kassa_pay_url
        sign = hmac.new(
            bytearray(api_key, 'utf-8'),
            bytearray(f'{user_id}|{private_key}', 'utf-8'),
            hashlib.sha256).hexdigest()
        data = {
            "signature": sign,
            "user_id": user_id,
            "user_name": user_name,
            "amount": amount,
        }
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=data, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.NewFKPay.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.NewFKPay(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)
                    else:
                        return backend_models.NewFKPay(status=False, msg=f'bad status code: {resp.status}')

    # Получить временные траназкции укзанного периода
    async def get_last_temp_transaction(self, date_range: datetime) -> backend_models.LastTempTransaction:
        url = self._base_url + self._get_last_temp_transaction_url
        data = {
            "range": str(date_range),
        }

        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=data, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.LastTempTransaction.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.LastTempTransaction(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.LastTempTransaction(status=False,
                                                                  msg=f'bad status code: {resp.status}')

    # Произвести успешную оплату трранзакции
    async def fk_success_pay(self, order_id: int) -> backend_models.SuccessFKPay:
        url = self._base_url + self.fk_success_pay_url + f'/{order_id}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()

            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.SuccessFKPay.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.SuccessFKPay(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.SuccessFKPay(status=False, msg=f'bad status code: {resp.status}')

    async def get_temp_transactions(self,
                                    period: int,
                                    status_transations: str = 'created',
                                    ) -> backend_models.GetTempTransResponse:
        """Получить временные транзхакции соозданные N кол дней назад"""

        url = self._base_url + self.get_temp_transactions_url
        data = {
            "period": period,
            "status_transaction": status_transations,
        }
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=data, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.GetTempTransResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.GetTempTransResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.GetTempTransResponse(status=False, msg=f'bad status code: {resp.status}')

    async def create_payok_pay(self,
                               user_id: int,
                               amount: int,
                               user_name: str = None,
                               currency: Optional[str] = 'USD',
                               ) -> backend_models.CreateNewPaymentPayokResponse:
        """Создать новую Payok.io платежку"""

        url = self._base_url + self._create_payok_pay_url
        sign = hmac.new(
            bytearray(api_key, 'utf-8'),
            bytearray(f'{user_id}|{private_key}', 'utf-8'),
            hashlib.sha256).hexdigest()
        data = {
            "signature": sign,
            "user_id": user_id,
            "user_name": user_name,
            "currency": currency,
            "amount": amount,
        }
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=data, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.CreateNewPaymentPayokResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.CreateNewPaymentPayokResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.CreateNewPaymentPayokResponse(status=False,
                                                                            msg=f'bad status code: {resp.status}')

    async def get_info_temp_transaction(self,
                                        order_id: int,
                                        ) -> backend_models.GetTempTransactionResponse:
        """Создать новую Payok.io платежку"""

        url = self._base_url + self._get_info_temp_transaction_url + f'/{order_id}'

        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.GetTempTransactionResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.GetTempTransactionResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.GetTempTransactionResponse(status=False,
                                                                         msg=f'bad status code: {resp.status}')

    async def create_lava_pay(self,
                              user_id: int,
                              amount: int,
                              user_name: str = None,
                              ) -> backend_models.CreateNewLavaPaymentResponse:
        """Создать новую LAVA платежку"""

        url = self._base_url + self._create_lava_pay_url
        data = {
            "user_id": user_id,
            "user_name": user_name,
            "count": amount,
        }
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=data, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.CreateNewLavaPaymentResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.CreateNewLavaPaymentResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)
                    else:
                        return backend_models.CreateNewLavaPaymentResponse(status=False, msg=f'bad status code: {resp.status}')
