from pydantic import ValidationError
from typing import Optional, Dict
import aiohttp
import asyncio

from models.backend import backend_models
from configs.config import bot_name, backend_base_url
from models.backend.auth import Baselogin


class PartnerAPI(Baselogin):
    """Класс апи бекенда партнерской программы"""

    def __init__(self):
        super().__init__()
        self.__header: Optional[Dict] = None

        self._base_url = f'{backend_base_url}/v1/partner-system/'
        self._get_base_stats_url = 'get-base-stats'
        self._create_request_payment_url = 'create-request-payment'

    # получить базовую статистику партнера
    async def get_base_stats(self, user_id: int):
        url = self._base_url + self._get_base_stats_url + f'/{user_id}'

        for index in range(5):
            conn = aiohttp.TCPConnector()
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }

            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.ResponseBasePartnerStats.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.PlanInfoResult(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.ResponseBasePartnerStats(status=False,
                                                                       msg=f'bad status code: {resp.status}')

    async def create_request_payment(self,
                                     user_id: int,
                                     amount: float,
                                     currency: str,
                                     user_wallet: str,
                                     user_name: Optional[str] = None,

                                     ):
        """Создать выплату партнеру"""
        url = self._base_url + self._create_request_payment_url

        for index in range(5):
            conn = aiohttp.TCPConnector()
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            if not user_name:
                user_name = 'anonimus'
            data = {
                "user_id": user_id,
                "user_name": user_name,
                "amount": amount,
                "currency": currency,
                "user_wallet": user_wallet,
            }
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=data, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.NewPaymentRequest.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.PlanInfoResult(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.NewPaymentRequest(status=False,
                                                                msg=f'bad status code: {resp.status}')
