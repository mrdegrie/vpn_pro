import aiohttp
import logging
from typing import Union
from pydantic import ValidationError
from configs.config import api_key, backend_base_url
from models.backend import backend_models
from models.uttils.cash import Cash


class Baselogin:
    """Класс авторизации на бекенде"""

    def __init__(self):
        self._login_url: str = f'{backend_base_url}/auth/token-auth'

    async def login(self) -> Union[str, bool]:
        """Произвести вторизцию на беэннде, получить ктуальный токен авторизации"""

        conn = aiohttp.TCPConnector()
        data = {
            "token": api_key
        }
        async with aiohttp.ClientSession(connector=conn) as session:
            async with session.post(self._login_url, json=data, ssl=False) as resp:
                if resp.status == 200:
                    response = await resp.text(encoding='utf-8')
                    try:
                        auth_result = backend_models.BaseLoginResult.parse_raw(response)
                        if not auth_result.status:
                            return False

                        return auth_result.access_token

                    except ValidationError as err:
                        logging.error(err.json())
                        return False
                else:
                    return False

    async def _base_auth(self) -> str:
        """Получить токен авторизации из кеша, если устарел - обновить"""

        cash = Cash(exp=3600)
        access_token = cash.get_auth_token()
        if not access_token:
            # токена нет, производим логин
            access_token = await self.login()
            if not access_token:
                cash.close()
                return 'Bas auth'

            cash.client.set(key='access_token', value=access_token, expire=cash.exp)
        else:
            access_token = access_token.decode("utf-8")
        cash.close()
        return access_token

    async def _refresh_token(self) -> Union[str, bool]:
        access_token = await self.login()
        if not access_token:
            return False

        cash = Cash(exp=3600)
        cash.client.set(key='access_token', value=access_token, expire=cash.exp)
        cash.close()
        return access_token


