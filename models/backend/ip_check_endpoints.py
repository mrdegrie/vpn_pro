from pydantic import ValidationError
from typing import Optional, Dict, List
import aiohttp
import asyncio

from models.backend import backend_models
from configs.config import bot_name, backend_base_url
from models.backend.auth import Baselogin


class IPCheckAPIv1(Baselogin):
    def __init__(self):
        super().__init__()
        self.__header: Optional[Dict] = None

        self._base_url = f'{backend_base_url}/v1/checker-ip/'
        self._check_one_ip_url = 'checking-one-address'
        self._multi_check_url = 'checking-multiple'
        self._get_last_history_url = 'get-last-checks/{user_id}'

    async def check_one_ip_adress(self,
                                  user_id: int,
                                  ip: str,
                                  ) -> backend_models.IpCheckResult:
        """Пробить один IP адресс"""
        url = self._base_url + self._check_one_ip_url + f'/{user_id}/{ip}'

        for index in range(5):
            conn = aiohttp.TCPConnector()
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }

            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.IpCheckResult.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.IpCheckResult(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)
                    else:
                        return backend_models.IpCheckResult(status=False, msg=f'bad status code: {resp.status}')

    async def multi_check(self,
                          user_id: int,
                          ip_list: List[str],
                          ) -> backend_models.MultiCheckResponse:
        """
        Пробить список ip адресов
        Status Codes:
            200 - OK
            404  - User not found
            405 - Not enough balance
            500 - Checker not found
        """
        url = self._base_url + self._multi_check_url

        for index in range(5):
            conn = aiohttp.TCPConnector()
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            payload = {
                "user_id": user_id,
                "ip": ip_list,
            }
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=payload, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.MultiCheckResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.MultiCheckResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)
                    else:
                        return backend_models.MultiCheckResponse(status=False, msg=f'bad status code: {resp.status}')

    async def get_last_history(self,
                               user_id: int,
                               ) -> backend_models.CheckerHistoryResponse:
        """Пробить один IP адресс"""
        url = self._base_url + self._get_last_history_url.format(user_id=user_id)

        for index in range(5):
            conn = aiohttp.TCPConnector()
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }

            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.CheckerHistoryResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.CheckerHistoryResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)
                    else:
                        return backend_models.CheckerHistoryResponse(
                            status=False, msg=f'bad status code: {resp.status}')
