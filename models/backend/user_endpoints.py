import aiohttp
from typing import Optional, Dict
from pydantic import ValidationError
import asyncio

from models.backend import backend_models
from configs.config import bot_name, backend_base_url
from models.backend.auth import Baselogin


class UserAPIv1(Baselogin):
    """Класс для взаимодействия с бекендом. Пользовательские методы v1"""

    def __init__(self):
        super().__init__()
        self.__header: Optional[Dict] = None

        self._base_url = f'{backend_base_url}/v1/users/'
        self._add_user = 'add-user'
        self._get_user_info_url = 'get-user-info'
        self._update_user_balance_url = 'update-user-balance'
        self._get_users_of_mailing_url = 'get-users-of-mailing'
        self._get_users_of_private_trial = 'get-users-of-private-trial'
        self._get_language_url = 'get-language'
        self._update_language_url = 'update-language'

    # Добавить пользователя в систему
    async def add_user(self,
                       user_id: int,
                       language: str,
                       user_name: Optional[str] = None,
                       partner_id: Optional[int] = 666,
                       role: Optional[str] = 'user',
                       ) -> backend_models.AddUserResponse:

        url = self._base_url + self._add_user
        data = {
            "user_id": user_id,
            "user_name": user_name,
            "partner_id": partner_id,
            "role": role,
            "language": language,
        }
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=data, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.AddUserResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.AddUserResponse(status=False, message=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)
                    else:
                        return backend_models.AddUserResponse(status=False, message=f'bad status code: {resp.status}')

    # Получить информацию пользователя
    async def get_user_info(self, user_id: int) -> backend_models.UserInfoResponse:
        url = f'{self._base_url}{self._get_user_info_url}/{user_id}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()

            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.UserInfoResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.UserInfoResponse(status=False, message=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.UserInfoResponse(status=False, message=f'bad status code: {resp.status}')

    async def get_user_language(self, user_id: int) -> backend_models.GetUserLanguage:
        """Получить язык пользователя"""

        url = f'{self._base_url}{self._get_language_url}/{user_id}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()

            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.GetUserLanguage.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.GetUserLanguage(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.GetUserLanguage(status=False, msg=f'bad status code: {resp.status}')

    async def update_user_language(self, user_id: int, language: str) -> backend_models.UpdateLanguageResponse:
        """Обновить язык пользователя"""

        url = f'{self._base_url}{self._update_language_url}/{user_id}'
        data = {
            "language": language,
        }
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()

            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=data, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.UpdateLanguageResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.UpdateLanguageResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.UpdateLanguageResponse(status=False, msg=f'bad status code: {resp.status}')

    # Обновить баланс пользователя
    async def update_user_balance(self, user_id: int,
                                  amount: int,
                                  updat_mode: str = 'add') -> backend_models.UpdateUserBalanceResponse:
        """

        :param user_id:
        :param amount:
        :param updat_mode:
        :return:
        """
        url = self._base_url + self._update_user_balance_url
        data = {
            "user_id": user_id,
            "update_mode": updat_mode,
            "amount": amount,
        }
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.post(url, json=data, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.UpdateUserBalanceResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.UpdateUserBalanceResponse(status=False, msg=err.json())

                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)

                    else:
                        return backend_models.UpdateUserBalanceResponse(
                            status=False, msg=f'bad status code: {resp.status}')

    async def get_users_of_mailing(self, pattern: str) -> backend_models.GetUsersMailingResponse:
        """

        Получить список пользователей для рассылки с учеттом выбранного патерна
        users_no_purchases: все кто не совершал успешныых оплат
        yes_purchases: все кто совершал успешную оплату

        """

        url = self._base_url + self._get_users_of_mailing_url + f'/{pattern}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.GetUsersMailingResponse.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.GetUsersMailingResponse(status=False, msg=err.json())
                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)
                    else:
                        return backend_models.GetUsersMailingResponse(status=False,
                                                                      msg=f'bad status code: {resp.status}')

    async def get_users_of_private_trial_activate(self,
                                                  pattern: str) -> backend_models.GetUsersFromPrivateTrial:
        """

        Получить список пользователей для активации триалки с учеттом выбранного патерна
        users_no_purchases: все кто не совершал успешныых оплат
        yes_purchases: все кто совершал успешную оплату

        """

        url = self._base_url + self._get_users_of_private_trial + f'/{pattern}'
        for index in range(5):
            acces_token = await self._base_auth()
            self.__header = {
                "User-Agent": bot_name,
                "Authorization": f"Bearer {acces_token}",
            }
            conn = aiohttp.TCPConnector()
            async with aiohttp.ClientSession(connector=conn, headers=self.__header) as session:
                async with session.get(url, ssl=False) as resp:
                    if resp.status == 200:
                        response = await resp.text(encoding='utf-8')
                        try:
                            return backend_models.GetUsersFromPrivateTrial.parse_raw(response)
                        except ValidationError as err:
                            return backend_models.GetUsersFromPrivateTrial(status=False, msg=err.json())
                    elif resp.status == 400:
                        await self._refresh_token()
                        await asyncio.sleep(1)
                    else:
                        return backend_models.GetUsersFromPrivateTrial(status=False,
                                                                      msg=f'bad status code: {resp.status}')
