from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup, WebAppInfo
from typing import Optional
import ipaddress
import os
import json
from random import randint

# from encrypto import Encrypto
from models.ui.ui_storage import UIControl
from models.backend.ip_check_endpoints import IPCheckAPIv1
from models.uttils.file_downloader import LoadFile
from models.backend import backend_models
from configs.config import backend_remote_domain


class IPCheckControl:
    """Класс чекера ip SCORE"""

    def __init__(self, user_id: int):
        self._user_id = user_id
        self._keyboard = InlineKeyboardMarkup()
        self._MSG: Optional[str] = None
        self._abount_the_service_url = f'{backend_remote_domain}' + '/v1/checker-ip/about-the-service/{lang}'
        self._privatecheck_url = f'{backend_remote_domain}' + '/v1/checker-ip/private-check-ip/{user_id}/{lang}'
        self._temp_file_name: Optional[str] = None

    @property
    def kayboard(self):
        return self._keyboard

    @property
    def mesage_text(self):
        return self._MSG

    async def main_page(self):
        """Create main page: IP SCORE CHECK"""
        ui = UIControl(self._user_id)
        await ui.get_language()

        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text("check_main_ip"),
                                 web_app=WebAppInfo(
                                     url=self._privatecheck_url.format(user_id=self._user_id, lang=ui.language))))
        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text("manual_check"), callback_data='manual_check'),
            InlineKeyboardButton(text=ui.get_text("check_from_the_file"), callback_data='opt_check'),
        )
        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text("checker_history_plug"), callback_data='checker_history_plug'),
            InlineKeyboardButton(text=ui.get_text("about_the_service"),
                                 web_app=WebAppInfo(url=self._abount_the_service_url.format(lang=ui.language))),
        )
        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='next_start_message'),
        )
        self._MSG = ui.get_text("ip_check_main_page")
        return self.kayboard

    async def ip_address_equest_page(self):
        """Страница запроса ip адреса пользователя"""
        ui = UIControl(self._user_id)
        await ui.get_language()

        self._MSG = ui.get_text('get_address_page')
        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page')
        )
        return self._keyboard

    async def request_file(self):
        """Страница запроса файла для массового чека IP"""
        ui = UIControl(self._user_id)
        await ui.get_language()

        self._MSG = ui.get_text('request_file_for_ip_check')
        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page')
        )
        return self._keyboard

    async def stat_check_adress(self, ip: str):
        """Произсвести чек адресса"""
        ui = UIControl(self._user_id)
        await ui.get_language()
        if not self.ip_validation(ip):
            self._MSG = ui.get_text("invalid_ip_adress_text")
            self._keyboard.row(
                InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page'))
            return self._keyboard
        # Пробиваем IP
        check_api = IPCheckAPIv1()
        result = await check_api.check_one_ip_adress(self._user_id, ip)
        if not result.status:
            if result.msg.lower() == 'not enough balance':
                # Не хватило баланса для чека
                self._MSG = ui.get_text("little_balance_for_check")
                self._keyboard.row(
                    InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page'))
                return self._keyboard

            # -  Если ошибка другая:
            self._MSG = ui.get_text("invalid_ip_adress_text")
            self._keyboard.row(
                InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page'))
            return self._keyboard

        self._MSG = ui.get_text('check_one_adress_esult').format(order=result.check_id)
        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text('get_result_ip_check'), web_app=WebAppInfo(url=result.web_resul)),
        )
        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page'),
            InlineKeyboardButton(text=ui.get_text("re_check"), callback_data='manual_check'),
        )
        return self._keyboard

    async def start_mass_check(self,
                               file_path: str,
                               ):
        """Произвести массовый чек IP из файла пользователя"""
        ui = UIControl(self._user_id)
        await ui.get_language()

        file_name = f'{randint(99999999, 11111111111)}.txt'
        loader = LoadFile()
        file = loader.load_file(remote_path=file_path, file_dir='storage/documents', file_name=file_name)
        if not file:
            self._MSG = ui.get_text("eror_load_files")
            self._keyboard.row(
                InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page'),
            )
            return self._keyboard

        try:
            with open(file, 'r') as user_file:
                ip_for_checking = []
                for ip in user_file.readlines():
                    try:
                        ipaddress.IPv4Address(ip.strip())
                        ip_for_checking.append(ip.strip())
                    except ValueError:
                        pass
                # ip_for_checking = [ip.strip() for ip in user_file.readlines()]
            os.remove(file)
        except:
            self._MSG = ui.get_text("eror_load_files")
            self._keyboard.row(
                InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page'),
            )
            return self._keyboard

        if not ip_for_checking:
            self._MSG = ui.get_text("eror_load_files")
            self._keyboard.row(
                InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page'),
            )
            return self._keyboard

        # -- DEMO MODE
        if len(ip_for_checking) > 100:
            self._MSG = ui.get_text("big_size_ip_file")
            self._keyboard.row(
                InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page'),
            )
            return self._keyboard

        check_api = IPCheckAPIv1()
        """
        Status Codes:
            200 - OK
            404  - User not found
            405 - Not enough balance
            500 - Checker not found
        """
        response = await check_api.multi_check(self._user_id, ip_for_checking)
        if response.status_code == 404:
            # Нет такого юзера
            self._MSG = ui.get_text("eror_multi_checking")
            self._keyboard.row(
                InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page'),
            )
            return self._keyboard

        elif response.status_code == 400:
            self._MSG = ui.get_text("few_addresses")
            self._keyboard.row(
                InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page'),
            )
            return self._keyboard

        elif response.status_code == 405:
            self._MSG = ui.get_text("little_balance_for_check")
            self._keyboard.row(
                InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page'),
            )
            return self._keyboard

        elif response.status_code == 500:
            # Ошибка чекера
            self._MSG = ui.get_text("eror_multi_checking")
            self._keyboard.row(
                InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page'),
            )
            return self._keyboard

        # Собираем юзеру txt документ с результатом пробива
        result = await self._write_check_to_file(user_id=self._user_id, check_result=response)
        self._MSG = ui.get_text("multi_check_adress_result").format(order=response.check_id)
        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text("get_result_ip_check"),
                                 web_app=WebAppInfo(url=response.web_result),
                                 ),
            InlineKeyboardButton(
                text=ui.get_text("download_file"),
                callback_data=f'down_check#{self._temp_file_name}') if result else None,
        )
        self._keyboard.row(
            InlineKeyboardButton(text=ui.get_text('next_start_message'), callback_data='main_ip_score_page'),
            InlineKeyboardButton(text=ui.get_text("re_check"), callback_data='opt_check'),
        )
        return self._keyboard

    def ip_validation(self, ip: str) -> bool:
        try:
            ipaddress.IPv4Address(ip)
            return True
        except ValueError:
            return False

    async def get_checker_history_to_file(self, user_id: int):
        """Получить историю прбива в .txt file"""
        ui = UIControl(user_id)
        await ui.get_language()
        checker_api = IPCheckAPIv1()
        history = await checker_api.get_last_history(user_id)
        if not history.history:
            self._MSG = ui.get_text('checker_history_not_found')
            self._keyboard.row(
                InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='next_start_message'),
            )
            return False

        main_text = '--- YouFast Checker ---\n\n'
        file_name = f'History#{user_id}.txt'

        for hi in history.history:
            main_text += f'CheckID: {hi.order_id}\n' \
                         f'Time: {hi.check_time.strftime("%d.%m.%Y")}\n' \
                         f'Price: ${round(hi.amount, 3)}\n\n'

            if hi.ip_count > 1:
                payload = backend_models.MultiCheckResponse.parse_raw(json.loads(hi.payload))
                check_text = await self._parsing_multi_check(payload, ui)
                main_text += check_text

            else:
                payload = backend_models.IpCheckResult.parse_raw(json.loads(hi.payload))
                check_text = await self._parsing_one_check(ip=payload.ip, ui=ui, check=payload)
                main_text += check_text

        try:
            with open(f'storage/checker/{file_name}', 'w') as history_file:
                history_file.write(main_text)
            self._MSG = ui.get_text('history_you_fast_file')
            return file_name

        except:
            self._MSG = ui.get_text('checker_history_not_found')
            self._keyboard.row(
                InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='next_start_message'),
            )
            return False

    async def _parsing_multi_check(self,
                                   payload: backend_models.MultiCheckResponse,
                                   ui: UIControl,
                                   ) -> str:

        message_text = ''
        for check in payload.cheking:
            scan_status = False
            scan_ports = ''
            for port in check.ports:
                if check.ports[port]:
                    scan_status = True
                    scan_ports += f'{port}: Opened ⛔️\n'
            if not scan_status:
                scan_ports = ui.get_text('open_ports_not_found')
            message_text += ui.get_text('checker_result_template').format(
                ip=check.ip,
                continent=check.geo_data.continent,
                continentCode=check.geo_data.continentCode,
                country=check.geo_data.country,
                countryCode=check.geo_data.continentCode,
                region=check.geo_data.region,
                regionName=check.geo_data.regionName,
                city=check.geo_data.city,
                zip=check.geo_data.zip,
                lat=check.geo_data.lat,
                lon=check.geo_data.lon,
                timezone=check.geo_data.timezone,
                currency=check.geo_data.currency,
                callingCode=check.geo_data.callingCode,
                isp=check.org_data.isp,
                as_=check.org_data.as_,
                asname=check.org_data.asname,
                reverse=check.org_data.reverse,
                port_scan=scan_ports,
                proxy_value=ui.get_text('low_lvl') if check.connection_data.proxy else ui.get_text('high_lvl'),
                vpn_value=ui.get_text('low_lvl') if check.connection_data.vpn_risk else ui.get_text('high_lvl'),
                mobile_ip=ui.get_text('yes_lvl') if check.connection_data.mobile else ui.get_text('no_lvl'),
                resident_ip=ui.get_text('yes_lvl') if check.connection_data.resident else ui.get_text('no_lvl'),
                hosting=ui.get_text('yes_lvl') if check.connection_data.hosting else ui.get_text('no_lvl'),
                active_vpn=ui.get_text('yes_lvl') if check.connection_data.active_vpn else ui.get_text('no_lvl'),
                active_tor=ui.get_text('yes_lvl') if check.connection_data.active_tor else ui.get_text('no_lvl'),
                is_bot=ui.get_text('low_lvl') if check.connection_data.bot else ui.get_text('high_lvl'),
                fraud_score_block=ui.get_text('fraud_score_block_plug'),
            )
        return message_text

    async def _parsing_one_check(self,
                                 ip: str,
                                 ui: UIControl,
                                 check: backend_models.IpCheckResult,
                                 ) -> str:
        # сфрмироовать инфо о сканировании портов
        scan_status = False
        scan_ports = ''
        for port in check.ports:
            if check.ports[port]:
                scan_status = True
                scan_ports += f'{port}: Opened ⛔️\n'

        if not scan_status:
            scan_ports = ui.get_text('open_ports_not_found')

        fraund = f'Fraund: {check.score.fraund_score}\n' \
                 f'Risk: {check.score.risk_score}\n'
        print(check.connection_data)

        return ui.get_text('checker_result_template').format(
            ip=ip,
            continent=check.geo_data.continent,
            continentCode=check.geo_data.continentCode,
            country=check.geo_data.country,
            countryCode=check.geo_data.continentCode,
            region=check.geo_data.region,
            regionName=check.geo_data.regionName,
            city=check.geo_data.city,
            zip=check.geo_data.zip,
            lat=check.geo_data.lat,
            lon=check.geo_data.lon,
            timezone=check.geo_data.timezone,
            currency=check.geo_data.currency,
            callingCode=check.geo_data.callingCode,
            isp=check.org_data.isp,
            as_=check.org_data.as_,
            asname=check.org_data.asname,
            reverse=check.org_data.reverse,
            port_scan=scan_ports,
            proxy_value=ui.get_text('high_lvl') if check.connection_data.proxy is True else ui.get_text('low_lvl'),
            vpn_value=ui.get_text('high_lvl') if check.connection_data.vpn_risk is True else ui.get_text('low_lvl'),
            mobile_ip=ui.get_text('yes_lvl') if check.connection_data.mobile is True else ui.get_text('no_lvl'),
            resident_ip=ui.get_text('yes_lvl') if check.connection_data.resident is True else ui.get_text('no_lvl'),
            hosting=ui.get_text('yes_lvl') if check.connection_data.hosting is True else ui.get_text('no_lvl'),
            active_vpn=ui.get_text('yes_lvl') if check.connection_data.active_vpn is True else ui.get_text('no_lvl'),
            active_tor=ui.get_text('yes_lvl') if check.connection_data.active_tor is True else ui.get_text('no_lvl'),
            is_bot=ui.get_text('high_lvl') if check.connection_data.bot is True else ui.get_text('low_lvl'),
            fraud_score_block=fraund,
        )

    async def _write_check_to_file(self, user_id: int, check_result: backend_models.MultiCheckResponse):
        """Сохранить результат массового чека в файл .txt"""

        ui = UIControl(user_id)
        await ui.get_language()

        main_text = '--- YouFast Checker ---\n\n'
        self._temp_file_name = f'{check_result.check_id}.txt'
        for check in check_result.cheking:
            # сфрмироовать инфо о сканировании портов
            scan_status = False
            scan_ports = ''
            for port in check.ports:
                if check.ports[port]:
                    scan_status = True
                    scan_ports += f'{port}: Opened ⛔️\n'
            if not scan_status:
                scan_ports = ui.get_text('open_ports_not_found')

            main_text += ui.get_text('checker_result_template').format(
                ip=check.ip,
                continent=check.geo_data.continent,
                continentCode=check.geo_data.continentCode,
                country=check.geo_data.country,
                countryCode=check.geo_data.continentCode,
                region=check.geo_data.region,
                regionName=check.geo_data.regionName,
                city=check.geo_data.city,
                zip=check.geo_data.zip,
                lat=check.geo_data.lat,
                lon=check.geo_data.lon,
                timezone=check.geo_data.timezone,
                currency=check.geo_data.currency,
                callingCode=check.geo_data.callingCode,
                isp=check.org_data.isp,
                as_=check.org_data.as_,
                asname=check.org_data.asname,
                reverse=check.org_data.reverse,
                port_scan=scan_ports,
                proxy_value=ui.get_text('low_lvl') if check.connection_data.proxy else ui.get_text('high_lvl'),
                vpn_value=ui.get_text('low_lvl') if check.connection_data.vpn_risk else ui.get_text('high_lvl'),
                mobile_ip=ui.get_text('yes_lvl') if check.connection_data.mobile else ui.get_text('no_lvl'),
                resident_ip=ui.get_text('yes_lvl') if check.connection_data.resident else ui.get_text('no_lvl'),
                hosting=ui.get_text('yes_lvl') if check.connection_data.hosting else ui.get_text('no_lvl'),
                active_vpn=ui.get_text('yes_lvl') if check.connection_data.active_vpn else ui.get_text('no_lvl'),
                active_tor=ui.get_text('yes_lvl') if check.connection_data.active_tor else ui.get_text('no_lvl'),
                is_bot=ui.get_text('low_lvl') if check.connection_data.bot else ui.get_text('high_lvl'),
                fraud_score_block=ui.get_text('fraud_score_block_plug'),
            )
        try:
            with open(f'storage/checker/{self._temp_file_name}', 'w') as check_file:
                check_file.write(main_text)
            return True
        except:
            return False
