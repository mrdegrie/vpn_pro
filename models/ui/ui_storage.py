from typing import Optional
from aioredis import Redis

from models.users.user_control import UserControl
# from models.ui.locales.available_localizations import check_localization_availability
from database.db_connected import get_redis
from models.ui.locales.ru_RU import ru_locale
from models.ui.locales.en_EN import en_locale


class UIControl:
    def __init__(self, user_id: int, language: str = None):
        self._user_id = user_id

        self._language: Optional[str] = language
        # -- Mailing
        self._redis: Optional[Redis] = None  # Обьект редиса

    @property
    def language(self):
        """Актуальный язык пользователя"""
        return self._language

    @property
    def user_id(self):
        return self._user_id

    async def _language_refrash(self, redis: Redis):
        user_control = UserControl()
        language = await user_control.get_user_language(self._user_id)
        if not language:
            self._language = 'en'
            return self._language

        await redis.set(name=f'lang_{self._user_id}', value=language)
        self._language = language
        return self._language

    async def get_language(self):
        _redis = await get_redis()
        language = await _redis.get(name=f'lang_{self._user_id}')
        if language is None:
            # Языка нет в кеше, берем его из бэкенда
            await self._language_refrash(_redis)

        if not self._language:
            self._language = language
        await _redis.close()
        return self._language

    async def update_language(self, language: str) -> bool:
        """Обновить язык пользователя  - в БД и в кеше"""
        # Апдейтим бэк:
        user_cont = UserControl()
        update = await user_cont.update_user_language(self._user_id, language)
        if not update:
            return False
        # Апдейтим кеш
        _redis = await get_redis()
        await _redis.set(name=f'lang_{self._user_id}', value=language)
        await _redis.close()
        self._language = language
        return True

    def get_text(self, ui_id: str):
        """Получить текст по ui_id"""

        if self._language == 'ru':
            return ru_locale[ui_id]

        elif self._language == 'en':
            return en_locale[ui_id]

        else:
            return en_locale[ui_id]

    async def get_language_for_mainling(self, user_id: int):
        """Получить язык для указанного юзера, для рассылки - не закрывть конект с редисом"""

        if self._redis is None:
            self._redis = await get_redis()

        language = await self._redis.get(name=f'lang_{user_id}')
        if language is None:
            # Языка нет в кеше, берем его из бэкенда и обновляем кеш
            user_control = UserControl()
            language = await user_control.get_user_language(user_id)
            if not language:
                # В бэке не оказалось, ставим дефолт значение
                self._language = 'en'
                return self._language

            await self._redis.set(name=f'lang_{user_id}', value=language)
            self._language = language
            return self._language

        else:
            self._language = language
            return self.language

    async def open_redis_connection(self):
        self._redis = await get_redis()
        return

    async def close_redis_connection(self):
        return await self._redis.close()
