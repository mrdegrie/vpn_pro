en_locale = {
    "main_menu_text_button": "ℹ️ Main menu",
    "shared_vpn": "🛡 VPN",
    "private_cabinet": '⚙️ Account',
    "main_ip_score_page": "🔓 IP Checker",
    "main_proxy_page": "🌏 Proxy",
    "partner_cabinet": "🤝 Affiliate",
    "channel_url_button": "🧩 Our channel",
    "vpn_info": "👥 About us",
    "help_meesage": "🆘 Help",
    "start_message": "🔥 Our servers have no limits on speed and traffic, "
                     "VPN works on all devices, YouTube in 4K - no delays!\n\n"
                     "🔥 Maximum anonymity and security that no other VPN service in the world can provide.\n\n"
                     "✅ Our channel: @YouFastVPN_en",
    "check_subs": "✅ Subscribed",
    "subscribe": "<b>⚠️ To continue, you need to subscribe to our channel 👇</b>",
    "error_message": "⛔️ You are still not subscribed",
    "lang_change": "🌏 Language",
    "private_cabinet_text": '<b>Account</b>\n\n'
                            '🆔 <code>{user_id}</code>\n'
                            '💰 Balance: <b>${balance}</b>\n\n'
                            '<i>The balance is used to pay for any services in the bot. '
                            'Use the keys below to fund it</i>',

    "all_payments": "💰 Add funds",
    "my_shared_config_list": "🛡 My VPNs",
    "backend_error": "<b>⛔️ An error has occurred – try again later or contact Support 😔</b>",
    "next_start_message": "🔙 Back",
    "FAQ_text": '<b>🆘 Need help?</b>\n\n'
                '❓ How to pay for a subscription?\n'
                '❓ How to use VPN?\n'
                '❓ How is it all set up?\n\n'
                '👉 <b>Check out our FAQ and find answers to these and other questions!</b>',
    "vpn_manual": "📄 Instructions",
    "FAQ": "🆘 FAQ",
    "ask_me": "💬 Contact us",
    "help_message": '1️⃣ Our VPN service uses OpenVPN Connect, with the Open VPN protocol with '
                    'open source to provide you with fast and secure Internet access. '
                    'We also improved traffic encryption, we use SSL for connection, '
                    'support all protocols (and there are no WebRTC leaks) and so on.\n\n'
                    '2️⃣ We use Telegram to distribute VPN, so we cant be blocked or '
                    'remove from appstores - YouFast VPN™ is with you wherever there is Telegram.\n\n'
                    '3️⃣ Unlike many free VPNs, we do not collect or sell any data about you. '
                    'Logs are deleted from our servers instantly, we do not store the history of your visits.\n\n'
                    '4️⃣ Our servers have no limits on speed and traffic '
                    '(we use servers with channels up to 10 Gbps), VPN works on all devices, '
                    'YouTube in 4K - no delay!\n\n✅ Our channel: @YouFastVPN',
    "what_device": "<b>What device do you have?</b>",
    "ios_vpn-user_manual": "🍏 iPhone / iPad",
    "android_vpn_user_manual": "🤖 Android",
    "macos_user_manual": "🍏 Mac",
    "win_user_manual": "🪟 Windows",
    "linux_user_manual": "🐧 Linux",
    "android_vpn_manual_text": 'Good thing its not an iPhone 🙂\n\n'
                               '1️⃣ Download and install the OpenVPN Connect app from '
                               '<a href="https://play.google.com/store/apps/details?id=net.openvpn.openvpn">'
                               'Google Play</a>\n\n'
                               '2️⃣ Download the config file received from us to your phone\n\n'
                               '3️⃣ Launch OpenVPN Connect. Click File, then Browse and '
                               'find the VPN config file. '
                               'For example, on our Android it went to Downloads / Telegram.\n\n'
                               '4️⃣ The OpenVPN Connect app will prompt you to import settings '
                               '(Import .ovpn profile?). Click OK, then Add.\n\n'
                               '5️⃣ To enable/disable VPN just move the slider '
                               'opposite the config in OpenVPN to the right and left. '
                               'Green slider means VPN is enabled.\n\n----------------------\n\n'
                               '⚠️ One config works on only one device at a time, one by one – you can.\n\n'
                               '⚠️ Dont share your VPN config with anyone, because the speed will drop. '
                               'To invite friends, its better to send them your link from the section '
                               'Affiliate Program – you will also earn on it.',
    "ios_vpn_manual_text": 'Good thing its not Android 🙂\n\n'
                           '1️⃣ Download and install the free OpenVPN Connect app from '
                           '<a href="https://itunes.apple.com/us/app/openvpn-connect/id590379981?mt=8">AppStore</a>\n\n'
                           '2️⃣ Click on the VPN config you received from us and "share" it with the OpenVPN app\n\n'
                           '3️⃣ On the File tab, select the downloaded config, click Add and Allow '
                           'add VPN configurations, then Connect.\n\n'
                           '4️⃣ To enable/disable VPN just move the slider '
                           'opposite the config in OpenVPN to the right and left. The green color of the slider means '
                           'that VPN is enabled.\n\n\n'
                           '----------------------\n\n'
                           '⚠️ One config works on only one device at a time, one by one – you can.\n\n'
                           '⚠️ Dont share your VPN config with anyone, because the speed will drop. '
                           'To invite friends, its better to send them your link from the section '
                           'Affiliate Program – you will also earn on it.',
    "macos_user_manual_text": 'Good thing its not Windows 🙂\n\n'
                              '1️⃣ Download the official app <a href="https://openvpn.net/downloads/'
                              'openvpn-connect-v3-macos.dmg">OpenVPN Connect</a> and install it.\n\n'
                              '2️⃣ Open the file downloads panel in the browser and double click for '
                              'app installation, complete installation.\n\n'
                              '3️⃣ Click on the VPN config you received from us with the file permission. ovpn.'
                              'OpenVPN will prompt you to import settings '
                              '(Import .ovpn profile?) – click OK, then Connect.\n\n'
                              '4️⃣ To enable / disable VPN, just move the slider '
                              'opposite the config in OpenVPN to the right and left. '
                              'The green slider means the VPN is enabled.\n\n\n'
                              '----------------------\n\n'
                              '⚠️ One config works on only one device at a time, one by one – you can.\n\n'
                              '⚠️ Dont share your VPN config with anyone, because the speed will drop. '
                              'To invite friends, its better to send them your link from the section '
                              'Affiliate Program – you will also earn on it.',
    "win_user_manual_text": 'Good thing its not macOS 🙂\n\n'
                            '1️⃣ Download the official app '
                            '<a href="https://openvpn.net/downloads/openvpn-connect-v3-windows.msi">OpenVPN Connect</a>'
                            ' and install it.\n\n'
                            '2️⃣ Open the file download panel in your browser and double click to install the program,'
                            ' complete the installation.\n\n'
                            '3️⃣ Save the VPN config you received from us and double click on it. '
                            'OpenVPN will prompt you to import settings '
                            '(Import .ovpn profile?) – click OK, then Connect.\n\n'
                            '4️⃣ To enable / disable VPN, just move the slider '
                            'opposite the config in OpenVPN to the right and left. '
                            'The green slider means the VPN is enabled.\n\n\n'
                            '----------------------\n\n'
                            '⚠️ One config works on only one device at a time, one by one – you can.\n\n'
                            '⚠️ Dont share your VPN config with anyone, because the speed will drop. '
                            'To invite friends, its better to send them your link from the section '
                            'Affiliate Program – you will also earn on it.',
    "linux_user_manual_text": 'Its good that its not Mac or Windows 🙂\n\n'
                              '1️⃣ Open the<a href="https://community.openvpn.net/openvpn/wiki/OpenVPN3Linux?_gl=1*1ccu4y1*_ga*MTgzMzU3NzkxOC4xNjYxMjg1Nzk1*_ga_SPGM8Y8Y79*MTY2MTI4NTc5NS4xLjEuMTY2MTI4NTgwNC4wLjAuMA..&_ga=2.209999893.1276507484.1661285795-1833577918.1661285795">installation instructions</a>'
                              ' for OpenVPN Connect and install the program.\n\n'
                              '2️⃣ Save the VPN config you received from us and open it via OpenVPN Connect.\n\n'
                              '3️⃣ To enable / disable VPN, just move the slider '
                              'opposite the config in OpenVPN to the right and left. '
                              'The green slider means the VPN is enabled.\n\n\n'
                              '----------------------\n\n'
                              '⚠️ One config works on only one device at a time, one by one – you can.\n\n'
                              '⚠️ Dont share your VPN config with anyone, because the speed will drop. '
                              'To invite friends, its better to send them your link from the section '
                              'Affiliate Program – you will also earn on it.',
    "wallet_crypto": "💰 Crypto",
    "wallet_bank_card": "💳 Bank card",
    "wallet_paypal": "🟡 PayPal",
    "other_methods_button": "➡️ Other methods",
    "choosing_payment_system": "<b>Choose a payment system for topping up your balance 👇</b>",
    "payment_error": "<b>🫤 The payment system is not active – try again later</b>",
    "select_deposit_amount": "<b>💰 Specify the amount to top up the balance\n\n</b>",
    "custom_amounnt_pay": "💰 Other amount",
    "successful_payment_text": "<b>✅ The payment has been paid, your balance has been funded by ${price}!</b>",
    "waiting_for_payment": '<b>⛔️ The payment has not yet been paid!\n\n</b>'
                           '<i>If you paid, but it is not credited for more than 20 minutes – '
                           'contact <a href="{support_link}">Support</a></i>',
    "to_pay_button": "🛍 Pay",
    "check_payment_button": "🔄 Check payment",
    "custom_deposit_amount": '<b>💰 Enter the amount to top up the balance in dollars\n\n'
                             '⛔️ MINIMAL AMOUNT – $2</b>',
    "new_payment_fk": '🛡 Payment <b>#{order_id}</b>\n\n'
                      '💰 Amount: <b>{price}$</b>\n'
                      '→ <b>For payment follow the link</b>\n\n'
                      '<b>⛔️ Funds will be received automatically within 1-5 minutes after payment</b>',
    "payment_creation_error": "<b>❌ Error creating payment - contact Support!</b>",
    "payment_amount_error": "⛔️ <b>Enter the amount as a number!\n\nMinimal amount – $2</b>",
    "amount_type_error": "⛔️ MINIMAL AMOUNT – $2",
    "user_block_message": "It looks like you are blocked in the bot",
    "search_country_button": "🔎 Search",
    "my_shared_config_list_button": "🛡 My VPNs",
    "country_for_VPN": "<b>Choose a country for your VPN ⬇️</b>",
    "no_countries": "All countries are busy, please try again later... 🕘",
    "general_error_message": "An error occurred, please try again later",
    "tariff_line": '<b>{country}\n\n'
                   '💰 The best VPN at the best price!</b>\n\n'
                   '├ 1 month: <b>${price}</b>\n'
                   '├ 6 months: <b>${month_6} (-10% discount)</b>\n'
                   '├ 1 year: <b>${year_1} (-20% discount)</b>\n'
                   '├ 3 years: <b>${year_3} (-40% discount)</b>',
    "bay_shared_ovpn_1": "🏷️ Buy for 1 month",
    "bay_shared_ovpn_6": "🏷️ Buy for 6 months",
    "bay_shared_ovpn_12": "🏷️ Buy for 1 year",
    "bay_shared_ovpn_36": "🏷️ Buy for 3 years",
    "trial_pay_button": "🎁 Free for {days} days",
    "user_device_selection": '<b>Select the device on which you want to use our VPN</b>\n\n'
                             '<i>Installation instructions for a specific device depend on this.</i>',
    "no_money_to_buy": '<b>❌ Not enough balance to buy – '
                       'recommend top up</b>',
    "config_inform": "\n\n<b><i>⚠️ The config may not work for the first 2-3 minutes, this is normal.</i></b>",
    "error_create_vpn": "<b>⛔️ VPN creation error🛡\nContact Support!</b>",
    "overview_configs": "<b>These are your VPN files to connect</b>",
    "no_vpn_purchased": "You don't have purchased VPNs yet 😐",
    "config_overview_text": '<b>🛡 OPEN VPN FILE (config)</b>\n\n'
                            '├ Country: <b>{country}</b>\n'
                            '├ Filename: \n    └<b> {country_2}_{ovpn_user_name}.ovpn</b>\n'
                            '├ Purchase date: <b>{order_time}</b>\n'
                            '├ Renewal date: <b>{exp_time}</b>\n'
                            '├ Renewal price: <b>${renewal_price}</b>\n'
                            '└ Auto renewal: <b>{auto_payer}</b>\n\n'
                            '❓ <i>If you need to re-download the file - click download below 🔽</i>',
    "to_extend_button": "Renewal",
    "download": '📤 Download',
    "error_read_file": '❌ Error reading file, please try again later!',
    "enabling_auto_renewal": 'Now the config will automatically renew from your balance',
    "disablet_auto_renewal": 'Now the config will NOT automatically renew from your balance',
    "partner_sysstem_general_text": '<b>🤝 Affiliate program\n\n</b>'
                                    '<i>Refer friends and earn {partner_percent}% from their deposits, '
                                    'lifetime!</i>\n\n'
                                    '<b>⬇️️ Your referral link:</b>\n'
                                    '└ <code>{ref_link}</code>\n\n'
                                    '🏅<b> Statistics:\n</b>'
                                    '├ Friends referred: <b>{all_users}</b>\n'
                                    '├ Friends who paid for a VPN: <b>{paid_users_specified_partner}</b>\n'
                                    '├ Total earned: <b>${all_profit}</b>\n'
                                    '└ Available for withdrawal: <b>${avaliable_balance}</b>',
    "share_a_link": "📢 Share link",
    "partner_payment_request": "💰 Withdrawal of funds",
    "request_money_text": '<b>💸 Withdrawals</b>\n\n'
                          '<i>You can withdraw funds to PayPal and USDT</i>\n\n'
                          'Minimal withdrawal amount – $10\n\n'
                          '💶 Available for withdrawal: <b>${avaliable_balance}</b>',
    "request_money#usdt": "💵 USDT (TRC20)",
    "request_money#paypal": "🟡 PayPal",
    "not_enough_money_text": "Not enough funds to withdraw! Minimum amount – ${mim_amount_request_pay}",
    "get_user_wallet": "Enter {currency} to withdraw funds",
    "accept_create_new_request_payment": '<b>Check your details</b>\n\n'
                                         'Withdrawal on: <b>{currency}</b>\n'
                                         'Payment Details: <b>{user_wallet}</b>\n'
                                         'Amount: <b>${amount}</b>\n\n'
                                         '<b>Withdraw funds?</b>',
    "yes": "🟢 Да",
    "request_creation_error": "Error creating request – contact Support!",
    "successful_application": '<b>🟢 Request successfully created!</b>\n\n'
                              'Request number: {order_id}\n\n'
                              '<b><i>Money will be credited within 48 hours</i></b>',
    "bank_card_number": "Bank card number",
    "wallet_number": "Wallet number",
    "paypal_number": "PayPal number",
    "Bank_card": "Bank card",
    "time_transaction_mail": '<b>👋 Hi, {user_name}\n'
                             'We see you created a payment for ${price} but didnt pay it.\n'
                             'Here are some reasons why you should join YouFast VPN™ right now:</b>\n\n'
                             '🚀 You have the highest speed\n'
                             '🛡 The most powerful protection\n'
                             '⚠️ No risk of blocking \n'
                             '✅ All sites are freely available\n\n'
                             '<b>⤵️ To try, click: /start</b>',

    "balanc_task": '<b>⚠️ {days} d. until one of your VPNs is renewed {config_country}\n\n'
                   '→ Make sure you have enough balance to renew 😎</b>',

    "trial_mail": '🎁 Hello! We have already sent you <b>free</b> trial access to our VPN service, '
                  'so that you can see for yourself how fast and anonymous it is, '
                  'but apparently you didnt notice it 😥\n\n'
                  'We are sure that you will like our service, therefore'
                  '<b>we re giving you 5 days of free subscription again! 🚀</b>\n\n'
                  'To activate, click on the button at the bottom of this message.\n\n'
                  '❗️<b>The offer is only available for the next 6 hours.</b>\n\n'
                  '---------------\n\n'
                  'Take it, see for yourself in our quality – and stay with us👇',

    "trial_send_text": 'Hello! We noticed that you were interested in our VPN, but never tried it 😥\n\n'
                       'We re sure you ll love it, so <b>we re giving you {days} days of subscription for free!</b> 🚀\n\n'
                       'To activate, click on the button at the bottom of this message.\n\n'
                       '❗️<b>The offer is only available for the next 6 hours.</b>\n\n'
                       '---------------\n\n'
                       'Take it, see for yourself in our quality – and stay with us👇',

    "search_country_text": '🔎 <b>Search by country name</b>\n\n'
                           '<i>Enter the name of the country you are interested in. '
                           'The poisĸ starts from two buĸv and can be performed in both Russian and English.</i>',
    "message_short_text": '<b>The message is too short!\n'
                          'The minimum length is 2 characters</b>',

    "message_long_text": 'The message is too long! The maximum length is 20 characters',
    "select_county": '<b>Select the country for your VPN connection</b>',
    "no_country": 'No countries found for your query, try again 🔎',
    "creating_file": "<b>Wait, I'm creating a file for you...🕘</b>",

    "pre_mailing_active_users": "Hello, dear friend!\n\n"
                                "First of all, we want to thank you for using our VPN.\n\n"
                                "Secondly, we inform you that we have a 'Partner Program' section with a QR code "
                                "that you can send to a friend and earn 10% from each payment. "
                                "The money can later be withdrawn or spent on paying for services in the bot.\n\n"
                                "⬇️️ Your referral link:\n"
                                "└ https://t.me/YouFast_vpn_bot?start={user_id}",

    "pre_mailing_stopped_paying": 'We re sorry you stopped using our VPN 😢\n\n'
                                  'Please let us know what you didnt like or why you are not renewing. Thanks!\n\n'
                                  '👉 {support_link}',
    "payments_for_mailing": "💰 Top up your balance",
    "bay_vpn_trial_version": "🎁 Get a VPN",
    "config_successfully_extended": "👉 <b>The config has been successfully extended!"
                                    " ({country})</b>\nThank you for being with us 😌",

    "no_money_for_extension": '🫤 <b>Config extension error!\n\n'
                              'You didnt have enough money on your balance, so your VPN was deactivated ({country}).\n'
                              'Dont forget to top up your balance in advance 🙂</b>',
    "config_deactivate": "👉 The config is deactivated because auto-renewal has been disabled ({country})",
    "crypto_change_button": "💲 Exchanger",
    "ip_check_main_page": '<b>🔓 YouFast™ IP Checker</b>\n\n'
                          '👉 <i>Get full information about your or any other IP address:</i>\n\n'
                          '├ 🌏 Geodata\n'
                          '├ 👔 Reg data\n'
                          '├ 👁️‍ IP type: Hosting/Mobile/Home\n'
                          '├ 🔓 Open ports (new)❗️\n'
                          '├ ⚠️ Risk Score\n'
                          '├ 🤬 Fraud Score\n'
                          '├ 🪬 Active:\n'
                          '        └ <b>VPN / Proxy / TOR</b>❗️\n'
                          '├ 🤖 Lets check if its a bot❗️\n'
                          '└ ⚫️ Presence in blacklists<b>(soon)</b>\n\n'
                          '💲 <b>1 check – $0.01\n'
                          '💲 From 100 (wholesale) – %10 discount</b>',
    "manual_check": '🤚 Manual check',
    "check_from_the_file": "📎 Check from the file",
    "check_main_ip": '🌐 Check your IP',
    "get_address_page": "<i>Enter one IP address in IPv4 or IPv6 format</i>",
    "checking_please_wait": "One moment..🕘",
    "invalid_ip_adress_text": '<b>Invalid format for IP address! Try again</b>',
    "check_one_adress_esult": "✅ Successfully!\n"
                              "👉 Order ID #<code>{order}</code>",
    "get_result_ip_check": "Result",
    "buy_more": "Buy more",
    "re_check": "Still 🔄",
    "request_file_for_ip_check": "<i>Send the file in the format .txt with IP addresses</i>\n\n"
                                 "<b>❗️Format: one line – one IP address</b>\n\n"
                                 "⛔️ <i>Punching wholesale works in test mode – "
                                 "therefore, no more than 100 addresses in the file\n\n"
                                 "⛔️ Fraud and Risk Score checks are also not available yet</i>",
    "eror_load_files": "⛔️ Error downloading your file, try again",
    "big_size_ip_file": 'Too many IP addresses!\nPunching wholesale works in test mode – '
                        'therefore, no more than 100 addresses in the file',
    "eror_multi_checking": "⛔️ An error has occurred, try again or contact support",

    "multi_check_adress_result": "✅ Successfully!\n👉 OrderID #{order}",
    "about_the_service": "❓ About the service",
    "little_balance_for_check": '<b>❌ The balance is not enough to check – we recommend to top up</b>',
    "few_addresses": '<b>❌ Few addresses in the file!</b>',
    "checker_history_plug": "📎 History",
    "soo-plug": "Soon! 🎉",
    "download_file": '📄 File',
    "checker_result_template": "IP: {ip}\n\n"
                               "GEODATA\n"
                               "Continent: {continent}\n"
                               "Continent code: {continentCode}\n"
                               "Country: {country}\n"
                               "Country code: {countryCode}\n"
                               "Region: {region}\n"
                               "Region name: {regionName}\n"
                               "City: {city}\n"
                               "Zip: {zip}\n"
                               "Longitude: {lat}\n"
                               "Width: {lon}\n"
                               "Time zone: {timezone}\n"
                               "Currency: {currency}\n"
                               "Phone code: {callingCode}\n\n"

                               "ORGANIZATION\n"
                               "Provider: {isp}\n"
                               "ASN: {as_}\n"
                               "ASN name: {asname}\n"
                               "Reverse: {reverse}\n\n"

                               "PORT SCANER\n"
                               "{port_scan}\n\n"

                               "FRAUD & RISK SCORE\n"
                               "{fraud_score_block}\n\n"

                               "CONNECTION ANALYSIS\n"
                               "Proxy Probability: {proxy_value}\n"
                               "VPN Probability: {vpn_value}\n"
                               "Mobile IP: {mobile_ip}\n"
                               "Resident: {resident_ip}\n"
                               "Hosting / data center: {hosting}\n"
                               "Active VPN: {active_vpn}\n"
                               "Active TOR: {active_tor}\n"
                               "Probability of a bot: {is_bot}\n\n"
                               "********************************************************************\n\n",
    "high_lvl": 'High ⛔️',
    "low_lvl": "Low ✅",
    "yes_lvl": "Yes",
    "no_lvl": "No",
    "open_ports_not_found": "✅ No open ports found",
    "checker_history_not_found": "⛔️ The history is not found – apparently, you haven't punched anything yet",
    "fraud_score_block_plug": 'Not available',
    "history_you_fast_file": "Last 10 checks",
    "refferral_user_alert": "🤝 <b>Affiliate Program</b>\n\n"
                            "A new user has signed up for your team — @{user_name}!\n\n"
                            "It is in your best interest to tell him about the advantages of our service\n"
                            "<i>(the fastest VPN, IP checker, proxy)</i> "
                            "and help to make a payment with which you will receive %.\n\n"
                            "<b>Write to</b> @{user_name} 👈",

    "refferral_user_anonum_alert": "🤝 <b>Affiliate Program</b>\n\n"
                                   "A new user has signed up for your team — ID <code>{user_id}</code>!\n\n"
                                   "🔓 Unfortunately, the user does not have a clickable username, "
                                   "to write to him and tell him about the advantages of our service.",
    "shared_vpn_country": "🌏 Selection by country",
    "choosing-vpn-type": "<b>Choose a VPN by purpose of use or country ⬇️</b>",
    "refrash_config": "♻️ Update",
    "dell_config": "🗑️ Remove",
    "proxy-sell-main-page": "🛡️ <b>YouFast™ Proxy</b>\n\n"
                            "👉 Get excellent proxies for your needs in a convenient bot:\n\n"
                            "🗄️ Hosting\n"
                            "🏠 Resident\n"
                            "📲 Mobile\n\n"
                            "<i>Private/Shared/Ipv4/Ipv6/Socks5/Https</i>",
    "button-sell-proxy": "🧦 Buy",
    "button-proxy-history": "📎 History",
    "button-abount-proxy-service": "❓ About the service",
    "button-some": "?",
    "proxy_sell_category_text": "<b>🌏 YouFast Proxy</b>\n\nWhat type of proxy are you interested in?",
    "proxy_type_datacentr": "🗄️ Data Center",
    "proxy_type_resident": "🖥️ Resident",
    "proxy_type_mobile": "📱 Mobile",
    "proxy_type_business": "💼 Business",
    "button-poxy-faq": "❓ What to choose?",

}
