locales = ['ru', 'en']
flags = {
    "ru": '🇷🇺 RU',
    "en": '🇺🇸 EN',
    "fr": "🇫🇷 FR"
}


def check_localization_availability(lang: str) -> str:
    if lang in locales:
        return lang
    else:
        return 'en'


def country_lang_conv(country: str, lang: str):
    """Функция конвертирует код стран в Полноое название страны с учетом языка (ru or en)"""
    country_dict = {
        "ru": {
            "pl": "🇵🇱 Польша",
            "fr": "🇫🇷 Франция",
            "gb": "🇬🇧 Британия",
            "jp": "🇯🇵 Япония",
            "ca": "🇨🇦 Канада",
            "se": "🇸🇪 Швеция",
            "es": "🇪🇸 Испания",
            "ge": "🇩🇪 Германия",
            "nl": "🇳🇱 Нидерланды",
            "ru": "🇷🇺 Россия",
            "us": "🇺🇸 США",
            "torrent": "🏴‍☠️ Для торрентов",
            "social": "🤳 Для соцсетей",
        },

        "en": {
            "pl": "🇵🇱 Poland",
            "fr": "🇫🇷 France",
            "gb": "🇬🇧 Britain",
            "jp": "🇯🇵 Japan",
            "ca": "🇨🇦 Canada",
            "se": "🇸🇪 Sweden",
            "es": "🇪🇸 Spain",
            "ge": "🇩🇪 Germany",
            "nl": "🇳🇱 Netherlands",
            "ru": "🇷🇺 Russia",
            "us": "🇺🇸 USA",
            "torrent": "🏴‍☠️ For torrents",
            "social": "🤳 Social networks",
        }
    }

    try:
        return country_dict[lang][country.lower()]
    except KeyError:
        try:
            return country_dict['en'][country.lower()]
        except KeyError:
            return country
