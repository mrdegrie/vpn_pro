from pydantic import BaseModel
from typing import Optional, List


class UserLang(BaseModel):
    user_id: int
    lang: str


class Langs(BaseModel):
    users: List[UserLang]

