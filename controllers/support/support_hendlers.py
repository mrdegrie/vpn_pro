from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
# from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
# from aiogram import exceptions
#
# from models.base_models.base_control import BaseControl
from models.support.support_control import UserSupport


# Старт страницы поддержки
async def start_support_page(call: types.CallbackQuery):
    support = UserSupport()
    keyboard = await support.get_faq_page(user_id=call.from_user.id, lang=call.from_user.language_code)
    await call.message.edit_caption(caption=support.MSG, reply_markup=keyboard)


# Информация о VPN
async def vpn_support_info(call: types.CallbackQuery, state: FSMContext):
    support = UserSupport()
    msg = await support.vpn_info_page(call_back=True, user_id=call.from_user.id, lang=call.from_user.language_code)
    if msg:
        await call.message.edit_caption(caption=msg, reply_markup=support.keyboard)
    else:
        await call.bot.answer_callback_query(call.id, show_alert=True, text='⛔️ Unknown error')


# Информация о VPN - текстовый хендлер
async def vpn_support_info_text_version(message: types.Message, state: FSMContext):
    await state.finish()
    support = UserSupport()
    msg = await support.vpn_info_page(call_back=False,
                                      user_id=message.from_user.id,
                                      lang=message.from_user.language_code)
    if msg:
        await message.answer(msg, disable_web_page_preview=True)
    pass


# VPN USER MANUAL - SELECT TYPE USER MANUAL - callback version
async def vpn_user_manual(call: types.CallbackQuery, state: FSMContext):
    await state.finish()
    support = UserSupport()
    msg = await support.seelect_usert_device(user_id=call.from_user.id, lang=call.from_user.language_code)
    await call.message.edit_caption(caption=msg, reply_markup=support.keyboard)


# VPN USER MANUAL - SELECT TYPE USER MANUAL - text version
async def vpn_user_manual_text_version(mesage: types.Message, state: FSMContext):
    await state.finish()
    support = UserSupport()
    msg = await support.seelect_usert_device(text_mode=True,
                                             user_id=mesage.from_user.id,
                                             lang=mesage.from_user.language_code,
                                             )
    await mesage.answer_photo(caption=msg,
                              photo=support.start_photo,
                              reply_markup=support.keyboard
                              )


# SELECT USER MANUAL DEVICE TYPE
async def vpn_user_manual_select_type_manual(call: types.CallbackQuery):
    support = UserSupport()
    msg = await support.vpn_user_manual_page(manual_type=call.data.split('#')[1],
                                             call_back=True,
                                             user_id=call.from_user.id,
                                             lang=call.from_user.language_code)
    if msg:
        await call.message.edit_caption(caption=msg, reply_markup=support.keyboard)
    else:
        await call.bot.answer_callback_query(call.id, show_alert=True, text='⛔️ Unknown error')


def register_user_support_hendlers(dp: Dispatcher):
    """
    Хендлеры пользовательской поддержки
    :param dp:
    :return:
    """
    # Старт SUPPORT MENU
    dp.register_callback_query_handler(
        start_support_page,
        lambda callback_query: callback_query.data == 'help_meesage',
        state='*',
    )
    # VPN INFO - callback verion
    dp.register_callback_query_handler(
        vpn_support_info,
        lambda callback_query: callback_query.data == 'vpn_info',
        state='*',
    )
    # VPN INFO - text version
    dp.register_message_handler(
        vpn_support_info_text_version,
        lambda mesage: mesage.text == '🤔 O VPN',
        state='*',
    )
    # VPN user manual - select user device - callback_version
    dp.register_callback_query_handler(
        vpn_user_manual,
        lambda callback_query: callback_query.data == 'vpn_manual',
        state='*',
    )
    # VPN user manual - select user device - text version
    dp.register_message_handler(
        vpn_user_manual_text_version,
        lambda mesage: mesage.text == '🗳 ИНСТРУКЦИИ',
        state='*',
    )
    # VPN USER MANUAL -  SELECT MANUAL TYPE (USER DEVICE)
    dp.register_callback_query_handler(
        vpn_user_manual_select_type_manual,
        lambda callback_query: callback_query.data.split("#")[0] == 'user_manual',
        state='*',
    )


