from aiogram import Dispatcher
from aiogram import types
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.dispatcher import FSMContext

from models.payments.payok_control import PayokControl
from controllers.payment.payment_states import PayokStates
from controllers.payment.free_kassa import FreeKassaControl


async def select_amount_add_balance(call: types.CallbackQuery, state: FSMContext):
    """Запрашиваем сумму пополнения баланса"""

    payment = FreeKassaControl()
    keyboard = await payment.select_amount_page(user_id=call.from_user.id, lang=call.from_user.id)
    msg = await call.message.edit_caption(caption=payment.MSG, reply_markup=keyboard)
    await state.update_data(msg=msg['message_id'])
    await PayokStates.SELECT_AMOUNT_NEW_PAY.set()


async def create_new_payok_pay_inline(call: types.CallbackQuery, state: FSMContext):
    """Создать платежку Payok inline version"""

    payok_control = PayokControl()
    try:
        amout = int(call.data.split('#')[1])
        if amout:
            keyboard_success = await payok_control.create_new_payment(user_id=call.from_user.id,
                                                                      amount=amout,
                                                                      user_name=call.from_user.username,
                                                                      lang=call.from_user.language_code,
                                                                      )

            await call.message.edit_caption(caption=payok_control.MSG,
                                            reply_markup=keyboard_success,
                                            )
            await state.finish()
        else:
            keyboard = await payok_control.get_error_page(user_id=call.from_user.id,
                                                          ui_id='amount_type_error')
            await call.message.edit_caption(caption=payok_control.MSG, reply_markup=keyboard)
    except ValueError:
        keyboard = await payok_control.get_error_page(user_id=call.from_user.id,
                                                      ui_id='payment_amount_error')
        await call.message.edit_caption(caption=payok_control.MSG, reply_markup=keyboard)


async def create_new_payok_pay_text(msg: types.Message, state: FSMContext):
    """Создать платежку text version"""
    await msg.delete()
    message_id = (await state.get_data()).get('msg')
    await msg.bot.edit_message_caption(chat_id=msg.from_user.id,
                                       message_id=message_id,
                                       caption='<b>Creating a payment...🕘</b>')

    payok_control = PayokControl()
    try:
        amout = int(msg.text)
        if amout >= 2:
            payok_control = PayokControl()
            keyboard_success = await payok_control.create_new_payment(user_id=msg.from_user.id,
                                                                      amount=amout,
                                                                      user_name=msg.from_user.username,
                                                                      lang=msg.from_user.language_code,
                                                                      )

            await msg.bot.edit_message_caption(chat_id=msg.from_user.id,
                                               message_id=message_id,
                                               caption=payok_control.MSG,
                                               reply_markup=keyboard_success,
                                               )
            await state.finish()
        else:
            keyboard = await payok_control.get_error_page(user_id=msg.from_user.id,
                                                          ui_id='amount_type_error')
            await msg.bot.edit_message_caption(chat_id=msg.from_user.id,
                                               message_id=message_id,
                                               caption=payok_control.MSG,
                                               reply_markup=keyboard,
                                               )
    except ValueError:
        keyboard = await payok_control.get_error_page(user_id=msg.from_user.id,
                                                      ui_id='payment_amount_error')

        await msg.bot.edit_message_caption(chat_id=msg.from_user.id,
                                           message_id=message_id,
                                           caption=payok_control.MSG,
                                           reply_markup=keyboard,
                                           )


def register_payok_hendlers(dp: Dispatcher):
    dp.register_callback_query_handler(
        select_amount_add_balance,
        lambda callback_query: callback_query.data == 'custom_amounnt_pay',
        state=PayokStates.SELECT_AMOUNT_NEW_PAY,
    )

    dp.register_callback_query_handler(
        create_new_payok_pay_inline,
        lambda callback_query: callback_query.data.split("#")[0] == 'amounnt_pay',
        state=PayokStates.SELECT_AMOUNT_NEW_PAY,
    )

    dp.register_message_handler(
        create_new_payok_pay_text,
        content_types=['text'],
        state=PayokStates.SELECT_AMOUNT_NEW_PAY,
    )
