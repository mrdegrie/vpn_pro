from aiogram import Dispatcher
from aiogram import types
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.dispatcher import FSMContext

from models.payments.free_kassa import FreeKassaControl
from controllers.payment.payment_states import FreeKassaStates


# Запрашиваем сумму пополнения баланса
async def select_amount_add_balance(call: types.CallbackQuery, state: FSMContext):
    payment = FreeKassaControl()
    keyboard = await payment.select_amount_page(user_id=call.from_user.id, lang=call.from_user.language_code)
    msg = await call.message.edit_caption(caption=payment.MSG, reply_markup=keyboard)
    await state.update_data(msg=msg['message_id'])
    await FreeKassaStates.SELECT_AMOUNT_NEW_PAY.set()


async def create_new_fk_pay(msg: types.Message, state: FSMContext):
    """Создать платежку text version"""

    await msg.delete()
    message_id = (await state.get_data()).get('msg')
    payment = FreeKassaControl()
    if msg.text.isdigit():
        if int(msg.text) >= 2:
            keyboard_success = await payment.create_new_payment(user_id=msg.from_user.id,
                                                                amount=int(msg.text),
                                                                user_name=msg.from_user.username,
                                                                lang=msg.from_user.language_code,
                                                                )

            await msg.bot.edit_message_caption(chat_id=msg.from_user.id,
                                               message_id=message_id,
                                               caption=payment.MSG,
                                               reply_markup=keyboard_success,
                                               )
            await state.finish()
        else:
            keyboard = await payment.get_error_amount_message(user_id=msg.from_user.id, lang=msg.from_user.language_code)
            await msg.bot.edit_message_caption(chat_id=msg.from_user.id,
                                               message_id=message_id,
                                               caption=payment.MSG,
                                               reply_markup=keyboard,
                                               )
    else:
        keyboard = await payment.get_error_amount_message(user_id=msg.from_user.id, lang=msg.from_user.language_code)
        await msg.bot.edit_message_caption(chat_id=msg.from_user.id,
                                           message_id=message_id,
                                           caption=payment.MSG,
                                           reply_markup=keyboard,
                                           )


async def create_new_fk_pay_inline(call: types.CallbackQuery, state: FSMContext):
    """Создать платежку inline version"""
    amout = int(call.data.split('#')[1])
    payment = FreeKassaControl()
    if amout >= 2:
        keyboard_success = await payment.create_new_payment(user_id=call.from_user.id,
                                                            amount=amout,
                                                            user_name=call.from_user.username,
                                                            lang=call.from_user.language_code
                                                            )

        await call.message.edit_caption(caption=payment.MSG,
                                        reply_markup=keyboard_success,
                                        )
        await state.finish()
    else:
        keyboard = await payment.get_error_amount_message(user_id=call.from_user.id, lang=call.from_user.language_code)
        await call.message.edit_caption(caption=payment.MSG,
                                        reply_markup=keyboard,
                                        )


def register_free_kassa_hendlers(dp: Dispatcher):
    dp.register_callback_query_handler(
        select_amount_add_balance,
        lambda callback_query: callback_query.data == 'custom_amounnt_pay',
        state=FreeKassaStates.SELECT_AMOUNT_NEW_PAY,
    )
    # Покупаем подписку
    dp.register_message_handler(
        create_new_fk_pay,
        content_types=['text'],
        state=FreeKassaStates.SELECT_AMOUNT_NEW_PAY
    )
    # Принять кнопку с гтовой суммой оплаты и сформировть платежку фк
    dp.register_callback_query_handler(
        create_new_fk_pay_inline,
        lambda callback_query: callback_query.data.split("#")[0] == 'amounnt_pay',
        state=FreeKassaStates.SELECT_AMOUNT_NEW_PAY
    )
