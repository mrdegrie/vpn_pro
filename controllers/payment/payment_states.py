from aiogram.dispatcher.filters.state import State, StatesGroup


class FreeKassaStates(StatesGroup):
    SELECT_AMOUNT_NEW_PAY = State()


class PaymentStates(StatesGroup):
    PAYMENT_HUB = State()
    SELECT_AMOUNT_NEW_PAY = State()


class PayokStates(StatesGroup):
    """Состояние платежной системы Payok.io"""
    SELECT_AMOUNT_NEW_PAY = State()


class LavaStates(StatesGroup):
    """Состояние платежной системы Lava"""
    SELECT_AMOUNT_NEW_PAY = State()
