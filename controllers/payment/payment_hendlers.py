from aiogram import Dispatcher
from aiogram import types
from aiogram.dispatcher import FSMContext

from models.payments.payment_control import PaymentControl
from controllers.privat_cabinet.cabinet_hendlers import start_private_cabinet
from controllers.payment.payment_states import PaymentStates, PayokStates, FreeKassaStates, LavaStates


async def get_all_active_paymnt_menthods(call: types.CallbackQuery, state: FSMContext):
    """Даем на выбор активные платежные системы"""

    payment = PaymentControl()
    await payment.get_all_active_wallet(user_id=call.from_user.id)
    if payment.error_msg:
        await call.bot.answer_callback_query(call.id, show_alert=True, text=payment.error_msg)
        await start_private_cabinet(call, state)
    elif payment.MSG:
        await call.message.edit_caption(caption=payment.MSG, reply_markup=payment.keyboard)
        await PaymentStates.PAYMENT_HUB.set()


async def payment_for_mailing(call: types.CallbackQuery):
    """Выбор плаатежных система из рассылки"""

    payment = PaymentControl()
    await payment.get_all_active_wallet(user_id=call.from_user.id, get_settings=True)
    if payment.error_msg:
        await call.bot.answer_callback_query(call.id, show_alert=True, text=payment.error_msg)
    elif payment.MSG:
        await call.bot.send_photo(chat_id=call.from_user.id,
                                  photo=payment.img_id,
                                  caption=payment.MSG,
                                  reply_markup=payment.keyboard,
                                  )
        # await call.message.edit_caption(caption=payment.MSG, reply_markup=payment.keyboard)
        await PaymentStates.PAYMENT_HUB.set()


async def payment_hub(call: types.CallbackQuery, state: FSMContext):
    """Запрасить сумму пополнения и включить стейт в зависимости от метода оплаты"""

    payment_control = PaymentControl()
    keyboard = await payment_control.select_amount_page(user_id=call.from_user.id, lang=call.from_user.language_code)

    await state.update_data(payment_method=call.data.split('#')[1])
    await call.message.edit_caption(caption=payment_control.MSG, reply_markup=keyboard)

    if call.data.split('#')[1] == 'FK':
        await FreeKassaStates.SELECT_AMOUNT_NEW_PAY.set()

    if call.data.split('#')[1] == 'PAYOK':
        await PayokStates.SELECT_AMOUNT_NEW_PAY.set()

    elif call.data.split('#')[1] == 'LAVA':
        await LavaStates.SELECT_AMOUNT_NEW_PAY.set()


async def pending_pament(call: types.CallbackQuery):
    """Проверить статус платежа"""

    order_id = int(call.data.split('#')[1])
    await call.message.edit_caption(caption='Checking the status...')
    payment_control = PaymentControl()
    await payment_control.get_tyemp_transaction_info(order_id=order_id,
                                                     user_id=call.from_user.id,
                                                     lang=call.from_user.language_code,
                                                     )
    await call.message.edit_caption(caption=payment_control.MSG, reply_markup=payment_control.keyboard)


def register_payment_hendlers(dp: Dispatcher):
    dp.register_callback_query_handler(
        get_all_active_paymnt_menthods,
        lambda callback_query: callback_query.data == 'all_payments',
        state='*',
    )
    # payment_for_mailing
    dp.register_callback_query_handler(
        payment_for_mailing,
        lambda callback_query: callback_query.data == 'payments_for_mailing',
        state='*',
    )
    dp.register_callback_query_handler(
        payment_hub,
        lambda callback_query: callback_query.data.split('#')[0] == 'wallet',
        state=PaymentStates.PAYMENT_HUB,
    )
    # Проверить статус платежа
    dp.register_callback_query_handler(
        pending_pament,
        lambda callback_query: callback_query.data.split('#')[0] == 'pending_pay',
        state='*',
    )
