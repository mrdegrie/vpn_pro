from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram import exceptions

from models.privat_cabinet.private_cabinet_control import PrivateCabinet


# Старт личного кабинета - callback version
async def start_private_cabinet(call: types.CallbackQuery, state: FSMContext):
    await state.finish()
    cabinet_control = PrivateCabinet()
    keyboard = await cabinet_control.get_private_cabinet(user_id=call.from_user.id)
    await call.message.edit_caption(caption=cabinet_control.MSG, reply_markup=keyboard)


# Старт личного кабинета - text version
async def start_private_cabinet_text_version(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer('<b>Getting information.. 🕘</b>')
    cabinet_control = PrivateCabinet()
    keyboard = await cabinet_control.get_private_cabinet(user_id=message.from_user.id, text_mode=True)

    await message.answer_photo(photo=cabinet_control.start_photo,
                               caption=cabinet_control.MSG,
                               reply_markup=keyboard)


# # подписки - показывам список сервисов для подписок
# async def all_service_subs(call: types.CallbackQuery, state: FSMContext):
#     await state.finish()
#     cabinet_control = PrivateCabinet()
#     keyboard = await cabinet_control.get_all_sub_services(lang=call.from_user.language_code, user_id=call.from_user.id)
#     await call.message.edit_caption(caption=cabinet_control.MSG, reply_markup=keyboard)


def register_private_cabinet_hendlers(dp: Dispatcher):
    # Start pprivate cabinet - callback versio
    
    dp.register_callback_query_handler(
        start_private_cabinet,
        lambda callback_query: callback_query.data == 'private_cabinet',
        state='*',
    )
    # start private cabinet - text_version
    dp.register_message_handler(
        start_private_cabinet_text_version,
        lambda message: message.text == '⚙️ Аккаунт',
        state='*',
    )
    dp.register_message_handler(
        start_private_cabinet_text_version,
        lambda message: message.text == '⚙️ Account',
        state='*',
    )
    # # Список сервисов для подписки
    # dp.register_callback_query_handler(
    #     all_service_subs,
    #     lambda callback_query: callback_query.data == 'all_subs',
    #     state='*',
    # )
