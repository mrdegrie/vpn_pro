from aiogram.dispatcher.filters.state import State, StatesGroup


class RequestPaymentStates(StatesGroup):
    CREATE_NEW_REQUEST = State()
    GET_WALLET = State()
    ACCEPT_CREATE_STATE = State()
