from aiogram import types, Dispatcher, exceptions
from aiogram.dispatcher import FSMContext
import logging
import os
# from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
# from aiogram import exceptions

# from pagination.MagicPaginator import ButtonPagination

from models.partner_system.partner_control import ParnerSystem
from controllers.partner_system.patner_states import RequestPaymentStates
from configs.config import admins
from configs.config import mim_amount_request_pay


# from models.shared_vpn.shared_vpn_control import SharedVPNcontrol
# from controllers.shared_vpn.shared_vpn_states import VPNServersState

async def start_private_cabinet_partner(call: types.CallbackQuery, state: FSMContext):
    """Вход в кабинет партнера"""

    await state.finish()
    partner_control = ParnerSystem()
    auth = await partner_control.start_partner_cabinet(user_id=int(call.from_user.id),
                                                       lang=call.from_user.language_code,
                                                       )
    if auth:
        if partner_control.qr_path:
            await call.message.delete()
            with open(partner_control.qr_path, 'rb') as image:
                await call.message.bot.send_photo(chat_id=call.from_user.id,
                                                  photo=image,
                                                  caption=partner_control.MSG,
                                                  reply_markup=partner_control.keyboard
                                                  )
            try:
                os.remove(path=partner_control.qr_path)
            except:
                pass
                #  Знаю что анти паттерн, но хуле :)
        else:
            await call.message.edit_caption(caption=partner_control.MSG, reply_markup=partner_control.keyboard)
    else:
        await call.bot.answer_callback_query(call.id, show_alert=True, text='Sorry, ⛔')
        logging.warning(f'bad auth to partner cab! User id: {call.from_user.id}')
        return


async def exit_pp(call: types.CallbackQuery, state: FSMContext):
    """Выход из кабинета партнерки"""

    await state.finish()
    await call.message.delete()


async def payment_request(call: types.CallbackQuery, state: FSMContext):
    """Страница запроса вывода заработка партнера"""

    partner_control = ParnerSystem(lang=call.from_user.language_code)
    keyboard = await partner_control.cabinet_request_money(user_id=call.from_user.id)
    if not partner_control.alert_msg:
        await call.message.edit_caption(caption=partner_control.MSG, reply_markup=keyboard)
        await state.update_data(request_amount=partner_control.user_amount)
        await RequestPaymentStates.CREATE_NEW_REQUEST.set()
    else:
        await call.bot.answer_callback_query(call.id, show_alert=True, text=partner_control.alert_msg)
        await call.message.edit_caption(caption=partner_control.MSG, reply_markup=keyboard)


async def alert_no_money_request_money(call: types.CallbackQuery):
    """Сообщние о недостаточности средств для зпроса на вывод"""

    await call.bot.answer_callback_query(call.id,
                                         show_alert=True,
                                         text=f'Недостаточно средств для вывода!\n'
                                              f'Минимальная сумма для вывода: ${mim_amount_request_pay}')


async def select_amount_request_payment(call: types.CallbackQuery, state: FSMContext):
    """Получить валюту вывода, запросить кошелек для вывода"""

    currency = call.data.split('#')[1]
    await state.update_data(currency=currency)
    partner_control = ParnerSystem(lang=call.from_user.language_code, user_id=call.from_user.id)
    keyboard = await partner_control.get_user_wallet(currency=currency)
    msg = await call.message.edit_caption(caption=partner_control.MSG, reply_markup=keyboard)
    await state.update_data(msg=msg['message_id'])
    await RequestPaymentStates.GET_WALLET.set()


async def accept_new_request_payment(message: types.Message, state: FSMContext):
    """Запросить подтверждение на создание нового запрос на вывод"""

    await message.delete()

    user_wallet = message.text
    msg = (await state.get_data()).get('msg')
    request_amount = (await state.get_data()).get('request_amount')
    currency = (await state.get_data()).get('currency')
    partner_control = ParnerSystem(lang=message.from_user.language_code, user_id=message.from_user.id)
    await state.update_data(user_wallet=user_wallet)

    keyboard = await partner_control.accept_create_new_equest_pament(currency=currency,
                                                                     user_wallet=user_wallet,
                                                                     amount=request_amount)
    await message.bot.edit_message_caption(
        chat_id=message.from_user.id,
        message_id=msg,
        caption=partner_control.MSG,
        reply_markup=keyboard
    )
    await RequestPaymentStates.ACCEPT_CREATE_STATE.set()


async def create_request(call: types.CallbackQuery, state: FSMContext):
    """Создть новый запрос на вывод средств"""

    await call.message.edit_caption(caption='Ожидайте..🕘')
    request_amount = (await state.get_data()).get('request_amount')
    currency = (await state.get_data()).get('currency')
    user_wallet = (await state.get_data()).get('user_wallet')

    partner_control = ParnerSystem(user_id=call.from_user.id, lang=call.from_user.language_code)
    result = await partner_control.create_order_request_paymwnt(user_id=call.from_user.id,
                                                                amount=request_amount,
                                                                currency=currency,
                                                                user_wallet=user_wallet,
                                                                user_name=call.from_user.username)

    await call.message.edit_caption(caption=partner_control.MSG, reply_markup=partner_control.keyboard)
    await state.finish()

    if result:
        for admin in admins:
            try:
                await call.bot.send_message(chat_id=admin, text=partner_control.alert_msg)
            except exceptions.BadRequest:
                pass


def register_partnr_hendlers(dp: Dispatcher):
    """Регистрация хендлеров"""

    # private_cabinet
    dp.register_callback_query_handler(
        start_private_cabinet_partner,
        lambda callback_query: callback_query.data == 'partner_cabinet',
        state='*'
    )
    # Запрос на вывод средств партнера в ЛК
    dp.register_callback_query_handler(
        payment_request,
        lambda callback_query: callback_query.data == 'partner_payment_request',
        state='*'
    )
    # aler_no_money_request_money
    dp.register_callback_query_handler(
        alert_no_money_request_money,
        lambda callback_query: callback_query.data == 'no_money_equest_money',
        state='*'
    )
    # Запросить кошелек для вывода
    dp.register_callback_query_handler(
        select_amount_request_payment,
        lambda callback_query: callback_query.data.split("#")[0] == 'request_money',
        state=RequestPaymentStates.CREATE_NEW_REQUEST,
    )
    # Запросить подтверждение на создание нового запрос на вывод
    dp.register_message_handler(
        accept_new_request_payment,
        content_types=['text'],
        state=RequestPaymentStates.GET_WALLET,
    )
    # accept_create_request_wallet
    dp.register_callback_query_handler(
        create_request,
        lambda callback_query: callback_query.data == 'accept_create_request_wallet',
        state=RequestPaymentStates.ACCEPT_CREATE_STATE,
    )
