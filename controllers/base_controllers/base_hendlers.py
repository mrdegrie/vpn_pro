from aiogram import types, Dispatcher, exceptions
from aiogram.dispatcher import FSMContext

from models.base_models.base_control import BaseControl
from models.ui.ui_storage import UIControl


# from configs.config import channel_id, channel_url


async def start_bot(message: types.Message, state: FSMContext):
    """Старт бота"""
    await state.finish()
    await message.answer('<b>Authorization..🕘</b>')
    menu = BaseControl()
    keyboard = await menu.start_bot(
        user_id=message.from_user.id,
        user_name=message.from_user.username,
        message_text=message.text,
        language=message.from_user.language_code,
    )
    # Проверяем подписан ли юзер на канал
    if await menu.sub_check(message, user_id=message.from_user.id):
        await message.answer('<b>✅</b>', reply_markup=menu.text_keyboard)
        await message.answer_photo(photo=menu.start_photo, caption=menu.MSG, reply_markup=keyboard)
    else:
        await message.answer(menu.MSG, reply_markup=menu.keyboard)
    if menu.referral and menu.new_user.status:
        try:
            ui = UIControl(user_id=menu.referral)
            await ui.get_language()
            if message.from_user.username:
                alert_text = ui.get_text("refferral_user_alert").format(user_name=message.from_user.username)
            else:
                alert_text = ui.get_text("refferral_user_anonum_alert").format(user_id=message.from_user.id)

            await message.bot.send_message(chat_id=menu.referral, text=alert_text)
        except exceptions.BadRequest:
            pass


async def start_message_text_mode(message: types.Message, state: FSMContext):
    """Стрт бота текстовая версия"""

    await state.finish()
    menu = BaseControl()
    keyboard = await menu.start_bot(start=False,
                                    language=message.from_user.language_code,
                                    user_id=message.from_user.id)
    await message.delete()
    await message.bot.send_photo(chat_id=message.from_user.id,
                                 photo=menu.start_photo,
                                 caption=menu.MSG,
                                 reply_markup=keyboard,
                                 )


async def start_bot_inline_veersion(call: types.CallbackQuery, state: FSMContext):
    """"""

    await state.finish()
    menu = BaseControl()

    keyboard = await menu.start_bot(
        user_id=call.message.from_user.id,
        user_name=call.message.from_user.username,
        message_text='',
        language=call.from_user.language_code,
        start=False,
    )
    # провееряем подписан ли юзер на канал
    if await menu.sub_check(call.message, user_id=call.from_user.id):
        await call.message.answer('<b>✅</b>', reply_markup=menu.text_keyboard)
        await call.message.answer_photo(photo=menu.start_photo, caption=menu.MSG, reply_markup=keyboard)
    else:
        await call.bot.answer_callback_query(call.id, show_alert=True, text=menu.error_message)
        # await call.message.answer(menu.MSG, reply_markup=menu.keyboard)


async def next_start_message(call: types.CallbackQuery, state: FSMContext):
    """Старт бота при Inline выходе в главное меню"""

    await state.finish()
    menu = BaseControl()
    keyboard = await menu.start_bot(language=call.from_user.language_code, user_id=call.from_user.id, start=True)
    await call.message.edit_caption(caption=menu.MSG, reply_markup=keyboard)


async def exit_partner_cabonet(call: types.CallbackQuery, state: FSMContext):
    """Выход из кабинета партнерк в главное меню"""

    await state.finish()
    await call.message.delete()
    menu = BaseControl()
    keyboard = await menu.start_bot(language=call.from_user.language_code, user_id=call.from_user.id, start=False)
    await call.message.bot.send_photo(chat_id=call.from_user.id,
                                      photo=menu.start_photo,
                                      caption=menu.MSG,
                                      reply_markup=keyboard)


async def change_lang_page(call: types.CallbackQuery, state: FSMContext):
    """Запросить язык для смены локкализации"""

    await state.finish()
    menu = BaseControl()
    keyboard = await menu.change_lang_page()
    await call.message.edit_caption(caption=menu.MSG, reply_markup=keyboard)


async def change_lang(call: types.CallbackQuery, state: FSMContext):
    menu = BaseControl()
    if await menu.change_bot_lang(new_lang=call.data.split('#')[1], user_id=call.from_user.id):
        keyboard = await menu.start_bot(language=call.from_user.language_code, user_id=call.from_user.id)
        await call.message.delete()
        await call.message.answer('✅', reply_markup=menu.text_keyboard)
        await call.message.answer_photo(photo=menu.start_photo, caption=menu.MSG, reply_markup=keyboard)
    else:
        await call.bot.answer_callback_query(call.id, show_alert=True, text=':-( Error')


def register_base_hendlers(dp: Dispatcher):
    dp.register_message_handler(
        start_bot,
        commands=['start'],
        state='*',
    )

    dp.register_callback_query_handler(
        start_bot_inline_veersion,
        lambda callback_query: callback_query.data == 'check_subs',
        state='*',
    )
    dp.register_message_handler(
        start_message_text_mode,
        lambda message: message.text == 'ℹ️ Главное меню',
        state='*',
    )
    # ℹ️ Main menu
    dp.register_message_handler(
        start_message_text_mode,
        lambda message: message.text == 'ℹ️ Main menu',
        state='*',
    )

    # Выход в главное меню
    dp.register_callback_query_handler(
        next_start_message,
        lambda callback_query: callback_query.data == 'next_start_message',
        state='*',
    )
    # Выход в главное меню из партнерки
    dp.register_callback_query_handler(
        exit_partner_cabonet,
        lambda callback_query: callback_query.data == 'exit_partner_cabonet',
        state='*',
    )
    # Выход в главное меню из партнерки
    dp.register_callback_query_handler(
        change_lang_page,
        lambda callback_query: callback_query.data == 'lang_change',
        state='*',
    )
    # Сменить локаль
    dp.register_callback_query_handler(
        change_lang,
        lambda callback_query: callback_query.data.split("#")[0] == 'change_lang',
        state='*',
    )
