from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
# from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram import exceptions

# from pagination.MagicPaginator import ButtonPagination
# from models.shared_vpn.shared_vpn_control import SharedVPNcontrol
# from controllers.shared_vpn.shared_vpn_states import VPNServersState
from models.ui.ui_storage import UIControl
from models.ip_check.ip_check_control import IPCheckControl
from controllers.ip_score_check.checker_states import ManualCheckState


async def start_main_ip_score_page(call: types.CallbackQuery):
    """Получить главную страницу чекера IP"""
    ip_control = IPCheckControl(call.from_user.id)
    keyboard = await ip_control.main_page()
    await call.message.edit_caption(caption=ip_control.mesage_text, reply_markup=keyboard)


async def get_ip_for_check(call: types.CallbackQuery, state: FSMContext):
    """Запросить IP у пльзователя для ручного чего адреса"""
    ip_control = IPCheckControl(call.from_user.id)
    keyboad = await ip_control.ip_address_equest_page()
    msg = await call.message.edit_caption(caption=ip_control.mesage_text, reply_markup=keyboad)
    await state.update_data(msg_id=msg['message_id'])
    await ManualCheckState.GET_IP.set()


async def start_manual_check_ip(message: types.Message, state: FSMContext):
    """Произвести чек IP адреса"""
    await message.delete()
    msg_id = (await state.get_data()).get('msg_id')
    ui = UIControl(message.from_user.id)
    await ui.get_language()
    await message.bot.edit_message_caption(
        chat_id=message.from_user.id,
        message_id=msg_id,
        caption=ui.get_text('checking_please_wait'),
    )

    user_adress = message.text
    ip_control = IPCheckControl(message.from_user.id)
    keyboard = await ip_control.stat_check_adress(user_adress)
    await message.bot.edit_message_caption(
        chat_id=message.from_user.id,
        message_id=msg_id,
        caption=ip_control.mesage_text,
        reply_markup=keyboard,
    )


# Открыть страницу просмотра истории пробива
async def checker_history(call: types.CallbackQuery):
    ui = UIControl(call.from_user.id)
    await ui.get_language()
    ip_control = IPCheckControl(call.from_user.id)
    file_name = await ip_control.get_checker_history_to_file(call.from_user.id)
    if not file_name:
        await call.bot.answer_callback_query(call.id, show_alert=True, text=ip_control.mesage_text)

    with open(f'storage/checker/{file_name}', 'r') as history_file:
        try:
            await call.bot.send_document(chat_id=call.from_user.id,
                                         document=history_file,
                                         caption=ip_control.mesage_text,
                                         )
        except exceptions.BadRequest:
            await call.bot.answer_callback_query(call.id, show_alert=True, text=ip_control.mesage_text)


def register_main_ip_score_controllers(dp: Dispatcher):
    dp.register_callback_query_handler(
        start_main_ip_score_page,
        lambda call: call.data == 'main_ip_score_page',
        state='*',
    )
    # просить IP у пльзователя для ручного чего адреса
    dp.register_callback_query_handler(
        get_ip_for_check,
        lambda call: call.data == 'manual_check',
        state='*',
    )
    # Принять IP адрес, свлидировать, произвести чек
    dp.register_message_handler(
        start_manual_check_ip,
        content_types=['text'],
        state=ManualCheckState.GET_IP,
    )
    # checker_history_plug
    dp.register_callback_query_handler(
        checker_history,
        lambda call: call.data == 'checker_history_plug',
        state='*',
    )
