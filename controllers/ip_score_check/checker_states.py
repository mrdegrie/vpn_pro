from aiogram.dispatcher.filters.state import State, StatesGroup


class ManualCheckState(StatesGroup):
    GET_IP = State()


class MassIPCheckState(StatesGroup):
    GET_FILE = State()
