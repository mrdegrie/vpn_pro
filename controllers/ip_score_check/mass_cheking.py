from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext

from models.ui.ui_storage import UIControl
from models.ip_check.ip_check_control import IPCheckControl
from controllers.ip_score_check.checker_states import MassIPCheckState


#  Массовый чек - запрашиваем файл у пользователя
async def mass_check_file_request(call: types.CallbackQuery, state: FSMContext):
    ip_control = IPCheckControl(call.from_user.id)
    keyboad = await ip_control.request_file()
    msg = await call.message.edit_caption(caption=ip_control.mesage_text, reply_markup=keyboad)
    await state.update_data(msg_id=msg['message_id'])
    await MassIPCheckState.GET_FILE.set()


# Принять файл, распарсить, отправить на чек
async def load_file_and_mass_check(message: types.Message, state: FSMContext):
    await message.delete()
    message_id = (await state.get_data()).get('msg_id')
    ui = UIControl(message.from_user.id)
    await ui.get_language()

    await message.bot.edit_message_caption(
        chat_id=message.from_user.id,
        message_id=message_id,
        caption=ui.get_text('checking_please_wait'),
    )

    get_file = await message.document.get_file()
    ip_control = IPCheckControl(message.from_user.id)

    keyboard = await ip_control.start_mass_check(file_path=get_file.file_path)
    await message.bot.edit_message_caption(
        chat_id=message.from_user.id,
        message_id=message_id,
        caption=ip_control.mesage_text,
        reply_markup=keyboard,
    )
    await state.finish()


async def download_check_result_file(call: types.CallbackQuery):
    """Скачать файл результата чека"""
    with open(f"storage/checker/{call.data.split('#')[1]}", 'r') as user_file:
        await call.bot.send_document(chat_id=call.from_user.id, document=user_file, caption='✅ #YouFastChecker')


def register_mass_ip_checker_controllers(dp: Dispatcher):
    # Массовый чек - запрашиваем файл у пользователя
    dp.register_callback_query_handler(
        mass_check_file_request,
        lambda call: call.data == 'opt_check',
        state='*',
    )
    # Принять файл, распарсить, отправить на чек
    dp.register_message_handler(
        load_file_and_mass_check,
        content_types=['document'],
        state=MassIPCheckState.GET_FILE,
    )
    dp.register_callback_query_handler(
        download_check_result_file,
        lambda call: call.data.split("#")[0] == 'down_check',
        state='*',
    )
