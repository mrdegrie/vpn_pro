from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
# from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
# from aiogram import exceptions


# from pagination.MagicPaginator import ButtonPagination
# from models.shared_vpn.shared_vpn_control import SharedVPNcontrol
# from controllers.shared_vpn.shared_vpn_states import VPNServersState
# from models.ui.ui_storage import UIControl
from models.proxy_sell.main_poxy_control import ProxySellMainControl


async def start_main_proxy_page(call: types.CallbackQuery):
    await call.bot.answer_callback_query(call.id, show_alert=True, text='🎉 Уже скоро!')
    # proxy_control = ProxySellMainControl(call.from_user.id)
    # keyboard = await proxy_control.proxy_dell_main_page()
    # await call.message.edit_caption(caption=proxy_control.text_message, reply_markup=keyboard)


async def proxy_dell_categorys(call: types.CallbackQuery, state: FSMContext):
    """Сообщение - стартовое меню категорий для покупки Proxy"""
    # await call.bot.answer_callback_query(call.id, show_alert=True, text='🎉 Уже скоро!')
    proxy_control = ProxySellMainControl(call.from_user.id)
    keyboard = await proxy_control.get_sell_proxy_category()
    await call.message.edit_caption(caption=proxy_control.text_message, reply_markup=keyboard)


def register_main_proxy_controllers(dp: Dispatcher):
    dp.register_callback_query_handler(
        start_main_proxy_page,
        lambda Call: Call.data == 'main_proxy_page',
        state='*',
    )

    dp.register_callback_query_handler(
        proxy_dell_categorys,
        lambda Call: Call.data == 'sell-proxy',
        state='*',
    )
