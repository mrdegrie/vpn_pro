from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext


# from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
# from aiogram import exceptions


# from pagination.MagicPaginator import ButtonPagination
# from models.shared_vpn.shared_vpn_control import SharedVPNcontrol
# from controllers.shared_vpn.shared_vpn_states import VPNServersState
# from models.ui.ui_storage import UIControl


async def start_proxy_history_page(call: types.CallbackQuery, state: FSMContext):
    await call.bot.answer_callback_query(call.id, show_alert=True, text='🎉 Уже скоро!')


def register_proxy_history_controllers(dp: Dispatcher):
    dp.register_callback_query_handler(
        start_proxy_history_page,
        lambda Call: Call.data == 'proxy-history',
        state='*',
    )
