import random
import logging

from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram import exceptions

from pagination.MagicPaginator import ButtonPagination
from models.shared_vpn.shared_vpn_control import SharedVPNcontrol
from controllers.shared_vpn.shared_vpn_states import VPNServersState
from models.ui.ui_storage import UIControl


# Получаем нажатие на кнопку VPN - поазываем меню с назначениями VPN и даем выбор
async def view_taget_open_vpn_method(call: types.CallbackQuery, state: FSMContext):
    """Получить спсиок доступных назначений для VPN + лавиши управления"""

    items_control = SharedVPNcontrol()
    ui = UIControl(user_id=call.from_user.id)
    await ui.get_language()

    items, custom_keyboard, server_response = await items_control.all_target_methods(
        user_id=call.from_user.id,
    )

    if items_control.error_message:
        await call.bot.answer_callback_query(call.id, show_alert=True, text=items_control.error_message)
        await call.message.edit_caption(caption=items_control.error_message,
                                        reply_markup=items_control.keyboard)

    else:
        if items:
            paginator = ButtonPagination(button_data=items, amount_elements=10)
            keyboard = paginator.pagination(custom_keyboard=custom_keyboard, horizontal=True, switching_buttons=False)

            await call.message.edit_caption(
                caption=ui.get_text("choosing-vpn-type"),
                reply_markup=keyboard
            )
            await state.update_data(paginator=paginator)
            await state.update_data(
                message_text=ui.get_text("choosing-vpn-type"),
            )
            await state.update_data(servers_data=server_response)
            await VPNServersState.ALL_ACTIVE_SERVERS_PAGINATION.set()
        else:
            mss = ui.get_text("no_countries")
            await call.bot.answer_callback_query(call.id, show_alert=True, text=mss)
            await call.message.edit_caption(caption=mss, reply_markup=items_control.keyboard)


async def start_shared_vpn(call: types.CallbackQuery, state: FSMContext):
    """Старт shared vpn - callback version -- показываю спсиок доступных стран"""

    items_control = SharedVPNcontrol()
    ui = UIControl(user_id=call.from_user.id)
    await ui.get_language()

    items, custom_keyboard, server_response = await items_control.all_shared_vpn_items(
        user_id=call.from_user.id,
    )

    if items_control.error_message:
        await call.bot.answer_callback_query(call.id, show_alert=True, text=items_control.error_message)
        await call.message.edit_caption(caption=items_control.error_message,
                                        reply_markup=items_control.keyboard)
    else:
        if items:
            paginator = ButtonPagination(button_data=items, amount_elements=10)
            keyboard = paginator.pagination(custom_keyboard=custom_keyboard, horizontal=True)

            await call.message.edit_caption(
                caption=ui.get_text("country_for_VPN"),
                reply_markup=keyboard
            )
            await state.update_data(paginator=paginator)
            await state.update_data(
                message_text=ui.get_text("country_for_VPN"),
            )
            await state.update_data(servers_data=server_response)
            await VPNServersState.ALL_ACTIVE_SERVERS_PAGINATION.set()
        else:
            mss = ui.get_text("no_countries")
            await call.bot.answer_callback_query(call.id, show_alert=True, text=mss)
            await call.message.edit_caption(caption=mss, reply_markup=items_control.keyboard)


# Старт shared vpn - text version # bay_vpn_trial_version
async def start_shared_vpn_text_version(call: types.CallbackQuery, state: FSMContext):
    item_control = SharedVPNcontrol()
    ui = UIControl(user_id=call.from_user.id)
    await ui.get_language()

    items, custom_keyboard, server_response = await item_control.all_shared_vpn_items(
        user_id=call.from_user.id,
        text_mode=True,
    )
    if item_control.error_message:
        await call.message.answer_photo(photo=item_control.start_message,
                                        caption=item_control.error_message,
                                        reply_markup=item_control.keyboard
                                        )
    else:
        if items:
            paginator = ButtonPagination(button_data=items, amount_elements=10)
            keyboard = paginator.pagination(custom_keyboard=custom_keyboard, horizontal=True)

            await call.message.answer_photo(photo=item_control.start_message,
                                            caption=ui.get_text("country_for_VPN"),
                                            reply_markup=keyboard
                                            )
            await state.update_data(paginator=paginator)
            await state.update_data(message_text=ui.get_text("country_for_VPN"))
            await state.update_data(servers_data=server_response)
            await VPNServersState.ALL_ACTIVE_SERVERS_PAGINATION.set()
        else:
            mss = ui.get_text("no_countries")
            await call.message.answer_photo(photo=item_control.start_message,
                                            caption=mss,
                                            reply_markup=item_control.keyboard
                                            )


# Пагинация серверов
# Кнопки пагинации компаний
async def active_server_pagination(callback_query: types.CallbackQuery, state: FSMContext):
    paginator = (await state.get_data()).get('paginator')
    message_text = (await state.get_data()).get('message_text')
    keyboard = paginator.page_switch(callback_query.data.split('#')[1], horizontale=True)
    try:
        await callback_query.message.edit_caption(
            caption=message_text,
            reply_markup=keyboard
        )
        await state.update_data(paginator=paginator)
    except exceptions.MessageNotModified:
        await callback_query.bot.answer_callback_query(callback_query.id, show_alert=True, text='Стран больше нет')


async def select_device(call: types.CallbackQuery, state: FSMContext):
    """Нажатие на сервер Выбрать устройство пользователя"""
    try:
        item_id = call.data.split('#')[1]
    except ValueError:
        error_code = random.randint(1111, 999999)
        await call.message.edit_caption(f'<b>An error has occurred, contact Support! Error code #{error_code}</b>')
        logging.error(f'Errorr_code#{error_code}')
        return

    await state.update_data(item_id=item_id)
    shared_vpn = SharedVPNcontrol()
    keyboard = await shared_vpn.select_user_device(user_id=call.from_user.id)
    await call.message.edit_caption(caption=shared_vpn.MSG, reply_markup=keyboard)
    await VPNServersState.BAY_CONFIG.set()


async def server_info_browser(call: types.CallbackQuery, state: FSMContext):
    """Показываем инфо о сервере предлагаем купить конфиг"""
    item_id = (await state.get_data()).get('item_id')
    shared_vpn = SharedVPNcontrol()
    keyboard = await shared_vpn.server_remote_page(item_id=item_id, user_id=call.from_user.id)
    if shared_vpn.MSG:
        await state.update_data(device=call.data.split('#')[1])
        await call.message.edit_caption(caption=shared_vpn.MSG, reply_markup=keyboard)

    elif shared_vpn.error_message:
        await call.bot.answer_callback_query(call.id, show_alert=True, text=shared_vpn.error_message)


async def search_country(call: types.CallbackQuery, state: FSMContext):
    """Поиск стран по названию"""

    shared_vpn = SharedVPNcontrol()
    keyboard = await shared_vpn.search_country_page(call.from_user.id)
    msg = await call.message.edit_caption(caption=shared_vpn.MSG, reply_markup=keyboard)
    await state.update_data(msg=msg['message_id'])
    await VPNServersState.SEARTCH_CONFIG.set()


async def get_search_country(message: types.Message, state: FSMContext):
    """Произвести поиск по странам"""

    await message.delete()

    ui = UIControl(user_id=message.from_user.id)
    await ui.get_language()
    error_keyboard = InlineKeyboardMarkup()
    error_keyboard.row(InlineKeyboardButton(text=ui.get_text("next_start_message"), callback_data='shared_vpn'))
    msg = (await state.get_data()).get('msg')

    if len(message.text) < 2:
        await message.bot.edit_message_caption(chat_id=message.from_user.id,
                                               message_id=msg,
                                               caption=ui.get_text("message_short_text"),
                                               reply_markup=error_keyboard)
    elif len(message.text) > 20:
        await message.bot.edit_message_caption(chat_id=message.from_user.id,
                                               message_id=msg,
                                               caption=ui.get_text("message_long_text"),
                                               reply_markup=error_keyboard)
    else:
        # Производим поиск серверов по иимени
        query = message.text
        items_control = SharedVPNcontrol()
        items, custom_keyboard, server_response = await items_control.all_shared_vpn_items(
            user_id=message.from_user.id,
            query=query,
            search_mode=True,
        )

        if items_control.error_message:
            await message.bot.edit_message_caption(chat_id=message.from_user.id,
                                                   message_id=msg,
                                                   caption=items_control.error_message,
                                                   reply_markup=items_control.keyboard)
        else:
            if items:
                paginator = ButtonPagination(button_data=items, amount_elements=10)
                keyboard = paginator.pagination(custom_keyboard=custom_keyboard, horizontal=True)

                await message.bot.edit_message_caption(chat_id=message.from_user.id,
                                                       message_id=msg,
                                                       caption=ui.get_text("select_county"),
                                                       reply_markup=keyboard)

                await state.update_data(paginator=paginator)
                await state.update_data(
                    message_text=ui.get_text("select_county"),
                )
                await state.update_data(servers_data=server_response)
                await VPNServersState.ALL_ACTIVE_SERVERS_PAGINATION.set()
            else:
                mss = ui.get_text("no_country")
                await message.bot.edit_message_caption(chat_id=message.from_user.id,
                                                       message_id=msg,
                                                       caption=mss,
                                                       reply_markup=items_control.keyboard)


async def create_new_config(cal: types.CallbackQuery, state: FSMContext):
    """Производим покупку товара и выпуск OVPN конфига"""

    # ---
    ui = UIControl(cal.from_user.id)
    await ui.get_language()

    await cal.message.edit_caption(ui.get_text("creating_file"))
    shared_vpn = SharedVPNcontrol()
    item_id = int(cal.data.split('#')[1])
    tariff_id = int(cal.data.split('#')[2])
    device = (await state.get_data()).get('device')
    result = await shared_vpn.buy_config(item_id=item_id,
                                         user_id=int(cal.from_user.id),
                                         device=device,
                                         tariff_id=tariff_id,
                                         )
    if not result:
        await state.finish()
        await cal.message.edit_caption(caption=shared_vpn.MSG, reply_markup=shared_vpn.keyboard)
        return

    with open(f'storage/{shared_vpn.config_name}', 'w') as ovpn_file:
        ovpn_file.write(shared_vpn.config.config.config_text)

    with open(f'storage/{shared_vpn.config_name}', 'rb') as vpn_file:
        await cal.bot.send_document(
            chat_id=cal.from_user.id,
            document=vpn_file,
            caption=shared_vpn.MSG,
            reply_markup=shared_vpn.keyboard
        )
        await state.finish()


async def create_new_trial_config(cal: types.CallbackQuery, state: FSMContext):
    """Производим бесплатный выпуск OVPN конфига"""

    ui = UIControl(cal.from_user.id)
    await ui.get_language()
    # ---
    await cal.message.edit_caption(ui.get_text("creating_file"))
    shared_vpn = SharedVPNcontrol()
    item_id = int(cal.data.split('#')[1])
    device = (await state.get_data()).get('device')
    result = await shared_vpn.buy_config(item_id=item_id,
                                         user_id=int(cal.from_user.id),
                                         device=device,
                                         trial=True,
                                         )
    if not result:
        await state.finish()
        await cal.message.edit_caption(caption=shared_vpn.MSG, reply_markup=shared_vpn.keyboard)

    with open(f'storage/{shared_vpn.config_name}', 'w') as ovpn_file:
        ovpn_file.write(shared_vpn.config.config.config_text)
    with open(f'storage/{shared_vpn.config_name}', 'rb') as vpn_file:
        await cal.bot.send_document(
            chat_id=cal.from_user.id,
            document=vpn_file,
            caption=shared_vpn.MSG,
            reply_markup=shared_vpn.keyboard
        )
        await state.finish()


def register_shared_vpn_hendlers(dp: Dispatcher):
    """Регистрация хендлеров"""

    # view_taget_open_vpn_method
    dp.register_callback_query_handler(
        view_taget_open_vpn_method,
        lambda callback_query: callback_query.data == 'shared_vpn',
        state='*',
    )

    # Start shared vpn - callback version
    dp.register_callback_query_handler(
        start_shared_vpn,
        lambda callback_query: callback_query.data == 'shared_vpn_country',
        state='*',
    )
    # Start shared vpn - text version
    dp.register_callback_query_handler(
        start_shared_vpn_text_version,
        lambda callback_query: callback_query.data == 'bay_vpn_trial_version',
        state='*',
    )
    # Пагинация серверов
    dp.register_callback_query_handler(
        active_server_pagination,
        lambda callback_query: callback_query.data.split("#")[0] == 'amg',
        state=VPNServersState.ALL_ACTIVE_SERVERS_PAGINATION,
    )
    # Запрашивем устройство пользователя
    dp.register_callback_query_handler(
        select_device,
        lambda callback_query: callback_query.data.split("#")[0] == 'item',
        state=[VPNServersState.ALL_ACTIVE_SERVERS_PAGINATION],
    )
    # Принять устройство, зпросить подтверждения покупки
    dp.register_callback_query_handler(
        server_info_browser,
        lambda callback_query: callback_query.data.split("#")[0] == 'config_pay',
        state=VPNServersState.BAY_CONFIG,
    )
    # Кнопка купить конфиг - зпрашивем устройство пользователя
    dp.register_callback_query_handler(
        create_new_config,
        lambda callback_query: callback_query.data.split("#")[0] == 'bay_shared_ovpn',
        state=VPNServersState.BAY_CONFIG,
    )
    # Кнопка получить кнфиг бесплатно за триал
    dp.register_callback_query_handler(
        create_new_trial_config,
        lambda callback_query: callback_query.data.split("#")[0] == 'trial_pay',
        state=VPNServersState.BAY_CONFIG,
    )
    # search_country
    dp.register_callback_query_handler(
        search_country,
        lambda callback_query: callback_query.data == 'search_country',
        state=VPNServersState.ALL_ACTIVE_SERVERS_PAGINATION,
    )
    # get_search_country
    dp.register_message_handler(
        get_search_country,
        content_types=['text'],
        state=VPNServersState.SEARTCH_CONFIG,
    )
