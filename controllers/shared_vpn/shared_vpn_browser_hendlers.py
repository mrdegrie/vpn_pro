from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
from typing import Optional
import os
from aiogram import exceptions

from pagination.MagicPaginator import ButtonPagination
# from models.shared_vpn import models as shared_models
from models.shared_vpn.shared_vpn_control import SharedVPNcontrol
from controllers.shared_vpn.shared_vpn_states import SharedVPNconfigState
from models.ui.locales.available_localizations import country_lang_conv
from models.ui.ui_storage import UIControl


# Производим пагинационный обзор выпущенных конфигов пользователя
async def get_my_configs(call: types.CallbackQuery, state: FSMContext):
    vpn_control = SharedVPNcontrol()
    result, custom_keyboard, custom_date = await vpn_control.get_my_configs(user_id=call.from_user.id,
                                                                            lang=call.from_user.language_code,
                                                                            )
    if vpn_control.error_message:
        await call.bot.answer_callback_query(call.id, show_alert=True, text=vpn_control.error_message)
        await call.message.edit_caption(caption=vpn_control.error_message, reply_markup=vpn_control.keyboard)
    else:
        if result:
            paginator = ButtonPagination(button_data=result, amount_elements=10)
            keyboard = paginator.pagination(custom_keyboard=custom_keyboard)

            await call.message.edit_caption(
                caption=vpn_control.MSG,
                reply_markup=keyboard
            )
            await state.update_data(paginator=paginator)
            await state.update_data(message_text=vpn_control.MSG)
            await state.update_data(configs_data=custom_date)
            await SharedVPNconfigState.ALL_CONFIG_PAGINATION.set()
        else:
            await call.bot.answer_callback_query(call.id, show_alert=True, text=vpn_control.error_message)
            await call.message.edit_caption(caption=vpn_control.error_message, reply_markup=vpn_control.keyboard)


# Кнопки пагинации конфигов
async def active_configs_pagination(callback_query: types.CallbackQuery, state: FSMContext):
    paginator = (await state.get_data()).get('paginator')
    message_text = (await state.get_data()).get('message_text')
    keyboard = paginator.page_switch(callback_query.data.split('#')[1])
    try:
        await callback_query.message.edit_caption(
            caption=message_text,
            reply_markup=keyboard
        )
        await state.update_data(paginator=paginator)
    except exceptions.MessageNotModified:
        await callback_query.bot.answer_callback_query(callback_query.id,
                                                       show_alert=True,
                                                       text='No VPN')


# Нажатие на конфиг - показываем инфо и кнопки управления конфиговм
async def config_remote_page(call: types.CallbackQuery,
                             state: FSMContext,
                             update_mode: Optional[bool] = False,
                             update_status: Optional[bool] = None,
                             ):
    config_data = (await state.get_data()).get('configs_data')
    vpn_control = SharedVPNcontrol()
    keyboard = await vpn_control.get_config_remote_control(
        config_data=config_data,
        order_id=int(call.data.split('#')[1]),
        update_mode=update_mode,
        update_status=update_status,
        user_id=call.from_user.id,
    )
    await state.update_data(call=call)
    if vpn_control.error_message:
        await call.bot.answer_callback_query(call.id, show_alert=True, text=vpn_control.error_message)
        await call.message.edit_caption(caption=vpn_control.error_message, reply_markup=keyboard)
    else:
        try:
            await call.message.edit_caption(caption=vpn_control.MSG, reply_markup=keyboard)
        except exceptions.MessageNotModified:
            pass


# Удалить конфиг OVPN
async def delete_user_config(call: types.CallbackQuery, state: FSMContext):
    await call.bot.answer_callback_query(call.id, show_alert=True, text='🎉 Скоро!')

    # await call.message.edit_caption('<b>Деактивирую конфиг ...🕘</b>')
    # vpn_control = SharedVPNcontrol()
    # await vpn_control.delete_ovpn_config(ovpn_transaction_id=int(call.data.split('#')[1]))
    # await call.bot.answer_callback_query(call.id, show_alert=True, text=vpn_control.MSG)
    # await state.finish()
    # await get_my_configs(call, state)


# Обновление конфига
async def refrash_user_config(call: types.CallbackQuery, state: FSMContext):
    await call.bot.answer_callback_query(call.id, show_alert=True, text='🎉 Скоро!')


# Скачать конфиг ВПН
async def download_config_file(call: types.CallbackQuery, state: FSMContext):
    call_back = (await state.get_data()).get('call')
    vpn_config = SharedVPNcontrol()
    config_data = await vpn_config.get_config_text(user_id=call.from_user.id, order_id=int(call.data.split('#')[1]))
    if config_data:
        ui = UIControl(user_id=call.from_user.id)
        await ui.get_language()
        country_name = country_lang_conv(
            country=config_data.country,
            lang=ui.language,
        )
        with open(f'storage/{country_name}_{config_data.order_id}.ovpn', 'w') as write_file:
            write_file.write(config_data.ovpn_config)
        with open(f'storage/{country_name}_{config_data.order_id}.ovpn', 'rb') as read_file:
            await call.bot.send_document(
                chat_id=call.from_user.id,
                document=read_file,
                caption='<b>🛡 Your file VPN</b>'
            )
        os.remove(path=f'storage/{country_name}_{config_data.order_id}.ovpn')
        await config_remote_page(call=call_back, state=state)
    else:
        await config_remote_page(call=call_back, state=state)
        await call.message.edit_caption(caption='<b>⛔️ Error download file</b>')


async def select_auto_update(call: types.CallbackQuery, state: FSMContext):
    """Переключение статуса автопродления конфига"""
    vpn_control = SharedVPNcontrol()
    if await vpn_control.select_config_auto_update(order_id=int(call.data.split('#')[1]),
                                                   user_id=call.from_user.id,
                                                   lang=call.from_user.language_code,
                                                   ):
        await call.bot.answer_callback_query(call.id,
                                             show_alert=True,
                                             text=vpn_control.MSG)
        await config_remote_page(call, state, update_mode=True, update_status=vpn_control.new_status)
    else:
        await call.bot.answer_callback_query(call.id,
                                             show_alert=True,
                                             text=vpn_control.MSG)


# Изменение геолокации сервера
async def update_country_config(call: types.CallbackQuery, state: FSMContext):
    await call.bot.answer_callback_query(call.id, show_alert=True, text='Раздел в разработке 🕘')


# Ппродлить конфиг
async def update_pay(call: types.CallbackQuery, state: FSMContext):
    await call.bot.answer_callback_query(call.id, show_alert=True, text='Раздел в разработке 🕘')


# Регистрация хендлеров
def register_shared_vpn_browser(dp: Dispatcher):
    """
    Хендлеры обзора и управления личными конфигами пользователя:param dp:
    :return:
    """
    dp.register_callback_query_handler(
        get_my_configs,
        lambda callback_query: callback_query.data == 'my_shared_config_list',
        state='*'
    )
    # Пагинация серверов
    dp.register_callback_query_handler(
        active_configs_pagination,
        lambda callback_query: callback_query.data.split("#")[0] == 'amg',
        state=SharedVPNconfigState.ALL_CONFIG_PAGINATION,
    )
    # Нажатие на кнофиг
    dp.register_callback_query_handler(
        config_remote_page,
        lambda callback_query: callback_query.data.split("#")[0] == 'item',
        state=SharedVPNconfigState.ALL_CONFIG_PAGINATION,
    )
    # Удалить конфиг
    dp.register_callback_query_handler(
        delete_user_config,
        lambda callback_query: callback_query.data == 'dell_config',
        state=SharedVPNconfigState.ALL_CONFIG_PAGINATION,
    )
    # refrash_config
    dp.register_callback_query_handler(
        refrash_user_config,
        lambda callback_query: callback_query.data == 'refrash_config',
        state=SharedVPNconfigState.ALL_CONFIG_PAGINATION,
    )
    # Скачать фоиг впн
    dp.register_callback_query_handler(
        download_config_file,
        lambda callback_query: callback_query.data.split("#")[0] == 'download_shared_config',
        state=SharedVPNconfigState.ALL_CONFIG_PAGINATION,
    )
    # Переключение атвообновления конфига
    dp.register_callback_query_handler(
        select_auto_update,
        lambda callback_query: callback_query.data.split("#")[0] == 'auto_update_select',
        state=SharedVPNconfigState.ALL_CONFIG_PAGINATION,
    )
    # Адейт геолокации конфигаа
    dp.register_callback_query_handler(
        update_country_config,
        lambda callback_query: callback_query.data.split("#")[0] == 'update_country_config',
        state=SharedVPNconfigState.ALL_CONFIG_PAGINATION,
    )
    # продлить конфиг
    dp.register_callback_query_handler(
        update_pay,
        lambda callback_query: callback_query.data.split("#")[0] == 'update_pay',
        state=SharedVPNconfigState.ALL_CONFIG_PAGINATION,
    )
