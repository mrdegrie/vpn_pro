from aiogram.dispatcher.filters.state import State, StatesGroup


class VPNServersState(StatesGroup):
    ALL_ACTIVE_SERVERS_PAGINATION = State()
    BAY_CONFIG = State()
    SEARTCH_CONFIG = State()


class SharedVPNconfigState(StatesGroup):
    ALL_CONFIG_PAGINATION = State()
