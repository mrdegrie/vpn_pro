from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram import exceptions

from configs.config import admins, redis_url
from models.admin.stats.analyticks import Analyticks


async def admin_start_base_analytic_inline(call: types.CallbackQuery, state: FSMContext):
    """Вход в базовую аналитику - inline version"""

    await state.finish()
    if call.from_user.id in admins:
        await call.message.edit_text(text='Получаю аналитику.. 🕘')
        stats = Analyticks()
        keyboard = await stats.start_base_admin_analytic()
        if stats.scales_mode:
            await call.message.delete()
            try:
                with open(stats.stats_img_url, 'rb') as image:
                    await call.bot.send_photo(chat_id=call.from_user.id,
                                              photo=image,
                                              caption=stats.MSG,
                                              reply_markup=keyboard)
            except exceptions.MessageNotModified:
                await call.bot.send_message(chat_id=call.from_user.id,
                                            text=stats.MSG,
                                            reply_markup=keyboard)


async def refrash_stats(call: types.CallbackQuery, state: FSMContext):
    """Обновить статистику без выхода"""

    await state.finish()
    if call.from_user.id in admins:
        await call.message.edit_caption(caption='Получаю аналитику.. 🕘')
        stats = Analyticks()
        keyboard = await stats.start_base_admin_analytic()
        if stats.scales_mode:
            await call.message.delete()
            try:
                with open(stats.stats_img_url, 'rb') as image:
                    await call.bot.send_photo(chat_id=call.from_user.id,
                                              photo=image,
                                              caption=stats.MSG,
                                              reply_markup=keyboard)
            except exceptions.MessageNotModified:
                await call.bot.send_message(chat_id=call.from_user.id,
                                            text=stats.MSG,
                                            reply_markup=keyboard)


def register_admin_analytic_hendlers(dp: Dispatcher):

    dp.register_callback_query_handler(
        admin_start_base_analytic_inline,
        lambda callback_query: callback_query.data == 'admin_analyticks',
        state='*',
    )
    # refrash_stats
    dp.register_callback_query_handler(
        refrash_stats,
        lambda callback_query: callback_query.data == 'refrash_stats',
        state='*',
    )