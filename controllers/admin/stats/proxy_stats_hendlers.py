from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
# from aiogram import exceptions

# from configs.config import admins
# from models.admin.stats.analyticks import Analyticks


async def all_stats_methods(call: types.CallbackQuery, state: FSMContext):
    """Страница выбора всех метоодов расширенной аналитики PRXY"""

    await call.bot.answer_callback_query(call.id, show_alert=True, text='В разработке..🕘')


def register_admin_proxy_advanced_hendlers(dp: Dispatcher):
    dp.register_callback_query_handler(
        all_stats_methods,
        lambda callback_query: callback_query.data == 'proxy_advanced_stats',
        state='*',
    )
