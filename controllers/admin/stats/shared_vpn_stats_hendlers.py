from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
import os
# from aiogram import exceptions

# from configs.config import admins
# from models.admin.stats.analyticks import Analyticks
from models.admin.stats.shared_vpn_stats import SharedVPNStats


async def admin_vpn_country_stats(call: types.CallbackQuery):
    """ Страница статистики использования геолокаций VPN"""

    await call.bot.answer_callback_query(call.id, show_alert=True, text='На тех.работах..')

    # stats = SharedVPNStats()
    # keyboard = await stats.vpn_country_stats()
    # if stats.scales_mode:
    #     await call.message.delete()
    #     with open(stats.country_stats_img_url, 'rb') as scales_img:
    #         await call.message.bot.send_photo(chat_id=call.from_user.id,
    #                                           photo=scales_img,
    #                                           caption=stats.MSG,
    #                                           reply_markup=keyboard)
    #     os.remove(path=stats.country_stats_img_url)
    #     os.remove(path=stats.svg_path)
    # else:
    #     await call.message.edit_text(text=stats.MSG, reply_markup=keyboard)


async def refrash_country_stats(call: types.CallbackQuery):
    await call.bot.answer_callback_query(call.id, show_alert=True, text='В разработке..')


def register_admin_shared_vpn_advanced_hendlers(dp: Dispatcher):

    # Страница статистики использования геолокаций VPN
    dp.register_callback_query_handler(
        admin_vpn_country_stats,
        lambda callback_query: callback_query.data == 'admin_vpn_country_stats',
        state='*',
    )
    # refrash_country_stats
    dp.register_callback_query_handler(
        refrash_country_stats,
        lambda callback_query: callback_query.data == 'refrash_country_stats',
        state='*',
    )