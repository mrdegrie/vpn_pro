from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext

from configs.config import admins
from models.admin.admin_control import AdminControl


async def admin_start_cabinet(message: types.Message, state: FSMContext):
    """Ввход в админку /start command"""
    await state.finish()
    if message.from_user.id in admins:
        admin = AdminControl()
        keyboard = await admin.get_general_admin_window()
        await message.answer(text=admin.MSG, reply_markup=keyboard)
        # await message.answer_photo(photo=admin.header_image, caption=admin.MSG, reply_markup=keyboard)


async def start_admin_inline_mode(call: types.CallbackQuery, state: FSMContext):
    """Вход в админку Callback версия"""
    await state.finish()
    admin = AdminControl()
    keyboard = await admin.get_general_admin_window()
    await call.message.edit_text(text=admin.MSG, reply_markup=keyboard)


async def back_admin(call: types.CallbackQuery, state: FSMContext):
    """Выход в админу из статистики"""

    await call.message.delete()
    await state.finish()
    admin = AdminControl()
    keyboard = await admin.get_general_admin_window()
    await call.bot.send_message(chat_id=call.from_user.id, text=admin.MSG, reply_markup=keyboard)


async def exit_admin(call: types.CallbackQuery, state: FSMContext):
    """Выход из админа"""
    await state.finish()
    await call.message.delete()


async def shared_vpn_contol(call: types.CallbackQuery, state: FSMContext):
    """Страница управления Shared VPN"""

    admin = AdminControl()
    keyboard = await admin.shared_vpn_settings_page()
    await call.message.edit_text(text=admin.MSG, reply_markup=keyboard)


def register_base_admin_hendlers(dp: Dispatcher):
    dp.register_message_handler(
        admin_start_cabinet,
        commands=['admin'],
        state='*',
    )
    dp.register_callback_query_handler(
        start_admin_inline_mode,
        lambda callback_query: callback_query.data == 'start_admin',
        state='*',
    )
    # Exit admin
    dp.register_callback_query_handler(
        exit_admin,
        lambda callback_query: callback_query.data == 'exit_admin',
        state='*',
    )
    # shared_vpn_contol
    dp.register_callback_query_handler(
        shared_vpn_contol,
        lambda callback_query: callback_query.data == 'admin_shared_vpn_remote',
        state='*',
    )
    # back_admin
    dp.register_callback_query_handler(
        back_admin,
        lambda callback_query: callback_query.data == 'back_admin',
        state='*',
    )
