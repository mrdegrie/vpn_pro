from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
import aioredis
from aioredis import Redis

import pydantic
from pydantic import BaseModel, ValidationError
from typing import Optional, List

from configs.config import admins
# from models.admin.admin_control import AdminControl
from models.admin.vpn.admin_vpn_control import AdminVPNControl
from database.db_connected import get_redis


class Test(BaseModel):
    id: int
    user_name: str
    balance: float


class Test2(BaseModel):
    status: bool
    result: Test


async def ping_servers_command(message: types.Message, state: FSMContext):
    """Команда пинга VPN серверов"""
    if message.from_user.id in admins:
        await state.finish()
        await message.answer(text='<b>Запускаю Ping..</b>🕘')
        vpn_control = AdminVPNControl()
        msg = await vpn_control.ping_vpn_servers()
        await message.delete()
        await message.answer(text=msg)


async def test_redis(mess: types.Message):
    redis: Redis = await get_redis()
    result = Test(id=666, user_name='degrie', balance=50)
    test_data = Test2(status=True, result=result)
    await redis.set('user', value=test_data.json(), ex=40)
    # await redis.delete('data', 'names')

    await redis.close()


async def test_redis_2(msg: types.Message):
    redis: Redis = await get_redis()
    result = await redis.get(name='user')
    user = Test2.parse_raw(result)
    print(user)
    await redis.close()
    return


def register_admin_vpn_settings_hendlers(dp: Dispatcher):
    dp.register_message_handler(
        ping_servers_command,
        commands=['vpn_ping'],
        state='*',
    )
    # dp.register_message_handler(
    #     test_redis,
    #     commands=['redis'],
    #     state='*',
    # )
    # dp.register_message_handler(
    #     test_redis_2,
    #     commands=['redis_2'],
    #     state='*',
    # )
