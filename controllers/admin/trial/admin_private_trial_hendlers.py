from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
from typing import Optional, List

from models.admin.trial.private_trial_control import PrivateTrialControl
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from models.admin.admin_state import PrivateTrialState
from models.admin.mailing.admin_mailing_control import Mailing
from database.data_models import User


async def select_activate_mode(call: types.CallbackQuery):
    """Просим выбрать метод сортировки юзеров"""

    trial = PrivateTrialControl()
    keyboard = await trial.select_activate_mode()
    await call.message.edit_text(text=trial.MSG, reply_markup=keyboard)


async def select_users_by_activity(call: types.CallbackQuery):
    """Выбрать пользователей по активности - даем на выбор метод сортиировки по активности"""

    trial = PrivateTrialControl()
    keyboard = await trial.get_active_pattern_mailing()
    await call.message.edit_text(text=trial.MSG, reply_markup=keyboard)
    await PrivateTrialState.SELECT_ACTIVE_PATTERN.set()


async def get_user_id(call: types.CallbackQuery, state: FSMContext):
    """Метод сортировки по ID - запрашиваем ID юзера"""
    await call.bot.answer_callback_query(call.id, show_alert=True, text='В разработке..🕘')


async def get_no_pyrchase_users(call: types.CallbackQuery, state: FSMContext):
    """Получить юзеров подходящих под критерий сортировки - без покупок"""

    await call.message.edit_text(text='Один момент..🕘')
    pattern = call.data.split("#")[1]
    trial = PrivateTrialControl()
    keyboard = await trial.get_users_for_private_trial(pattern=pattern)

    await call.message.edit_text(text=trial.MSG, reply_markup=keyboard)
    await state.update_data(pattern=pattern)
    await state.update_data(users_count=trial.user_count)
    await state.update_data(users=trial.users)

    if trial.status:
        await PrivateTrialState.SELECT_DAYES.set()


async def get_count_days(call: types.CallbackQuery, state: FSMContext):
    """Получить количество дней для активации триалки"""

    days = int(call.data.split('#')[1])
    users_count = (await state.get_data()).get('users_count')
    await state.update_data(days=days)
    trial = PrivateTrialControl()
    keyboard = await trial.accept_create_trial_activate(days=days, users=users_count)
    await call.message.edit_text(text=trial.MSG, reply_markup=keyboard)
    await PrivateTrialState.TRIAL_ACTIVATE.set()


async def activate_pprivate_trial(call: types.CallbackQuery, state: FSMContext):
    """Активировать триальный период"""

    pattern = (await state.get_data()).get('pattern')
    days = (await state.get_data()).get('days')
    if not pattern and days:
        await call.bot.answer_callback_query(call.id, show_alert=True, text='Ошибка получения данных из стейтов')
        await select_activate_mode(call)

    await call.message.edit_text(text='Активирую триалки..🕘 Это может отнять время 💤')
    # Начинаем процесс активации триалки
    trial = PrivateTrialControl()
    keyboard = await trial.trial_activate(days=days, pattern=pattern)
    await call.message.edit_text(text=trial.MSG, reply_markup=keyboard)
    await PrivateTrialState.TRIAL_MAILIG.set()


async def start_trial_mailing(callback_query: types.CallbackQuery, state: FSMContext):
    """Начиннаем рассылку"""

    pattern = (await state.get_data()).get('pattern')
    days = (await state.get_data()).get('days')

    meiling_text = 'Привет! Заметили, что вы интересовались нашим VPN, но так и не попробовали 😥\n\n' \
                   f'Мы уверены, что он вам понравится, потому <b>дарим вам {days} дней подписки бесплатно!</b> 🚀\n\n' \
                   'Чтобы активировать, перейдите по кнопке внизу этого сообщения.\n\n' \
                   '❗️<b>Предложение доступно только следующие 6 часов.</b>\n\n' \
                   '---------------\n\n' \
                   'Забирайте, убедитесь сами в нашей скорости и анонимности – и оставайтесь с нами👇'

    # mailing_kyboards = [
    #     InlineKeyboardButton(text='🛡 Забрать VPN', callback_data='bay_vpn_trial_version')
    # ]
    users: List[User] = (await state.get_data()).get('users')

    msg = await callback_query.message.edit_text(text=f'<b>Начинаю рассылку, ожидайте....⏱</b>')
    mail = Mailing(user_id=int(callback_query.from_user.id),
                   user_name=callback_query.from_user.username,
                   text_meiling=meiling_text,
                   dp=callback_query,
                   msg=msg['message_id'],
                   pattern=pattern,
                   mode='private_trial_mailing',
                   text_mode='start_trial_mailing',
                   trial_users=users)

    keyboard = await mail.start_mailing()
    await callback_query.message.edit_text(text=mail.Message, reply_markup=keyboard)
    await state.finish()
    await callback_query.answer(show_alert=True, text='✅ Рассылка завершена')
    await state.finish()


def register_admin_private_trial_hendlers(dp: Dispatcher):
    """Регистрация хендлеров"""

    dp.register_callback_query_handler(
        select_activate_mode,
        lambda callback_query: callback_query.data == 'add_private_trials',
        state='*',
    )
    dp.register_callback_query_handler(
        select_users_by_activity,
        lambda callback_query: callback_query.data == 'private_trial_activae_sort',
        state='*',
    )
    # Выьор конкретного юзера по ID
    dp.register_callback_query_handler(
        get_user_id,
        lambda callback_query: callback_query.data == 'private_trial_id_sotr',
        state='*',
    )
    # Получить юзеров подзодящих под критерий сортировки - без покупок
    dp.register_callback_query_handler(
        get_no_pyrchase_users,
        lambda callback_query: callback_query.data.split("#")[0] == 'private_trial',
        state=PrivateTrialState.SELECT_ACTIVE_PATTERN,
    )
    # private_trial_activate
    dp.register_callback_query_handler(
        get_count_days,
        lambda callback_query: callback_query.data.split("#")[0] == 'trial_period',
        state=PrivateTrialState.SELECT_DAYES,
    )
    # Активировать триалку
    dp.register_callback_query_handler(
        activate_pprivate_trial,
        lambda callback_query: callback_query.data == 'private_trial_activate',
        state=PrivateTrialState.TRIAL_ACTIVATE,
    )
    # Произвести рассылку
    dp.register_callback_query_handler(
        start_trial_mailing,
        lambda callback_query: callback_query.data == 'start_trial_mailing',
        state=PrivateTrialState.TRIAL_MAILIG,
    )

