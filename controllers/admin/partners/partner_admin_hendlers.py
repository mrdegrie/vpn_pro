from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext

from configs.config import admins
from models.admin.admin_control import AdminControl


async def admin_page_for_managing_partners(call: types.CallbackQuery, state: FSMContext):
    """Страница управления партнерской программой"""


def register_admin_partner_hendlers(dp: Dispatcher):
    # Страница управления партнерской программой
    dp.register_callback_query_handler(
        admin_page_for_managing_partners,
        lambda callback_query: callback_query.data == 'admin_partner_system',
        state='*',
    )
