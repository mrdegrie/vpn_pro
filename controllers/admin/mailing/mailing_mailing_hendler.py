from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
# from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
# from aiogram import exceptions

from models.admin.mailing.admin_mailing_control import Mailing
from models.admin.admin_state import MailingState


async def mailing_lauch(callback_query: types.CallbackQuery, state: FSMContext):
    """Обрабатываем кнопку АДМИНКА-РАССЫЛКА"""

    await state.finish()
    mail = Mailing(user_id=int(callback_query.from_user.id))
    keyboard = await mail.admin_launch_create()
    if keyboard:
        await callback_query.message.edit_text(text=mail.Message, reply_markup=keyboard)
    else:
        await callback_query.bot.answer_callback_query(callback_query.id, show_alert=True, text='ОШИБКА ДОСТУПА')


async def pre_mailing(call: types.CallbackQuery):
    """Страница обзора подготовленных рассылок"""
    mail = Mailing(user_id=int(call.from_user.id))
    keyboard = await mail.pre_mailing_page()
    if not keyboard:
        await call.bot.answer_callback_query(call.id, show_alert=True, text='Error permision')
    await call.message.edit_text(text=mail.Message, reply_markup=keyboard)


async def mailing_active_gradation_select_pattrn(callback_query: types.CallbackQuery, state: FSMContext):
    """Копка рассылки с выборкой по активности, формируем патерны выбороки"""

    await state.finish()
    mail = Mailing()
    keyboard = await mail.get_active_pattern_mailing()
    await callback_query.message.edit_text(text=mail.Message, reply_markup=keyboard)


# Ловим выбор паттерна активности, запрашиваем текст сообщения
async def requests_mailing_text_the_active_pattern(callback_query: types.CallbackQuery, state: FSMContext):
    await state.update_data(mailing_pattern=callback_query.data.split('#')[1])
    await callback_query.message.edit_text(text='<b>Получаю пользователей...🕘</b>')
    mail = Mailing(pattern=callback_query.data.split('#')[1])
    users = await mail.create_amount_base_active_mailing()

    keyboard = types.InlineKeyboardMarkup()
    keyboard.row(types.InlineKeyboardButton(text='◀︎', callback_data='malinig_active_gradation'))
    if users > 0:
        meg = await callback_query.message.edit_text(text=mail.Message, reply_markup=keyboard)
        await state.update_data(msg=meg['message_id'])
        await MailingState.CREATE_MESSAGE_TEXT_ACTIVE_PATTERN.set()
    else:
        await callback_query.bot.answer_callback_query(
            callback_query.id,
            show_alert=True,
            text='Нет пользователей в этой категории'
        )
        await callback_query.message.edit_text(text='Нет пользователей в этой категории', reply_markup=keyboard)


# Ловим текст сообщения, валидируем, запрашиваем подтверждения рассылки
async def create_new_text_mailing_active_pattern(message: types.Message, state: FSMContext):
    await message.delete()
    msg = (await state.get_data()).get('msg')
    keyboard = types.InlineKeyboardMarkup()
    if len(message.text) < 300:  # валидируем длину сообщения
        await state.update_data(meiling_text=message.text)
        keyboard.row(types.InlineKeyboardButton(text='🚀 НАЧАТЬ РАССЫЛКУ', callback_data='start_mailing'))
        keyboard.row(types.InlineKeyboardButton(text='◀︎', callback_data='malinig_active_gradation'))
        message_text = f'<b>ВАШЕ СООБЩЕНИЕ:\n\n{message.text}\n\nНачинаем?</b>'
        await message.bot.edit_message_text(
            chat_id=message.from_user.id,
            message_id=msg,
            text=message_text,
            reply_markup=keyboard
        )
        await MailingState.START_MAILING.set()
    else:
        keyboard.row(types.InlineKeyboardButton(text='◀︎', callback_data='malinig_active_gradation'))
        await message.bot.edit_message_text(
            chat_id=message.from_user.id,
            message_id=msg,
            text='<b>УПС, СООБЩЕНИЕ ДЛИНЕЕ 300 символов!</b>',
            reply_markup=keyboard
        )


async def mailing_items_sale_gradation(callback_query: types.CallbackQuery):
    await callback_query.bot.answer_callback_query(callback_query.id, show_alert=True, text='Раздел в разработке')


# начиннаем рассылку
async def start_mailing(callback_query: types.CallbackQuery, state: FSMContext):
    mailing_pattern = (await state.get_data()).get('mailing_pattern')
    meiling_text = (await state.get_data()).get('meiling_text')
    msg = await callback_query.message.edit_text(text=f'<b>Начинаю рассылку, ожидайте....⏱</b>')
    mail = Mailing(user_id=int(callback_query.from_user.id),
                   user_name=callback_query.from_user.username,
                   text_meiling=meiling_text,
                   dp=callback_query,
                   msg=msg['message_id'],
                   pattern=mailing_pattern)

    keyboard = await mail.start_mailing()
    await callback_query.message.edit_text(text=mail.Message, reply_markup=keyboard)
    await state.finish()
    await callback_query.answer(show_alert=True, text='✅ Рассылка завершена')


async def confirmation_mailing_active_user(call: types.CallbackQuery):
    """Выбор типа рассылки - подготовленные, активу напоминание о рефке"""

    pattern = call.data.split('#')[1]
    await call.message.edit_text(text='<b>Получаю пользователей...🕘</b>')
    # await state.update_data(pattern=pattern)
    mail = Mailing(pattern=pattern)
    users = await mail.create_amount_base_active_mailing()
    keyboard = types.InlineKeyboardMarkup()
    if users:
        keyboard.row(types.InlineKeyboardButton(text='START!', callback_data='start_pre_mailing'))
    keyboard.row(types.InlineKeyboardButton(text='◀︎', callback_data='malinig_active_gradation'))
    if users > 0:
        await call.message.edit_text(text=mail.Message, reply_markup=keyboard)
        # await state.update_data(msg=meg['message_id'])
        # await MailingState.START_PRE_MAILING.set()
    else:
        await call.bot.answer_callback_query(
            call.id,
            show_alert=True,
            text='Нет пользователей в этой категории'
        )
        await call.message.edit_text(text='Нет пользователей в этой категории', reply_markup=keyboard)


# начиннаем рассылку
# active_users
async def start_pre_mailing(callback_query: types.CallbackQuery, state: FSMContext):
    msg = await callback_query.message.edit_text(text=f'<b>Начинаю рассылку, ожидайте....⏱</b>')
    mail = Mailing(user_id=int(callback_query.from_user.id),
                   user_name=callback_query.from_user.username,
                   dp=callback_query,
                   msg=msg['message_id'],
                   pattern='active_users',
                   text_mode='pre_mailing_active_users')

    keyboard = await mail.start_mailing()
    await callback_query.message.edit_text(text=mail.Message, reply_markup=keyboard)
    await callback_query.answer(show_alert=True, text='✅ Рассылка завершена')
    await state.finish()


async def confirmation_mailing_reminder_stopped_paying(call: types.CallbackQuery):
    """Выбор типа рассылки - подготовленные, активу переставшим платить за VPN"""

    pattern = call.data.split('#')[1]
    await call.message.edit_text(text='<b>Получаю пользователей...🕘</b>')
    # await state.update_data(pattern=pattern)
    mail = Mailing(pattern=pattern)
    users = await mail.create_amount_base_active_mailing()
    keyboard = types.InlineKeyboardMarkup()
    if users:
        keyboard.row(types.InlineKeyboardButton(text='START!', callback_data='start_pre_mailing_stopped_paying'))
    keyboard.row(types.InlineKeyboardButton(text='◀︎', callback_data='malinig_active_gradation'))
    if users > 0:
        await call.message.edit_text(text=mail.Message, reply_markup=keyboard)
        # await state.update_data(msg=meg['message_id'])
        # await MailingState.START_PRE_MAILING.set()
    else:
        await call.bot.answer_callback_query(
            call.id,
            show_alert=True,
            text='Нет пользователей в этой категории'
        )
        await call.message.edit_text(text='Нет пользователей в этой категории', reply_markup=keyboard)


# начиннаем рассылку
async def start_pre_mailing_stopped_paying(callback_query: types.CallbackQuery, state: FSMContext):
    msg = await callback_query.message.edit_text(text=f'<b>Начинаю рассылку, ожидайте....⏱</b>')
    mail = Mailing(user_id=int(callback_query.from_user.id),
                   user_name=callback_query.from_user.username,
                   dp=callback_query,
                   msg=msg['message_id'],
                   pattern='stopped_paying',
                   text_mode='pre_mailing_stopped_paying')

    keyboard = await mail.start_mailing()
    await callback_query.message.edit_text(text=mail.Message, reply_markup=keyboard)
    await callback_query.answer(show_alert=True, text='✅ Рассылка завершена')
    await state.finish()


def register_admin_mailing_hendlers(dp: Dispatcher):
    """Регистрация хендлеров рассылки для админки"""

    # ----- Обрабатываем кнопку АДМИНКА\РАССЫЛКА
    dp.register_callback_query_handler(
        mailing_lauch,
        lambda callback_query: callback_query.data == 'admin_mainling',
        state='*'
    )

    # Копка рассылки с выборкой по активности, формируем патерны выборок
    dp.register_callback_query_handler(
        mailing_active_gradation_select_pattrn,
        lambda callback_query: callback_query.data == 'malinig_active_gradation',
        state='*'
    )

    # Ловим выбор паттерна активности, запрашиваем текст сообщения
    dp.register_callback_query_handler(
        requests_mailing_text_the_active_pattern,
        lambda callback_query: callback_query.data.split("#")[0] == 'active_mailing',
        state='*'
    )

    #  -- Ловим текст сообщения, валидируем, запраашиваем подтверждения рассылки
    dp.register_message_handler(
        create_new_text_mailing_active_pattern,
        content_types=['text'],
        state=MailingState.CREATE_MESSAGE_TEXT_ACTIVE_PATTERN,
    )

    #  -- Копка рассылки с выборкой по активности, формируем патерны выборок
    dp.register_callback_query_handler(
        mailing_items_sale_gradation,
        lambda callback_query: callback_query.data == 'mailing_items_sale_gradation',
        state='*',
    )

    #  -- Начиннаем рассылку
    dp.register_callback_query_handler(
        start_mailing,
        lambda callback_query: callback_query.data == 'start_mailing',
        state=MailingState.START_MAILING,
    )
    # -- Дать выбор патерная подготовленной рассылкит
    dp.register_callback_query_handler(
        pre_mailing,
        lambda callback_query: callback_query.data == 'pre_mainling',
        state='*',
    )
    # Запрашиваем подтверждение на старт рассылки по указанному паттерну
    dp.register_callback_query_handler(
        confirmation_mailing_active_user,
        lambda callback_query: callback_query.data == 'pre_mainl#active_users',
        state='*',
    )

    # Начинаем рассылку prre-mailing
    dp.register_callback_query_handler(
        start_pre_mailing,
        lambda callback_query: callback_query.data == 'start_pre_mailing',
        state="*",
    )

    # Запрашиваем подтверждение на старт рассылки по указанному паттерну
    dp.register_callback_query_handler(
        confirmation_mailing_reminder_stopped_paying,
        lambda callback_query: callback_query.data == 'pre_mainl#stopped_paying',
        state='*',
    )

    # Начинаем рассылку prre-mailing-stopped_paying
    dp.register_callback_query_handler(
        start_pre_mailing_stopped_paying,
        lambda callback_query: callback_query.data == 'start_pre_mailing_stopped_paying',
        state="*",
    )