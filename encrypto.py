from random import randint
from uuid import uuid4


class Encrypto:
    def __init__(self, offset: int = None):
        self.__offset = offset
        self.__symbols: str = 'abcdefghijklmnopqrstuvwxyz-'
        self.__decryptor: dict = {}
        self.__decrypt_storage: list = []
        self.__shange_decryption_sign()

        self._decrypt_uuid_value: str = ''

    def __rundom(self) -> int:
        while True:
            random_key = randint(0, 99)
            if random_key not in self.__decrypt_storage:
                self.__decrypt_storage.append(random_key)
                return random_key

    def __shange_decryption_sign(self) -> None:

        for sinbol in self.__symbols:
            self.__decryptor.update({sinbol: self.__rundom()})
        for num in range(0, 11):
            self.__decryptor.update({num: self.__rundom()})

        self.__decrypt_storage.clear()
        return

    @property
    def recrypt_uuid(self) -> int:
        """
        :return: str value
        """
        value_to_str = str(uuid4())
        for num in value_to_str:
            if num.isdigit():
                self._decrypt_uuid_value += str(self.__decryptor[int(num)])
            else:
                self._decrypt_uuid_value += str(self.__decryptor[num.lower()])

        if not self.__offset:
            return int(self._decrypt_uuid_value)
        if self.__offset < 0:
            raise ValueError("Offset less than zero is not supported")
        if self.__offset > 50:
            raise ValueError("Offset more than 50 is not supported")

        return int(self._decrypt_uuid_value[:self.__offset])
