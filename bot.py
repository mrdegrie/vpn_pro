#!/usr/bin/env python3
import logging
from aiogram import Bot, Dispatcher, executor, types
# from aiogram.types import WebAppInfo
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.contrib.middlewares.logging import LoggingMiddleware

from configs.config import TOKEN
from controllers.base_controllers.base_hendlers import register_base_hendlers
from controllers.privat_cabinet.cabinet_hendlers import register_private_cabinet_hendlers
from controllers.support.support_hendlers import register_user_support_hendlers
from controllers.shared_vpn.shared_vpn_hendlers import register_shared_vpn_hendlers
from controllers.shared_vpn.shared_vpn_browser_hendlers import register_shared_vpn_browser
from controllers.partner_system.partner_hendlers import register_partnr_hendlers
from controllers.payment.payment_hendlers import register_payment_hendlers
from controllers.payment.free_kassa import register_free_kassa_hendlers
from controllers.admin.base_admin_hendlers import register_base_admin_hendlers
from controllers.admin.stats.admin_analyticks_hendlers import register_admin_analytic_hendlers
from controllers.admin.mailing.mailing_mailing_hendler import register_admin_mailing_hendlers
from controllers.payment.payok_hendlers import register_payok_hendlers
from controllers.payment.lava_hendlers import register_lava_hendlers
from controllers.admin.trial.admin_private_trial_hendlers import register_admin_private_trial_hendlers
from controllers.admin.stats.shared_vpn_stats_hendlers import register_admin_shared_vpn_advanced_hendlers
from controllers.admin.stats.proxy_stats_hendlers import register_admin_proxy_advanced_hendlers
from controllers.admin.VPN.vpn_settings_hendlers import register_admin_vpn_settings_hendlers
from controllers.proxy_sell.main_proxy_controllers import register_main_proxy_controllers
from controllers.ip_score_check.main_controllers import register_main_ip_score_controllers
from controllers.ip_score_check.mass_cheking import register_mass_ip_checker_controllers
from controllers.proxy_sell.history.controllers import register_proxy_history_controllers
from models.middlwares.trottling import ThrottleMiddleware
from models.tasks.task_starter import scheduler_task_starter

logging.basicConfig(format=u'%(filename)+13s [ LINE:%(lineno)-4s] %(levelname)-8s [%(asctime)s] %(message)s',
                    filename='logs/errors.log')
bot = Bot(token=TOKEN, parse_mode=types.ParseMode.HTML)
dp = Dispatcher(bot, storage=MemoryStorage())
dp.middleware.setup(LoggingMiddleware())
dp.middleware.setup(ThrottleMiddleware())

# Controllers register (hendlers)
register_base_hendlers(dp)  # Base hendlers
register_private_cabinet_hendlers(dp)  # Private cabinet
register_user_support_hendlers(dp)  # User supports
# -- Payments
register_payment_hendlers(dp)  # Payment hendlers
register_free_kassa_hendlers(dp)  # free-kassa Hendler
register_payok_hendlers(dp)  # Payok.io payments hendlers
register_lava_hendlers(dp)  # Lava.ru payments hendlers

# --/
register_shared_vpn_hendlers(dp)  # Function created shared vpn
register_shared_vpn_browser(dp)  # Browser and control private configs -  SHARED VPN user (ЛК)
register_partnr_hendlers(dp)  # Partner system

# -- Proxy
register_main_proxy_controllers(dp)
register_proxy_history_controllers(dp)
# -/

# IP SCORE SCHEK
register_main_ip_score_controllers(dp)  # manual check
register_mass_ip_checker_controllers(dp)  # Mass check ip

# --- Admimn system controllers
register_base_admin_hendlers(dp)  # Base admin hendlers
register_admin_analytic_hendlers(dp)  # Admmi analytics
register_admin_mailing_hendlers(dp)  # Admin mailing module
register_admin_private_trial_hendlers(dp)  # Admin trial settings
register_admin_shared_vpn_advanced_hendlers(dp)  # advanced stats shared VPN
register_admin_proxy_advanced_hendlers(dp)  # advanced stats proxy_sell
register_admin_vpn_settings_hendlers(dp)  # VPN admin settings


async def on_startup(dp: Dispatcher) -> None:
    return logging.info('succes created redis connection pool')


#
# @dp.message_handler(commands=['test'], state='*')
# async def test(call: types.Message):
#     k = types.InlineKeyboardMarkup()
#     k.row(types.InlineKeyboardButton(
#         text='test',
#         web_app=WebAppInfo(
#             url='https://vavada.com/ru/games/push-fat-santa-2/demo'),
#     ))
#     await call.answer(text='tet', reply_markup=k)

# @dp.message_handler(commands=['test'], state='*')
# async def test(call: types.Message):
#     await call.answer('<code>1126262393</code>')


if __name__ == '__main__':
    scheduler_task_starter(dp=dp)
    executor.start_polling(dp, skip_updates=True, on_startup=on_startup)
